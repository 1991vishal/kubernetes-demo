# README #

<< [back to Home](../) >>

# Not tested yet

testing these guides
[kubeclusters](https://www.kubeclusters.com/docs/How-to-Deploy-a-Highly-Available-kubernetes-Cluster-with-Kubeadm-on-CentOS7)
[LinuxTechi](https://www.linuxtechi.com/setup-highly-available-kubernetes-cluster-kubeadm/) 



To create HA, load balancer would be required. The architecture would look like this

![kubeadm ha](ha.jpg)

## on all nodes

 - disable selinux, firewall

 ```
setenforce 0
sed -i 's/SELINUX=permissive/SELINUX=disabled/' /etc/sysconfig/selinux
systemctl disable firewalld
systemctl stop firewalld
 ```

- disable swap

```
swapoff -a
sed -i 's/^.*swap/#&/' /etc/fstab
```


- network - iptables forwarding

```
iptables -P FORWARD ACCEPT
cat <  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
vm.swappiness=0
EOF

sysctl --system
```