# README #

<< [back to Home](../) >>

# Downgrade

kubeadm doesn't support downgrade and also, not recommended approach if any of Kubernetes distribution offers downgrading. 

[Github source](https://github.com/kubernetes/website/issues/17401), [Downgrade limitations from Google](https://cloud.google.com/kubernetes-engine/docs/how-to/upgrading-a-cluster#downgrading_limitations)


# upgrade 
[source](https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/)

The upgrade workflow at high level is the following:

1. Research about currect vs potential kubernetes versions


    - Please upgrade package version of `kubeadm` with next desired version. Then run `kubeadm upgrade plan` first. This will give information of next potential stable version. 

    - Now, please review change under next version and check if any of kubernetes object is marked under `deprecation` which could impact any application running under the existing infrastructure. 

    - sometimes, third party plugin/provider require to be upgraded manually. 

        + e.g. upgrading from `1.19` to `1.20`

            a. it requires swap should be turned off,  use of static control plane and etcd pods or external etcd, and recommendation of taking proper backups
            
            b. cni provider should be upgraded manually.  
         

2. Upgrade a primary control plane node.

3. Upgrade additional control plane nodes.

4. Upgrade worker nodes.


# Labs

upgrading from `1.19.x` to `1.21.x`

source [1.19.x to 1.20.x](https://v1-20.docs.kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/), [1.20.x to 1.21.x](https://v1-21.docs.kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/)


First I will setup cluster using `kubeadm=1.19.0`. For detailed installation guide, please refer [Install Kubernetes using kubeadm](../install-k8s)

```
#Ubuntu
#ubuntu/debian
sudo apt update && sudo apt install -y docker.io open-iscsi apt-transport-https ca-certificates curl
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update


#centos
sudo yum install -y yum-utils
curl -fsSL https://get.docker.com | sudo bash -
sudo yum install -y iscsi-initiator-utils
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

sudo systemctl enable docker
sudo service docker restart
sudo chmod 666 /var/run/docker.sock

#ubuntu
sudo apt list -a kubelet # this will give me exact available vrsion of particular package.
VER=1.19.0-00
sudo apt install kubeadm=$VER kubelet=$VER kubectl

#Centos
sudo yum --showduplicates list kubelet  --disableexcludes kubernetes
VER=1.19.0-0
sudo yum install kubectl kubeadm-$VER --disableexcludes kubernetes


sudo kubeadm init --pod-network-cidr=10.244.0.0/16

mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config

kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml

sudo systemctl enable kubelet
sudo systemctl daemon-reload && sudo systemctl restart kubelet

```

```
cloud_user@9f7423f9381c:~/Downloads$ kubectl get nodes
NAME                           STATUS   ROLES    AGE   VERSION
9f7423f9381c.mylabserver.com   Ready    master   45s   v1.19.0
cloud_user@9f7423f9381c:~/Downloads$ 
```

Now most importantly, as explained in the offical guide, major version can only be upgraded by +1 (e.g `1.17.x` to `1.18.x`, `1.18.x` to `1.19.x`). If plan is to upgrade version from `1.19.x` to `1.21.x`, it has to be done from `1.19.x` to `1.20.x` then `1.21.x` (it wont be possible to upgrade directly from `1.19.x` to `1.21.x`)

let us upgrade `kubeadm` and `kubelet`. Next version would be `1.20.0`


```
#Ubuntu
VER=1.20.0-00
sudo apt install kubeadm=$VER kubectl

#Centos
VER=1.20.0-0
sudo yum install kubectl kubeadm-$VER --disableexcludes kubernetes

```

now let us run `kubeadm upgrade plan` to find next stable version. 

```

Output from #Ubuntu

cloud_user@9f7423f9381c:~/Downloads$ sudo kubeadm upgrade plan
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade] Fetching available versions to upgrade to
[upgrade/versions] Cluster version: v1.19.14
[upgrade/versions] kubeadm version: v1.20.0
I0820 02:56:04.647989   21239 version.go:251] remote version is much newer: v1.22.1; falling back to: stable-1.20
[upgrade/versions] Latest stable version: v1.20.10
[upgrade/versions] Latest stable version: v1.20.10
[upgrade/versions] Latest version in the v1.19 series: v1.19.14
[upgrade/versions] Latest version in the v1.19 series: v1.19.14

Components that must be upgraded manually after you have upgraded the control plane with 'kubeadm upgrade apply':
COMPONENT   CURRENT       AVAILABLE
kubelet     1 x v1.19.0   v1.20.10

Upgrade to the latest stable version:

COMPONENT                 CURRENT    AVAILABLE
kube-apiserver            v1.19.14   v1.20.10
kube-controller-manager   v1.19.14   v1.20.10
kube-scheduler            v1.19.14   v1.20.10
kube-proxy                v1.19.14   v1.20.10
CoreDNS                   1.7.0      1.7.0
etcd                      3.4.9-1    3.4.13-0

You can now apply the upgrade by executing the following command:

        kubeadm upgrade apply v1.20.10

Note: Before you can perform this upgrade, you have to update kubeadm to v1.20.10.

_____________________________________________________________________


The table below shows the current state of component configs as understood by this version of kubeadm.
Configs that have a "yes" mark in the "MANUAL UPGRADE REQUIRED" column require manual config upgrade or
resetting to kubeadm defaults before a successful upgrade can be performed. The version to manually
upgrade to is denoted in the "PREFERRED VERSION" column.

API GROUP                 CURRENT VERSION   PREFERRED VERSION   MANUAL UPGRADE REQUIRED
kubeproxy.config.k8s.io   v1alpha1          v1alpha1            no
kubelet.config.k8s.io     v1beta1           v1beta1             no
_____________________________________________________________________

cloud_user@9f7423f9381c:~/Downloads$ 



```

it says that latest stable version in `1.20.x` family is `v1.20.10`. let us drain the node and upgrade the kubernetes cluster to a newer version 

```
cloud_user@9f7423f9381c:~/Downloads$ kubectl get nodes
NAME                           STATUS   ROLES    AGE   VERSION
9f7423f9381c.mylabserver.com   Ready    master   25m   v1.19.0
cloud_user@9f7423f9381c:~/Downloads$ kubectl drain 9f7423f9381c.mylabserver.com --ignore-daemonsets
node/9f7423f9381c.mylabserver.com cordoned
WARNING: ignoring DaemonSet-managed Pods: kube-system/kube-flannel-ds-blcdb, kube-system/kube-proxy-ljqvq
node/9f7423f9381c.mylabserver.com drained
cloud_user@9f7423f9381c:~/Downloads$ kubectl get nodes
NAME                           STATUS                     ROLES    AGE   VERSION
9f7423f9381c.mylabserver.com   Ready,SchedulingDisabled   master   25m   v1.19.0
cloud_user@9f7423f9381c:~/Downloads$ 


cloud_user@9f7423f9381c:~/Downloads$ sudo kubeadm upgrade apply v1.20.10
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade/version] You have chosen to change the cluster version to "v1.20.10"
[upgrade/versions] Cluster version: v1.19.14
[upgrade/versions] kubeadm version: v1.20.0
[upgrade/version] FATAL: the --version argument is invalid due to these errors:

        - Specified version to upgrade to "v1.20.10" is higher than the kubeadm version "v1.20.0". Upgrade kubeadm first using the tool you used to install kubeadm

Can be bypassed if you pass the --force flag
To see the stack trace of this error execute with --v=5 or higher

```

Oops! it won't be possible to upgrade the cluster to `1.20.10` using `1.20.0` package. Therefore, it needs to be upgraded the desired version

```
#Centos
VER=1.20.10-0
sudo yum install kubectl kubeadm-$VER --disableexcludes kubernetes



cloud_user@9f7423f9381c:~/Downloads$ VER=1.20.10-00
cloud_user@9f7423f9381c:~/Downloads$ sudo apt install kubeadm=$VER kubectl
Reading package lists... Done
Building dependency tree       
Reading state information... Done
kubectl is already the newest version (1.22.1-00).
The following packages will be upgraded:
  kubeadm kubelet
2 upgraded, 0 newly installed, 0 to remove and 10 not upgraded.
Need to get 26.6 MB of archives.
After this operation, 173 kB of additional disk space will be used.
Do you want to continue? [Y/n] y
Get:1 https://packages.cloud.google.com/apt kubernetes-xenial/main amd64 kubelet amd64 1.20.10-00 [18.9 MB]
Get:2 https://packages.cloud.google.com/apt kubernetes-xenial/main amd64 kubeadm amd64 1.20.10-00 [7705 kB]
Fetched 26.6 MB in 1s (23.7 MB/s) 
(Reading database ... 200046 files and directories currently installed.)
Preparing to unpack .../kubelet_1.20.10-00_amd64.deb ...
Unpacking kubelet (1.20.10-00) over (1.20.0-00) ...
Preparing to unpack .../kubeadm_1.20.10-00_amd64.deb ...
Unpacking kubeadm (1.20.10-00) over (1.20.0-00) ...
Setting up kubelet (1.20.10-00) ...
Setting up kubeadm (1.20.10-00) ...
cloud_user@9f7423f9381c:~/Downloads$ sudo kubeadm upgrade apply v1.20.10
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade/version] You have chosen to change the cluster version to "v1.20.10"
[upgrade/versions] Cluster version: v1.19.14
[upgrade/versions] kubeadm version: v1.20.10
[upgrade/confirm] Are you sure you want to proceed with the upgrade? [y/N]: y
[upgrade/prepull] Pulling images required for setting up a Kubernetes cluster
[upgrade/prepull] This might take a minute or two, depending on the speed of your internet connection
[upgrade/prepull] You can also perform this action in beforehand using 'kubeadm config images pull'

[upgrade/apply] Upgrading your Static Pod-hosted control plane to version "v1.20.10"...
Static pod: kube-apiserver-9f7423f9381c.mylabserver.com hash: 306ffcfa2eb99185c961998445b5baba
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
[upgrade/etcd] Upgrading to TLS for etcd
Static pod: etcd-9f7423f9381c.mylabserver.com hash: cb07732a67efb9c362a23c24bb661d15
[upgrade/staticpods] Preparing for "etcd" upgrade
[upgrade/staticpods] Renewing etcd-server certificate
[upgrade/staticpods] Renewing etcd-peer certificate
[upgrade/staticpods] Renewing etcd-healthcheck-client certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/etcd.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2021-08-20-03-09-40/etcd.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: etcd-9f7423f9381c.mylabserver.com hash: cb07732a67efb9c362a23c24bb661d15
Static pod: etcd-9f7423f9381c.mylabserver.com hash: cb07732a67efb9c362a23c24bb661d15
Static pod: etcd-9f7423f9381c.mylabserver.com hash: eeccd89710c8728ffe097fcd838b8092
[apiclient] Found 1 Pods for label selector component=etcd
[upgrade/staticpods] Component "etcd" upgraded successfully!
[upgrade/etcd] Waiting for etcd to become available
[upgrade/staticpods] Writing new Static Pod manifests to "/etc/kubernetes/tmp/kubeadm-upgraded-manifests941425898"
[upgrade/staticpods] Preparing for "kube-apiserver" upgrade
[upgrade/staticpods] Renewing apiserver certificate
[upgrade/staticpods] Renewing apiserver-kubelet-client certificate
[upgrade/staticpods] Renewing front-proxy-client certificate
[upgrade/staticpods] Renewing apiserver-etcd-client certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-apiserver.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2021-08-20-03-09-40/kube-apiserver.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-apiserver-9f7423f9381c.mylabserver.com hash: 306ffcfa2eb99185c961998445b5baba
Static pod: kube-apiserver-9f7423f9381c.mylabserver.com hash: 306ffcfa2eb99185c961998445b5baba
Static pod: kube-apiserver-9f7423f9381c.mylabserver.com hash: 306ffcfa2eb99185c961998445b5baba
Static pod: kube-apiserver-9f7423f9381c.mylabserver.com hash: 306ffcfa2eb99185c961998445b5baba
Static pod: kube-apiserver-9f7423f9381c.mylabserver.com hash: 84b3ad53c2c9c51a0d22d16a4de7c464
[apiclient] Found 1 Pods for label selector component=kube-apiserver
[upgrade/staticpods] Component "kube-apiserver" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-controller-manager" upgrade
[upgrade/staticpods] Renewing controller-manager.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-controller-manager.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2021-08-20-03-09-40/kube-controller-manager.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: b19cd16730faca86ae1431c7e7916998
Static pod: kube-controller-manager-9f7423f9381c.mylabserver.com hash: dcac11caa21c3f362cae65b8b8f5f807
[apiclient] Found 1 Pods for label selector component=kube-controller-manager
[upgrade/staticpods] Component "kube-controller-manager" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-scheduler" upgrade
[upgrade/staticpods] Renewing scheduler.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-scheduler.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2021-08-20-03-09-40/kube-scheduler.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: 95a200da190dc83220cab7c0a0910814
Static pod: kube-scheduler-9f7423f9381c.mylabserver.com hash: a861cb849f637ee86b15e52d95cfff1c
[apiclient] Found 1 Pods for label selector component=kube-scheduler
[upgrade/staticpods] Component "kube-scheduler" upgraded successfully!
[upgrade/postupgrade] Applying label node-role.kubernetes.io/control-plane='' to Nodes with label node-role.kubernetes.io/master='' (deprecated)
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.20" in namespace kube-system with the configuration for the kubelets in the cluster
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

[upgrade/successful] SUCCESS! Your cluster was upgraded to "v1.20.10". Enjoy!

[upgrade/kubelet] Now that your control plane is upgraded, please proceed with upgrading your kubelets if you haven't already done so.

#now it is time to upgrade kubelet

#Centos
VER=1.20.10-0
sudo yum install kubectl kubelet-$VER --disableexcludes kubernetes



cloud_user@9f7423f9381c:~/Downloads$ VER=1.20.10-00
cloud_user@9f7423f9381c:~/Downloads$ sudo apt install kubelet=$VER kubectl
cloud_user@9f7423f9381c:~/Downloads$ sudo systemctl enable kubelet && sudo systemctl daemon-reload && sudo systemctl restart kubelet



cloud_user@9f7423f9381c:~/Downloads$ kubectl get nodes
NAME                           STATUS   ROLES                  AGE    VERSION
9f7423f9381c.mylabserver.com   Ready    control-plane,master   8m8s   v1.20.10
cloud_user@9f7423f9381c:~/Downloads$ kubectl get nodes
NAME                           STATUS   ROLES                  AGE     VERSION
9f7423f9381c.mylabserver.com   Ready    control-plane,master   8m27s   v1.20.10
cloud_user@9f7423f9381c:~/Downloads$ 

```


Similarly, `1.20.x` to `1.21.x` can be done using the above path. 

path would be 

1. upgrade to `x.y.z` `kubeadm`, and `kubectl (optional)` packages 

2. run `sudo kubeadm upgrade plan`

3. if latest stable version from upgrade plan's output is matching with `x.y.z` of the package, please proceed with the next step. Otherwise, the package version needs to be upgraded to match with the output.

4. drain the node and run `kubeadm upgrade apply vX.Y.Z`

5. now, it is time to upgrade `kubelet` and needs daemon,kubelet need to be restarted

Done !!!


