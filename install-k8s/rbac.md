# README #

<< [back to Home](../) >>

List
- create role, rolebinding
- create cert and sign those certs to provide to user as kubeconfig (working on)
- 


## what is RBAC?
source [Kubernetes.io](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) 

Role-based access control (RBAC) is a method of regulating access to computer or network resources based on the roles of individual users within your organization.

RBAC authorization uses the rbac.authorization.k8s.io API group to drive authorization decisions, allowing you to dynamically configure policies through the Kubernetes API.

To enable RBAC, start the API server with the --authorization-mode flag set to a comma-separated list that includes RBAC; for example:


## RBAC is not working. how to check configuration?
```
# please visit kubeapi server manifest file under /etc/kubernetes/manifests/

kube-apiserver --authorization-mode=Example,RBAC --other-options --more-options


```


# Operations
source [api document](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.21/)

- Get: Get will retrieve a specific resource object by name.

- List: List will retrieve all resource objects of a specific type within a namespace, and the results can be restricted to resources matching a selector query.

  List All Namespaces: Like List but retrieves resources across all namespaces.

- Watch: Watch will stream results for an object(s) as it is updated. Similar to a callback, watch is used to respond to resource changes.

- Delete: Delete will delete a resource. Depending on the specific resource, child objects may or may not be garbage collected by the server. See notes on specific resource objects for details.

### Update

Updates come in 2 forms: Replace and Patch:

- Replace: Replacing a resource object will update the resource by replacing the existing spec with the provided one. For read-then-write operations this is safe because an optimistic lock failure will occur if the resource was modified between the read and write. Note: The ResourceStatus will be ignored by the system and will not be updated. To update the status, one must invoke the specific status update operation.

  Note: Replacing a resource object may not result immediately in changes being propagated to downstream objects. For instance replacing a `ConfigMap` or `Secret` resource will not result in all Pods seeing the changes unless the Pods are restarted out of band.

- Patch: Patch will apply a change to a specific field. How the change is merged is defined per field. Lists may either be replaced or merged. Merging lists will not preserve ordering.
Patches will never cause optimistic locking failures, and the last write will win. Patches are recommended when the full state is not read before an update, or when failing on optimistic locking is undesirable. When patching complex types, arrays and maps, how the patch is applied is defined on a per-field basis and may either replace the field's current value, or merge the contents into the current value.

---


![rbac groups](./rbacobj.png)

![auth methods](./auth_methods_k8s.png)

---


- how to create user?
  
  * CSR signing approach

    Source
       [Create Sign request with CSR](https://thenewstack.io/a-practical-approach-to-understanding-kubernetes-authentication/)
  
  
  * external CSR, CA, key generation

  1. create user CSR

  
   

  ```
  openssl genrsa -out user1.key 2048
  openssl req -new -key user1.key -out user1.csr
  ```

  2. create user cert


  ```
  openssl x509 -req -in user1.csr -CA /etc/kubernetes/pki/ca.crt -CAkey /etc/kubernetes/pki/ca.key -CAcreateserial -out user1.crt -days 500

  ```

  3. use above user certs as kubeconfig


  ```
  kubectl config set-credentials user1 --client-certificate=/root/user1.crt --client-key=user1.key

  #assuming role and rolebinding have been created under namespace test-namespace
  kubectl config set-context user1-context --cluster=kubernetes --namespace=test-namespace --user=user1

  ```


- Groups

  Recommended reading : [Google GKE RBAC](https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control)

- How to create service account?

  ```
  kubectl create sa serviceaccountname -n namespacename
  ```


e.g.
task: 

user `user1` (developer) just needs access to `list` pods under ns `test`

```

# create ns
kubectl create ns test

```


Create `role` `rolebinding`

```

kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  namespace: test
  name: developer
rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list"]
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: user1-binding
subjects:
- kind: User
  name: user1
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: developer
  apiGroup: rbac.authorization.k8s.io

```

  Practice: 
  make changes to allow `user1` to create pods only in `test` ns. 

---


# how to check access using `kubectl auth can-i`

- `auth can-i`  command will help to impersonate the user so access can be verified as follows. 

```
controlplane $ kubectl create ns abc
namespace/abc created

controlplane $ kubectl get sa -n abc
NAME      SECRETS   AGE
default   1         6s

controlplane $ kubectl auth can-i  get pods --as=system:serviceaccount:abc:default -n abc
yes

```


`--as` user impersonate

`--as-group` group impersonate
