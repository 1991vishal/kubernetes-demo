# README #

<< [back to Home](../) >>

Demo servers
```
user: cloud_user
master: 172.31.103.68 hostname 1472290c711c
worker 1: 172.31.105.193 hostname 44ad6fae5e1c
worker 2: 172.31.107.231 hostname 0894b355d51c
```
environent
```
cloud_user@1472290c711c:~/Downloads$ cat /etc/*release
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=20.04
```

Each server should have docker,open-iscsi installed. 

source : [install docker runtime](https://kubernetes.io/docs/setup/production-environment/container-runtimes/) , [install kubeadm-kubectl-kubelet](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

```
#ubuntu/debian
sudo apt update && sudo apt install -y docker.io open-iscsi apt-transport-https ca-certificates curl
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update

#centos7/RHEL7
sudo yum install -y yum-utils
curl -fsSL https://get.docker.com | sudo bash -
sudo yum install -y iscsi-initiator-utils
cat <<EOF | sudo tee /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config


sudo systemctl enable docker
sudo service docker restart
sudo chmod 666 /var/run/docker.sock
```

tune docker to use systemd and overlay2
on each server
```
sudo mkdir -p /etc/docker
cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
```

to apply `daemon.json` change, docker needs to be restarted

```
sudo service docker restart && sudo chmod 666 /var/run/docker.sock
```

(if docker restart fails) In case if you are unable to restart docker, it could be storage driver `overlay2` issue. For now for testing, please delete 
`sudo /etc/docker/daemon.json` and trying running `sudo service docker restart && sudo chmod 666 /var/run/docker.sock`

why can't I set storage-driver to overlay2 [link1](https://medium.com/@khushalbisht/docker-on-centos-7-with-xfs-filesystem-can-cause-trouble-when-d-type-is-not-supported-64cee61b39ab) [link2](https://docs.docker.com/storage/storagedriver/overlayfs-driver/)

on all nodes, 

Letting iptables see bridged traffic [source](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#letting-iptables-see-bridged-traffic)

```
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF

cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system

#swapoff
sudo swapoff -a  && sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab


```

If firewall is in place, please refer to the guide to open connection [list of required ports](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports)

&nbsp;

[Install Kubeadm, kubectl, kubelet](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#installing-kubeadm-kubelet-and-kubectl)

```
#ubuntu - latest
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl

#ubuntu - install specific version
VER=1.20.0-00
sudo apt list -a kubelet
sudo apt install kubeadm=$VER kubelet=$VER kubectl



#RHEL7/CentOS7 - latest
sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
sudo systemctl enable --now kubelet

```



# create the cluster 

On master node

[kubeadm-init](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/#initializing-your-control-plane-node)
[cidr-range](https://gist.github.com/rkaramandi/44c7cea91501e735ea99e356e9ae7883#configure-kubernetes-master)
```
kubeadm init --pod-network-cidr=10.244.0.0/16
```
upon successful `kubeadm init` command, 
```
sudo cp /etc/kubernetes/admin.conf $HOME/
sudo chown $(id -u):$(id -g) $HOME/admin.conf
export KUBECONFIG=$HOME/admin.conf
```

now, let us setup `cni`

[list of network cni](https://kubernetes.io/docs/concepts/cluster-administration/networking/)

```
kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml

```

Now, let us enable, reload dameon and restart kubelet

```
sudo systemctl enable kubelet
sudo systemctl daemon-reload && sudo systemctl restart kubelet
```

To add further node as worker node 

first, on master node
```
kubeadm token create --print-join-command
```

copy the output of command and then that command on the worker node which needs to be added to the cluster. 

#

in case if fails or needs to be reset. It is not recommended option if you're appearing for the CKA certification. 

```
sudo kubeadm reset -f
sudo rm -rf /etc/cni /etc/kubernetes /var/lib/dockershim /var/lib/etcd /var/lib/kubelet /var/run/kubernetes ~/.kube/*
sudo iptables -F && sudo iptables -X
sudo iptables -t nat -F && sudo iptables -t nat -X
sudo iptables -t raw -F && sudo iptables -t raw -X
sudo iptables -t mangle -F && sudo iptables -t mangle -X
sudo docker system prune -a -f
sudo docker volume prune -f
sudo systemctl stop kubelet
sudo systemctl stop docker
sudo iptables --flush
sudo iptables -tnat --flush
sudo systemctl enable kubelet
sudo systemctl start kubelet
sudo systemctl restart docker && sudo chmod 666 /var/run/docker.sock
```
#

> How I did it? Partial automation

1. (optional) creating file with all server ip under `/tmp/serverip`

```
cloud_user@1472290c711c:~$ echo -e "172.31.103.68\n172.31.105.193\n172.31.107.231" > /tmp/serverip
cloud_user@1472290c711c:~$ cat /tmp/serverip 
172.31.103.68
172.31.105.193
172.31.107.231
```

2. (optional) let us setup passwordless ssh to the above server. 

```
cloud_user@1472290c711c:~$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/cloud_user/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/cloud_user/.ssh/id_rsa
Your public key has been saved in /home/cloud_user/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:djSIML2BPsq9/LmxydFPwNDBkBgXXw2XLOQ6Yyqj7JA cloud_user@1472290c711c.mylabserver.com
The key's randomart image is:
+---[RSA 3072]----+
|    +=+=..++..   |
|    o+++.=..+    |
|   .  oo+ +.     |
|    o .o o .     |
| . o .  S .      |
| .o .  = =       |
|E  .o.+ . .      |
| o .o+ * o       |
| .+  .B.  .      |
+----[SHA256]-----+
cloud_user@1472290c711c:~$ ls -a /home/cloud_user/.ssh/
.  ..  authorized_keys  id_rsa  id_rsa.pub
cloud_user@1472290c711c:~$
```

copying public key to all the servers and will test login using ssh key pair

```
cloud_user@1472290c711c:~$ cat /tmp/serverip | while read eachip; do ssh-copy-id cloud_user@$eachip; done
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/cloud_user/.ssh/id_rsa.pub"
The authenticity of host '172.31.103.68 (172.31.103.68)' can't be established.
ECDSA key fingerprint is SHA256:+2gnMhUgUQs0lgMtd/1sZtd0CVSww0Ut0VT62wH+6DM.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
Password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'cloud_user@172.31.103.68'"
and check to make sure that only the key(s) you wanted were added.

/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/cloud_user/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
Password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'cloud_user@172.31.105.193'"
and check to make sure that only the key(s) you wanted were added.

/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/cloud_user/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
Password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'cloud_user@172.31.107.231'"
and check to make sure that only the key(s) you wanted were added.

#testing ssh key pair

loud_user@1472290c711c:~$ eval `ssh-agent`
Agent pid 4632
cloud_user@1472290c711c:~$ ssh-add
Identity added: /home/cloud_user/.ssh/id_rsa (cloud_user@1472290c711c.mylabserver.com)
cloud_user@1472290c711c:~$ ssh cloud_user@$(cat /tmp/serverip | head -n 1) hostname
1472290c711c.mylabserver.com
cloud_user@1472290c711c:~$ ssh cloud_user@$(cat /tmp/serverip | tail -n 1) 'hostname'
0894b355d51c.mylabserver.com
cloud_user@1472290c711c:~$ ssh cloud_user@$(cat /tmp/serverip | head -n 2 | tail -n 1) 'hostname'
44ad6fae5e1c.mylabserver.com
cloud_user@1472290c711c:~$ 
```


3. (mandatory) install docker and open-iscsi (k8s storage system)

```

USER1="cloud_user"
COMMAND1="curl -sfL https://bitbucket.org/1991vishal/kubernetes-demo/raw/8e60c1b982e93a3dd2f9efcef624198c3168bc7a/install-k8s/install-k8s.sh"


#downloading scripts on all servers

date > output.log

for host in $(cat /tmp/serverip); do echo -e "\n \n \n" >> "output.log" && echo "$host" >> "output.log" && echo -e "\n" >> "output.log" && ssh -l "$USER1" "$host" "$COMMAND1 | tee /tmp/install.sh" >>"output.log"; done

#installing scripts on all servers. 

INSTALLSC="bash /tmp/install.sh"
PW1="password_of_node" # this would require to initiate apt command
for host in $(cat /tmp/serverip); do echo -e "\n \n \n" >> "output.log" && echo "$host" >> "output.log" && echo -e "\n" >> "output.log" && ssh -l "$USER1" "$host" "$INSTALLSC $PW1" >>"output.log"; done

```

4. (mandatory) Creating kubernetes cluster using kubeadm. 
Now please follow instruction mentioned above under # create the cluster. 

