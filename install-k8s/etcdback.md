# README #

<< [back to Home](../) >>


Assuming that cluster is running in healthy state and etcdctl is installed on master node 

How to install etcdctl?


## what is etcd? 
source [IBM](https://www.ibm.com/cloud/learn/etcd)

etcd is an open source distributed key-value store used to hold and manage the critical information that distributed systems need to keep running. Most notably, it manages the configuration data, state data, and metadata for Kubernetes, the popular container orchestration platform.

### Topics
- key value store example
- setup etcd and show how keys,values are stored - change stored as incremental revision 

```
wget https://github.com/etcd-io/etcd/releases/download/v3.3.11/etcd-v3.3.11-linux-amd64.tar.gz
tar xzvf etcd-v3.3.11-linux-amd64.tar.gz
./etcd
./etcdctl set key001 value001
./etcdctl get key001
```


Installation of etcdctl
```
ETCD_VER=v3.5.0

GOOGLE_URL=https://storage.googleapis.com/etcd
GITHUB_URL=https://github.com/etcd-io/etcd/releases/download
DOWNLOAD_URL=${GOOGLE_URL}

rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
rm -rf /tmp/etcd-download-test && mkdir -p /tmp/etcd-download-test

curl -L ${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
tar xzvf /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz -C /tmp/etcd-download-test --strip-components=1
rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
sudo cp /tmp/etcd-download-test/etcdctl /usr/bin/etcdctl 
etcdctl  version

```

## Backup etcd

Whenever the cluster is sent for maintenance, it is recommended to take backup of the cluster state before taking any admin action. 

In order to take backup, request going to etcd should be authenticated. Manifest details of etcd are under `/etc/kubernetes/manifests/etcd.yaml`.
The above file will give all cert, configuration details of running etcd component.

`/etc/kubernetes/manifests/etcd.yaml`

```
apiVersion: v1
kind: Pod
metadata:
  annotations:
    kubeadm.kubernetes.io/etcd.advertise-client-urls: https://10.17.249.9:2379
  creationTimestamp: null
  labels:
    component: etcd
    tier: control-plane
  name: etcd
  namespace: kube-system
spec:
  containers:
  - command:
    - etcd
    - --advertise-client-urls=https://10.17.249.9:2379
    - --cert-file=/etc/kubernetes/pki/etcd/server.crt
    - --client-cert-auth=true
    - --data-dir=/var/lib/etcd
    - --initial-advertise-peer-urls=https://10.17.249.9:2380
    - --initial-cluster=controlplane=https://10.17.249.9:2380
    - --key-file=/etc/kubernetes/pki/etcd/server.key
    - --listen-client-urls=https://127.0.0.1:2379,https://10.17.249.9:2379
    - --listen-metrics-urls=http://127.0.0.1:2381
    - --listen-peer-urls=https://10.17.249.9:2380
    - --name=controlplane
    - --peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt
    - --peer-client-cert-auth=true
    - --peer-key-file=/etc/kubernetes/pki/etcd/peer.key
    - --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    - --snapshot-count=10000
    - --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    image: k8s.gcr.io/etcd:3.4.13-0
    imagePullPolicy: IfNotPresent
    livenessProbe:
      failureThreshold: 8
      httpGet:
        host: 127.0.0.1
        path: /health
        port: 2381
        scheme: HTTP
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    name: etcd
    resources:
      requests:
        cpu: 100m
        ephemeral-storage: 100Mi
        memory: 100Mi
    startupProbe:
      failureThreshold: 24
      httpGet:
        host: 127.0.0.1
        path: /health
        port: 2381
        scheme: HTTP
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    volumeMounts:
    - mountPath: /var/lib/etcd
      name: etcd-data
    - mountPath: /etc/kubernetes/pki/etcd
      name: etcd-certs
  hostNetwork: true
  priorityClassName: system-node-critical
  volumes:
  - hostPath:
      path: /etc/kubernetes/pki/etcd
      type: DirectoryOrCreate
    name: etcd-certs
  - hostPath:
      path: /var/lib/etcd
      type: DirectoryOrCreate
    name: etcd-data
status: {}

```

Task: 
Create copy of current etcd database and save it to `/tmp/snapshot.db`


```
root@controlplane:~# ETCDCTL_API=3 etcdctl --endpoints=https://[127.0.0.1]:2379 \
> --cacert=/etc/kubernetes/pki/etcd/ca.crt \
> --cert=/etc/kubernetes/pki/etcd/server.crt \
> --key=/etc/kubernetes/pki/etcd/server.key \
> snapshot save /tmp/snapshot.db
Snapshot saved at /tmp/snapshot.db
```

Verify the backup of etcd

```
root@controlplane:~# ETCDCTL_API=3 etcdctl --write-out=table snapshot status /tmp/snapshot.db 
+----------+----------+------------+------------+
|   HASH   | REVISION | TOTAL KEYS | TOTAL SIZE |
+----------+----------+------------+------------+
| 200ef407 |     1738 |        848 |     2.5 MB |
+----------+----------+------------+------------+
root@controlplane:~# 
```

#
## Restoring etcd

Now let us say any disaster occured and you would like to restore etcd back to previous state

```
root@controlplane:~#  ETCDCTL_API=3 etcdctl  --data-dir /var/lib/etcd-from-backup \
> snapshot restore /tmp/snapshot.db 

2021-08-31 10:16:48.469539 I | mvcc: restore compact to 1296
2021-08-31 10:16:48.478205 I | etcdserver/membership: added member 8e9e05c52164694d [http://localhost:2380] to cluster cdf818194e3a8c32
root@controlplane:~# 

```

it is notable that data dir is set to old state (`/var/lib/etcd`). So, it needs to be changed to  `/var/lib/etcd-from-backup`

```
vi /etc/kubernetes/manifests/etcd.yaml 

#please update path as follows. 

...
volumes:
  - hostPath:
      path: /var/lib/etcd-from-backup
      type: DirectoryOrCreate
    name: etcd-data
```

After saving this change, this will take some time 1-2 minutes. Workloads can be watched by running `watch "docker ps | grep etcd"`

` Note: in above command, it may fail the restore process due to auth error. In that situation, the complete command needs to be passed like ca cert, server cert and its keys. These details will be mentioned along with the backup or can be obtained from /etc/kubernetes/manifest directory` 
 
 The official document from `etcd.io` will explain ways of passing files usign `env` var
 i.e.

```
export ETCDCTL_CACERT=/etc/kubernetes/pki/etcd/ca.crt
export ETCDCTL_CERT=/etc/kubernetes/pki/etcd/server.crt
export ETCDCTL_KEY=/etc/kubernetes/pki/etcd/server.key
export ETCDCTL_API=3
```

 In some situation, it is recommended to create a separate 