
# README #

<< [back to Home](../) >>


Go app - Source from [CodeFresh repo](https://github.com/codefresh-contrib/golang-sample-app/blob/master/Dockerfile.multistage)

Codefresh is CI/CD platform that help buiding docker images in pipeline - more like automatic way

```
FROM golang:1.12-alpine AS build_base

RUN apk add --no-cache git

# Set the Current Working Directory inside the container
WORKDIR /tmp/go-sample-app

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# Unit tests
RUN CGO_ENABLED=0 go test -v

# Build the Go app
RUN go build -o ./out/go-sample-app .

# Start fresh from a smaller image
FROM alpine:3.9 
RUN apk add ca-certificates


COPY --from=build_base /tmp/go-sample-app/out/go-sample-app /app/go-sample-app

# This container exposes port 8080 to the outside world
EXPOSE 8080

# Run the binary program produced by `go install`
CMD ["/app/go-sample-app"]
```


### [APP folder structure](app)

```
│
└───app
        go.mod
        go.sum
        hello_server.go
        hello_server_test.go
        Dockerfile
```

### Why multi-stage builds??

- [10 Docker Security Best Practices](https://snyk.io/blog/10-docker-image-security-best-practices/)
: By leveraging Docker support for multi-stage builds, fetch and manage secrets in an intermediate image layer that is later disposed of so that no sensitive data reaches the image build. Use code to add secrets to said intermediate layer 

- [Node.js Docker Cheat Sheet](https://cheatsheetseries.owasp.org/cheatsheets/NodeJS_Docker_Cheat_Sheet.html#8-use-multi-stage-builds) : use a bigger Docker base image to install our dependencies, compile any native  packages if needed, and then copy all these artifacts into a small production base image, like our alpine example.


---

Now, we have app code and dockerfile ready. let us build docker image and push to the private registry. 

### Building of Docker Image

as per manual, it is mandatory to pass imagename with tag (`-t`) and location of app, Dockerfile. 
Location of App and Dockerfile are pointed by `.` which means the current director. 

`docker build -t imagename:tag .`

If Dockerfile and app are located to a different dir, command can be as follows

`docker build -t imagename:tag -f Dockerfile-Location /path_of_application`



```
## cd into app and checking contents of Dockerfile
controlplane $ pwd
/root/kubernetes-demo/docker/app

controlplane $ ls ; cat Dockerfile ;echo


Dockerfile  go.mod  go.sum  hello_server.go  hello_server_test.go
FROM golang:1.12-alpine AS build_base

RUN apk add --no-cache git

# Set the Current Working Directory inside the container
WORKDIR /tmp/go-sample-app

# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

# Unit tests
RUN CGO_ENABLED=0 go test -v

# Build the Go app
RUN go build -o ./out/go-sample-app .

# Start fresh from a smaller image
FROM alpine:3.9 
RUN apk add ca-certificates


COPY --from=build_base /tmp/go-sample-app/out/go-sample-app /app/go-sample-app

# This container exposes port 8080 to the outside world
EXPOSE 8080

# Run the binary program produced by `go install`
CMD ["/app/go-sample-app"]

```

### Docker build output

imagename is set to `demo.goharbor.io/goapp/go-app:v1` because private registry is hosted at `demo

```

controlplane $ docker build -t demo.goharbor.io/goapp/go-app:v1 .
Sending build context to Docker daemon   7.68kB
Step 1/14 : FROM golang:1.12-alpine AS build_base
1.12-alpine: Pulling from library/golang
c9b1b535fdd9: Pull complete 
cbb0d8da1b30: Pull complete 
d909eff28200: Pull complete 
665fbbf998e4: Pull complete 
4985b1919860: Pull complete 
Digest: sha256:3f8e3ad3e7c128d29ac3004ac8314967c5ddbfa5bfa7caa59b0de493fc01686a
Status: Downloaded newer image for golang:1.12-alpine
 ---> 76bddfb5e55e
Step 2/14 : RUN apk add --no-cache git
 ---> Running in 84ce30578094
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/community/x86_64/APKINDEX.tar.gz
(1/5) Installing nghttp2-libs (1.40.0-r1)
(2/5) Installing libcurl (7.79.1-r0)
(3/5) Installing expat (2.2.9-r1)
(4/5) Installing pcre2 (10.34-r1)
(5/5) Installing git (2.24.4-r0)
Executing busybox-1.31.1-r9.trigger
OK: 22 MiB in 20 packages
Removing intermediate container 84ce30578094
 ---> 01911ea87e78
Step 3/14 : WORKDIR /tmp/go-sample-app
 ---> Running in 69efac7e4ea9
Removing intermediate container 69efac7e4ea9
 ---> e6f74587c058
Step 4/14 : COPY go.mod .
 ---> c136f7da1cd2
Step 5/14 : COPY go.sum .
 ---> 3a5e027ad134
Step 6/14 : RUN go mod download
 ---> Running in 22514bb2a048
go: finding github.com/gorilla/mux v1.6.2
go: finding github.com/gorilla/context v1.1.1
Removing intermediate container 22514bb2a048
 ---> 6268e13c0eaf
Step 7/14 : COPY . .
 ---> f7fa4e5da97d
Step 8/14 : RUN CGO_ENABLED=0 go test -v
 ---> Running in 4dde6b7e86e0
=== RUN   TestGreetingSpecific
--- PASS: TestGreetingSpecific (0.00s)
=== RUN   TestGreetingDefault
--- PASS: TestGreetingDefault (0.00s)
PASS
ok      github.com/codefresh-contrib/go-sample-app      0.003s
Removing intermediate container 4dde6b7e86e0
 ---> fd02182d2a31
Step 9/14 : RUN go build -o ./out/go-sample-app .
 ---> Running in 8200c2427294
Removing intermediate container 8200c2427294
 ---> 2075e34a9c4d
Step 10/14 : FROM alpine:3.9
3.9: Pulling from library/alpine
31603596830f: Pull complete 
Digest: sha256:414e0518bb9228d35e4cd5165567fb91d26c6a214e9c95899e1e056fcd349011
Status: Downloaded newer image for alpine:3.9
 ---> 78a2ce922f86
Step 11/14 : RUN apk add ca-certificates
 ---> Running in 456aa0fff9c6
fetch http://dl-cdn.alpinelinux.org/alpine/v3.9/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.9/community/x86_64/APKINDEX.tar.gz
(1/1) Installing ca-certificates (20191127-r2)
Executing busybox-1.29.3-r10.trigger
Executing ca-certificates-20191127-r2.trigger
OK: 6 MiB in 15 packages
Removing intermediate container 456aa0fff9c6
 ---> 1a5537ad0fb1
Step 12/14 : COPY --from=build_base /tmp/go-sample-app/out/go-sample-app /app/go-sample-app
 ---> 7c83d487082c
Step 13/14 : EXPOSE 8080
 ---> Running in f9714134550f
Removing intermediate container f9714134550f
 ---> e9ab0f421dbf
Step 14/14 : CMD ["/app/go-sample-app"]
 ---> Running in a00101f52a8b
Removing intermediate container a00101f52a8b
 ---> 6aadf54762e7
Successfully built 6aadf54762e7
Successfully tagged demo.goharbor.io/goapp/go-app:v1
controlplane $ 

#image will be stored locally
controlplane $ docker images | grep -i harbor
demo.goharbor.io/goapp/go-app                         v1                  3485cc1d8c37        19 seconds ago      15.2MB
controlplane $ 
```

### Test docker image 

```
controlplane $ docker run -d -p 8080:8080 demo.goharbor.io/goapp/go-app:v1 
23fa5cbc4b20a87a5954f255f377f8f73bcf1c00cb24c579396ea38b0e5c9e42

controlplane $ curl localhost:8080
Hello, Guest
controlplane $ 
```

### Push docker image to goharbor. 

```
controlplane $ docker login demo.goharbor.io -u 'robot$goapp+push' -p 'rV0UENvfREeAAkG2TCZcUS6t2gEUyW6X'
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
controlplane $

controlplane $ docker push demo.goharbor.io/goapp/go-app:v1The push refers to repository [demo.goharbor.io/goapp/go-app]
a99d50d38abd: Pushed 
294f40fad94d: Pushed 
89ae5c4ee501: Pushed 
v1: digest: sha256:56a3b467f0b4132ddca630b82fe47486f7e3a9e456ffa60316102b2f1d83dbd3 size: 950
controlplane $ 

```


### To check logged account under `~/.docker/config.json` 


```
controlplane $ cat ~/.docker/config.json 
{
        "auths": {
                "demo.goharbor.io": {
                        "auth": "cm9ib3QkZ29hcHArcHVzaDpyVjBVRU52ZlJFZUFBa0cyVENaY1VTNnQyZ0VVeVc2WA=="
                }
        },
        "HttpHeaders": {
                "User-Agent": "Docker-Client/18.09.7 (linux)"
        }
}controlplane $
```


### Scan result - Goharbor

private registry can prevent vulnerable image from pulling for security purposes. 

![goharbor-scan-result](scan-result.png)

