
# README #

<< [back to Home](../) >>


[**Docker Compose**](https://www.tutorialspoint.com/docker/docker_compose.htm) is used to run multiple containers as a single service. For example, suppose you had an application which required NGNIX and MySQL, you could create one file which would start both the containers as a service without the need to start each one separately.


[Demo](https://docs.docker.com/compose/gettingstarted/)
```
## Requirement
controlplane $ cat requirement.txt 
flask
redis

#Dockerfile
controlplane $ cat Dockerfile 
# syntax=docker/dockerfile:1
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]


## app.py
controlplane $ cat app.py 
import time

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
controlplane $ 

```


`docker-compose.yml`


```
controlplane $ cat docker-compose.yml 
version: "3"
services:
  web:
    build: .
    ports:
      - "5000:5000"
  redis:
    image: "redis:alpine"
controlplane $ 
```

```

controlplane $ docker-compose up
Building web
Step 1/10 : FROM python:3.7-alpine
 ---> a1034fd13493
Step 2/10 : WORKDIR /code
 ---> Using cache
 ---> 0ab556e7a270
Step 3/10 : ENV FLASK_APP=app.py
 ---> Using cache
 ---> b8b05b3dcd1c
Step 4/10 : ENV FLASK_RUN_HOST=0.0.0.0
 ---> Using cache
 ---> 3438e908a0f8
Step 5/10 : RUN apk add --no-cache gcc musl-dev linux-headers
 ---> Using cache
 ---> d277f80ad6e0
Step 6/10 : COPY requirements.txt requirements.txt
 ---> 5c40e07093f1
Step 7/10 : RUN pip install -r requirements.txt
 ---> Running in 81d4dcec7c6b
Collecting flask
  Downloading Flask-2.0.2-py3-none-any.whl (95 kB)
Collecting redis
  Downloading redis-4.1.0-py3-none-any.whl (171 kB)
Collecting Jinja2>=3.0
  Downloading Jinja2-3.0.3-py3-none-any.whl (133 kB)
Collecting Werkzeug>=2.0
  Downloading Werkzeug-2.0.2-py3-none-any.whl (288 kB)
Collecting itsdangerous>=2.0
  Downloading itsdangerous-2.0.1-py3-none-any.whl (18 kB)
Collecting click>=7.1.2
  Downloading click-8.0.3-py3-none-any.whl (97 kB)
Collecting packaging>=21.3
  Downloading packaging-21.3-py3-none-any.whl (40 kB)
Collecting importlib-metadata>=1.0
  Downloading importlib_metadata-4.10.0-py3-none-any.whl (17 kB)
Collecting deprecated>=1.2.3
  Downloading Deprecated-1.2.13-py2.py3-none-any.whl (9.6 kB)
Collecting wrapt<2,>=1.10
  Downloading wrapt-1.13.3-cp37-cp37m-musllinux_1_1_x86_64.whl (78 kB)
Collecting typing-extensions>=3.6.4
  Downloading typing_extensions-4.0.1-py3-none-any.whl (22 kB)
Collecting zipp>=0.5
  Downloading zipp-3.7.0-py3-none-any.whl (5.3 kB)
Collecting MarkupSafe>=2.0
  Downloading MarkupSafe-2.0.1-cp37-cp37m-musllinux_1_1_x86_64.whl (30 kB)
Collecting pyparsing!=3.0.5,>=2.0.2
  Downloading pyparsing-3.0.6-py3-none-any.whl (97 kB)
Installing collected packages: zipp, typing-extensions, wrapt, pyparsing, MarkupSafe, importlib-metadata, Werkzeug, packaging, Jinja2, itsdangerous, deprecated, click, redis, flask
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
Successfully installed Jinja2-3.0.3 MarkupSafe-2.0.1 Werkzeug-2.0.2 click-8.0.3 deprecated-1.2.13 flask-2.0.2 importlib-metadata-4.10.0 itsdangerous-2.0.1 packaging-21.3 pyparsing-3.0.6 redis-4.1.0 typing-extensions-4.0.1 wrapt-1.13.3 zipp-3.7.0
WARNING: You are using pip version 21.2.4; however, version 21.3.1 is available.
You should consider upgrading via the '/usr/local/bin/python -m pip install --upgrade pip' command.
Removing intermediate container 81d4dcec7c6b
 ---> 41f9b698419b
Step 8/10 : EXPOSE 5000
 ---> Running in 6891802afb32
Removing intermediate container 6891802afb32
 ---> 4ccc130e996d
Step 9/10 : COPY . .
 ---> b6e10a69bc92
Step 10/10 : CMD ["flask", "run"]
 ---> Running in ddcd40dab134
Removing intermediate container ddcd40dab134
 ---> b1963a71d92a
Successfully built b1963a71d92a
Successfully tagged docker_web:latest
WARNING: Image for service web was built because it did not already exist. To rebuild this image you must use `docker-compose build` or `docker-compose up --build`.
Pulling redis (redis:alpine)...
alpine: Pulling from library/redis
59bf1c3509f3: Already exists
719adce26c52: Pull completeb8f35e378c31: Pull completed034517f789c: Pull complete3772d4d76753: Pull complete
211a7f52febb: Pull complete
Digest: sha256:4bed291aa5efb9f0d77b76ff7d4ab71eee410962965d052552db1fb80576431d
Status: Downloaded newer image for redis:alpine
Creating docker_web_1
Creating docker_redis_1
Attaching to docker_web_1, docker_redis_1
redis_1  | 1:C 02 Jan 2022 13:41:16.216 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
redis_1  | 1:C 02 Jan 2022 13:41:16.216 # Redis version=6.2.6, bits=64, commit=00000000, modified=0, pid=1, just started
redis_1  | 1:C 02 Jan 2022 13:41:16.216 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
redis_1  | 1:M 02 Jan 2022 13:41:16.217 * monotonic clock: POSIX clock_gettime
redis_1  | 1:M 02 Jan 2022 13:41:16.218 * Running mode=standalone, port=6379.
redis_1  | 1:M 02 Jan 2022 13:41:16.218 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
redis_1  | 1:M 02 Jan 2022 13:41:16.218 # Server initialized
redis_1  | 1:M 02 Jan 2022 13:41:16.218 * Ready to accept connections
web_1    |  * Serving Flask app 'app.py' (lazy loading)
web_1    |  * Environment: production
web_1    |    WARNING: This is a development server. Do not use it in a production deployment.
web_1    |    Use a production WSGI server instead.
web_1    |  * Debug mode: off
web_1    |  * Running on all addresses.
web_1    |    WARNING: This is a development server. Do not use it in a production deployment.
web_1    |  * Running on http://172.19.0.2:5000/ (Press CTRL+C to quit)


```


Output 

```
controlplane $ curl 172.19.0.2:5000
Hello World! I have been seen 1 times.
```