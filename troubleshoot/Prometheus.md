# README #

<< [back to Home](../) >>


Recommended Reading

- [prometheus official Documents](https://prometheus.io/docs/introduction/overview/)

- [Gathering Metrics from Kubernetes with Prometheus and Grafana
](https://tanzu.vmware.com/developer/guides/observability-prometheus-grafana-p1/)

- Youtube

    - [Why use Prometheus? (for monitoring)
](https://www.youtube.com/watch?v=NqUWL_0rQ5Y&ab_channel=TomGregoryTech)

    - [How Fastly used Prometheus to improve infrastructure and application monitoring
](https://www.oreilly.com/library/view/how-fastly-used/0636920452713/video329931.html)


Demo:

- using Vmware tutorial, Installing Prometheus and Grafana dashboard `helm install ...` 

```

➜ kubectl create ns  prometheus
namespace/prometheus created
 
➜ helm install prometheus bitnami/kube-prometheus -n prometheus
NAME: prometheus
LAST DEPLOYED: Fri Jan 14 13:48:44 2022
NAMESPACE: prometheus
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
CHART NAME: kube-prometheus
CHART VERSION: 6.6.0
APP VERSION: 0.53.1

** Please be patient while the chart is being deployed **

Watch the Prometheus Operator Deployment status using the command:

    kubectl get deploy -w --namespace prometheus -l app.kubernetes.io/name=kube-prometheus-operator,app.kubernetes.io/instance=prometheus

Watch the Prometheus StatefulSet status using the command:

    kubectl get sts -w --namespace prometheus -l app.kubernetes.io/name=kube-prometheus-prometheus,app.kubernetes.io/instance=prometheus

Prometheus can be accessed via port "9090" on the following DNS name from within your cluster:

    prometheus-kube-prometheus-prometheus.prometheus.svc.cluster.local

To access Prometheus from outside the cluster execute the following commands:

    echo "Prometheus URL: http://127.0.0.1:9090/"
    kubectl port-forward --namespace prometheus svc/prometheus-kube-prometheus-prometheus 9090:9090

Watch the Alertmanager StatefulSet status using the command:

    kubectl get sts -w --namespace prometheus -l app.kubernetes.io/name=kube-prometheus-alertmanager,app.kubernetes.io/instance=prometheus

Alertmanager can be accessed via port "9093" on the following DNS name from within your cluster:

    prometheus-kube-prometheus-alertmanager.prometheus.svc.cluster.local

To access Alertmanager from outside the cluster execute the following commands:

    echo "Alertmanager URL: http://127.0.0.1:9093/"
    kubectl port-forward --namespace prometheus svc/prometheus-kube-prometheus-alertmanager 9093:9093

```


```

➜ helm install grafana bitnami/grafana -n prometheus
NAME: grafana
LAST DEPLOYED: Fri Jan 14 14:03:17 2022
NAMESPACE: prometheus
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
CHART NAME: grafana
CHART VERSION: 7.6.1
APP VERSION: 8.3.3

** Please be patient while the chart is being deployed **

1. Get the application URL by running these commands:
    echo "Browse to http://127.0.0.1:8080"
    kubectl port-forward svc/grafana 8080:3000 &

2. Get the admin credentials:

    echo "User: admin"
    echo "Password: $(kubectl get secret grafana-admin --namespace prometheus -o jsonpath="{.data.GF_SECURITY_ADMIN_PASSWORD}" | base64 --decode)"
```


```

➜ echo "Password: $(kubectl get secret grafana-admin --namespace prometheus -o jsonpath="{.data.GF_SECURITY_ADMIN_PASSWORD}" | base64 --decode)"
Password: Af7pwEuJx6



export POD_NAME=$(kubectl get pods --namespace prometheus -l "app.kubernetes.io/name=grafana,app.kubernetes.io/instance=grafana" -o jsonpath="{.items[0].metadata.name}")
kubectl --namespace prometheus port-forward $POD_NAME 3000
```

To view dashboard, the above credentials would be required. 


Let us test 

1. Pull metric data using prometheus queries

    * [prometheus queries - official document](https://prometheus.io/docs/prometheus/latest/querying)

    * [PromQL tutorial for beginners and humans](https://valyala.medium.com/promql-tutorial-for-beginners-9ab455142085)


    Demo: 

    ```
    #using query from Rancher prometheus query - Cluster memory utilization in MB; https://rancher.com/docs/rancher/v2.0-v2.4/en/cluster-admin/tools/cluster-monitoring/expression/#cluster-memory-utilization

    ➜ kubectl  port-forward  svc/prometheus-kube-prometheus-prometheus 9090:9090 -n prometheus
    Forwarding from 127.0.0.1:9090 -> 9090
    Forwarding from [::1]:9090 -> 9090

    ```

    As shown in the image, first, enter the query and then hit execute to see the result.

    ![prometheus-query](pro-query.png)



2. Using the above query approach, custom dashboard can be built in Grafana. Grafana also offers [marketplace](https://grafana.com/grafana/dashboards/?) from where community created dashboard can be loaded. 


    ```
    ➜ kubectl  port-forward  svc/prometheus-kube-prometheus-prometheus 9090:9090 -n prometheus > /dev/null 2>&1 &
    [1] 4219

    ➜ kubectl port-forward --namespace prometheus svc/prometheus-kube-prometheus-alertmanager 9093:9093 > /dev/null 2>&1 &
    [2] 4262
    
    ➜ kubectl port-forward svc/grafana 8080:3000 --namespace prometheus > /dev/null 2>&1 &
    [3] 4291

    # now accessing localhost:8080 using admin:Af7pwEuJx6



    ```

    * I will be using dashboard([Kubernetes Clusterby buhay](https://grafana.com/grafana/dashboards/7249)) from the marketplace

    ID: 7249 for the above dashboard is mentioned under overview

    a.

    First grafana would need data source to fetch metric data. 

    Gear icon on left from Grafana UI > data source > Prometheus > url `http://prometheus-kube-prometheus-prometheus.prometheus.svc.cluster.local:9090` > hit `save & test` 
    Please ensure that you get message `Data source is working` 

    b. 

    To add above dashboard, hit `+` on left > `import` + `7249` then `load` > in last line, please select `prometheus` datasource >  hit `import`

    some data may not be available. Recommended to troubleshooting by ensuring that prometheus is able to collect data. if data is present, what difficulty grafana is facing to show on dashboard???

    ![dashboard](pro-dash1.png)



I completely understand that this seems to be manual process, but some of vendor have `helm` charts available on github which comes with preloaded template. With managed cloud Kubernetes cluster, most of monitoring/logging backend have example dashboard/queries available to load (e.g. Azure, GCP etc.)


To stop above `port-forward` services. please run `ps -aux | grep -i port-forward`. Later it can be stop using `kill` command.

### Challenge

`prometheus` helm chart has also installed alert manager. So, using query approach, alerts can be setup when any of workload becomes available. 

Setup alerts to Slack or email 

[Step-by-step guide to setting up Prometheus Alertmanager with Slack, PagerDuty, and Gmail
](https://grafana.com/blog/2020/02/25/step-by-step-guide-to-setting-up-prometheus-alertmanager-with-slack-pagerduty-and-gmail/)