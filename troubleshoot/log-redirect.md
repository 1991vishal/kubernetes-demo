# README #

<< [back to Home](../) >>

Source : https://kubernetes.io/docs/concepts/cluster-administration/logging/

Demo:

Writing to stdout 

```
controlplane $ cat pod.yaml 
apiVersion: v1
kind: Pod
metadata:
  name: counter
spec:
  containers:
  - name: count
    image: busybox
    args: [/bin/sh, -c,
            'i=0; while true; do echo "$i: $(date)"; i=$((i+1)); sleep 1; done']
controlplane $ 
controlplane $ kubectl get pod -n test 
NAME      READY   STATUS    RESTARTS   AGE
counter   1/1     Running   0          8s
controlplane $ kubectl logs counter -n test --tail 5
11: Wed Jan 12 22:50:50 UTC 2022
12: Wed Jan 12 22:50:51 UTC 2022
13: Wed Jan 12 22:50:52 UTC 2022
14: Wed Jan 12 22:50:53 UTC 2022
15: Wed Jan 12 22:50:54 UTC 2022

```


### Sidecar - Logging

Let’s implement a simple project to understand this pattern. Here is a simple pod that has main and sidecar containers. The main container is nginx serving on the port 80 that takes the index.html from the volume mount workdir location. The Sidecar container with the image busybox creates logs in the same location with a timestamp. Since the Sidecar container and main container runs parallel Nginx will display the new log information every time you hit in the browser.



[sidecar pattern](https://medium.com/bb-tutorials-and-thoughts/kubernetes-learn-sidecar-container-pattern-6d8c21f873d)


```
apiVersion: v1
kind: Pod
metadata:
  name: sidecar-container-demo
spec:
  containers:
  - image: busybox
    command: ["/bin/sh"]
    args: ["-c", "while true; do echo echo $(date -u) 'Hi I am from Sidecar container' >> /var/log/index.html; sleep 5;done"]
    name: sidecar-container
    resources: {}
    volumeMounts:
    - name: var-logs
      mountPath: /var/log
  - image: nginx
    name: main-container
    resources: {}
    ports:
      - containerPort: 80
    volumeMounts:
    - name: var-logs
      mountPath: /usr/share/nginx/html
  dnsPolicy: Default
  volumes:
  - name: var-logs
    emptyDir: {}

controlplane $ kubectl apply -f pod.yaml -n test
pod/sidecar-container-demo created

# sidecar-container container adds new entry every 5 second to main-container html which is in share with both the containers. 
controlplane $ kubectl exec -it sidecar-container-demo -n test -- wget -qO- localhost | tail -n 5
Defaulting container name to sidecar-container.
Use 'kubectl describe pod/sidecar-container-demo -n test' to see all of the containers in this pod.
echo Wed Jan 12 23:03:31 UTC 2022 Hi I am from Sidecar container
echo Wed Jan 12 23:03:36 UTC 2022 Hi I am from Sidecar container
echo Wed Jan 12 23:03:41 UTC 2022 Hi I am from Sidecar container
echo Wed Jan 12 23:03:46 UTC 2022 Hi I am from Sidecar container
echo Wed Jan 12 23:03:51 UTC 2022 Hi I am from Sidecar container
controlplane $ 
```

Similar approach can be used for logging purposes.

![Sidecar](https://d33wubrfki0l68.cloudfront.net/5bde4953b3b232c97a744496aa92e3bbfadda9ce/39767/images/docs/user-guide/logging/logging-with-streaming-sidecar.png)


By having your sidecar containers write to their own *stdout* and *stderr* streams, you can take advantage of the kubelet and the logging agent that already run on each node. The sidecar containers read logs from a file, a socket, or journald. Each sidecar container prints a log to its own *stdout* or *stderr* stream.

This approach allows you to separate several log streams from different parts of your application, some of which can lack support for writing to *stdout* or *stderr*. The logic behind redirecting logs is minimal, so it's not a significant overhead. Additionally, because *stdout* and *stderr* are handled by the kubelet, you can use built-in tools like `kubectl logs`.


Each sidecar container could tail a particular log file from a shared volume and then redirect the logs to its own `stdout` stream.

```
apiVersion: v1
kind: Pod
metadata:
  name: counter
spec:
  containers:
  - name: count
    image: busybox
    args:
    - /bin/sh
    - -c
    - >
      i=0;
      while true;
      do
        echo "$i: $(date)" >> /var/log/1.log;
        echo "$(date) INFO $i" >> /var/log/2.log;
        i=$((i+1));
        sleep 1;
      done      
    volumeMounts:
    - name: varlog
      mountPath: /var/log
  - name: count-log-1
    image: busybox
    args: [/bin/sh, -c, 'tail -n+1 -f /var/log/1.log']
    volumeMounts:
    - name: varlog
      mountPath: /var/log
  - name: count-log-2
    image: busybox
    args: [/bin/sh, -c, 'tail -n+1 -f /var/log/2.log']
    volumeMounts:
    - name: varlog
      mountPath: /var/log
  volumes:
  - name: varlog
    emptyDir: {}

```


```
# main app container would have no log going to stdout

controlplane $ kubectl get pod -n test
NAME      READY   STATUS    RESTARTS   AGE
counter   3/3     Running   0          99s
controlplane $ kubectl logs counter -n test -c count

# log 1.log => echo "$i: $(date)" >> /var/log/1.log;
controlplane $ kubectl logs counter -n test -c count-log-1 --tail 5
100: Wed Jan 12 23:12:07 UTC 2022
101: Wed Jan 12 23:12:08 UTC 2022
102: Wed Jan 12 23:12:09 UTC 2022
103: Wed Jan 12 23:12:10 UTC 2022
104: Wed Jan 12 23:12:11 UTC 2022

#log 2 : echo "$(date) INFO $i" >> /var/log/2.log
controlplane $ kubectl logs counter -n test -c count-log-2 --tail 5
Wed Jan 12 23:12:12 UTC 2022 INFO 105
Wed Jan 12 23:12:13 UTC 2022 INFO 106
Wed Jan 12 23:12:14 UTC 2022 INFO 107
Wed Jan 12 23:12:15 UTC 2022 INFO 108
Wed Jan 12 23:12:16 UTC 2022 INFO 109
controlplane $ 

```