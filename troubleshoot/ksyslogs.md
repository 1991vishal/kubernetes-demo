# README #

<< [back to Home](../) >>

### cluster components logs

1. *Kubelet* runs as service on each node. 

`sudo journalctl -u kubelet` will return kubelet log

2. Api server, control plane or scheduler log can be available from kube-system pods. In worst case scenario, `docker logs` will return same kind of logs from master nodes.


```


# Kubelet on all nodes
sudo journalctl -u kubelet

# API server
kubectl -n kube-system logs kube-apiserver-k8s-controlplane

# Controller Manager
kubectl -n kube-system logs kube-controller-manager-k8s-controlplane

# Scheduler
kubectl -n kube-system logs kube-scheduler-k8s-controlplane

```


Morever, will explain Cluster and Node logging in detail under `stdout & stderr logs`