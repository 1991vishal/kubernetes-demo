# README #

<< [back to Home](../) >>

Recommended readings:

- [Liveness and Readiness Probes - Redhat](https://cloud.redhat.com/blog/liveness-and-readiness-probes)

- [Kubernetes best practices: Setting up health checks with readiness and liveness probes](https://cloud.google.com/blog/products/containers-kubernetes/kubernetes-best-practices-setting-up-health-checks-with-readiness-and-liveness-probes)


### [Startup Probes in Kubernetes](https://loft.sh/blog/kubernetes-startup-probes-examples-common-pitfalls/)

Startup probes are a newly developed feature supported as a beta in Kubernetes v.1.18. These probes are very useful on slow-start applications; it is much better than increasing initialDelaySeconds on readiness or liveness probes. Startup probe allows our application to become ready, joined with readiness and liveness probes, it can dramatically increase our applications' availability.

### Readiness

Readiness probes are designed to let Kubernetes know when your app is ready to serve traffic. Kubernetes makes sure the readiness probe passes before allowing a service to send traffic to the pod. If a readiness probe starts to fail, Kubernetes stops sending traffic to the pod until it passes.


### Liveness

Liveness probes let Kubernetes know if your app is alive or dead. If you app is alive, then Kubernetes leaves it alone. If your app is dead, Kubernetes removes the Pod and starts a new one to replace it.


### How health checks help

Let’s look at two scenarios where readiness and liveness probes can help you build a more robust app.

### Readiness

Let’s imagine that your app takes a minute to warm up and start. Your service won’t work until it is up and running, even though the process has started. You will also have issues if you want to scale up this deployment to have multiple copies. A new copy shouldn’t receive traffic until it is fully ready, but by default Kubernetes starts sending it traffic as soon as the process inside the container starts. By using a readiness probe, Kubernetes waits until the app is fully started before it allows the service to send traffic to the new copy.

<img src="https://storage.googleapis.com/gweb-cloudblog-publish/original_images/google-kubernetes-probe-readiness6ktf.GIF" width="240" height="200" />



### Liveness

Let’s imagine another scenario where your app has a nasty case of deadlock, causing it to hang indefinitely and stop serving requests. Because the process continues to run, by default Kubernetes thinks that everything is fine and continues to send requests to the broken pod. By using a liveness probe, Kubernetes detects that the app is no longer serving requests and restarts the offending pod.

<img src="https://storage.googleapis.com/gweb-cloudblog-publish/original_images/google-kubernetes-probe-livenessae14.GIF" width="240" height="200" />



#### Type of Probes

The next step is to define the probes that test readiness and liveness. There are three types of probes: HTTP, Command, and TCP. You can use any of them for liveness and readiness checks.

#### HTTP

HTTP probes are probably the most common type of custom liveness probe. Even if your app isn’t an HTTP server, you can create a lightweight HTTP server inside your app to respond to the liveness probe. Kubernetes pings a path, and if it gets an HTTP response in the 200 or 300 range, it marks the app as healthy. Otherwise it is marked as unhealthy.


#### Command

For command probes, Kubernetes runs a command inside your container. If the command returns with exit code 0, then the container is marked as healthy. Otherwise, it is marked unhealthy. This type of probe is useful when you can’t or don’t want to run an HTTP server, but can run a command that can check whether or not your app is healthy.


#### TCP

The last type of probe is the TCP probe, where Kubernetes tries to establish a TCP connection on the specified port. If it can establish a connection, the container is considered healthy; if it can’t it is considered unhealthy.

TCP probes come in handy if you have a scenario where HTTP probes or command probe don’t work well. For example, a gRPC or FTP service is a prime candidate for this type of probe.


## Configure Probes



[Probes](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.23/#probe-v1-core) have a number of fields that
you can use to more precisely control the behavior of liveness and readiness
checks:

* `initialDelaySeconds`: Number of seconds after the container has started
before liveness or readiness probes are initiated. Defaults to 0 seconds. Minimum value is 0.

* `periodSeconds`: How often (in seconds) to perform the probe. Default to 10
seconds. Minimum value is 1.

* `timeoutSeconds`: Number of seconds after which the probe times out. Defaults
to 1 second. Minimum value is 1.

* `successThreshold`: Minimum consecutive successes for the probe to be
considered successful after having failed. Defaults to 1. Must be 1 for liveness
and startup Probes. Minimum value is 1.

* `failureThreshold`: When a probe fails, Kubernetes will
try `failureThreshold` times before giving up. Giving up in case of liveness probe means restarting the container. In case of readiness probe the Pod will be marked Unready.
Defaults to 3. Minimum value is 1.



Demo:

[probes.yaml](probes.yaml)

[source](https://github.com/Praqma/k8s-probes-demo/blob/master/readiness-liveness-with-startup-probe.yaml)

```
➜ cat probes.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: readiness-liveness-with-startup-probe
  labels:
    app: readiness-liveness-with-startup-probe
    
spec:
  selector:
    matchLabels:
      app: readiness-liveness-with-startup-probe
  template:
    metadata:
      labels:
        app: readiness-liveness-with-startup-probe
    spec:
      containers:
      - name: readiness-liveness-with-startup-probe
        image: kamranazeem/k8s-probes-demo:latest
        imagePullPolicy: Always
        env:
        - name: START_DELAY
          value: "30" 
        ports:
        - name: http 
          containerPort: 80
        resources:
          limits:
            cpu: 10m
            memory: 20Mi
          requests:
            cpu: 5m
            memory: 20Mi
        readinessProbe:
          httpGet:
            path: /readinesscheck.txt
            port: 80
          initialDelaySeconds: 5
          periodSeconds: 5
          failureThreshold: 24
          successThreshold: 1
        
        livenessProbe:
          httpGet:
            path: /livenesscheck.txt
            port: 80
          initialDelaySeconds: 10
          periodSeconds: 10
          timeoutSeconds: 1
          failureThreshold: 1
        startupProbe:
          httpGet:
            path: /startupcheck.txt
            port: 80
          periodSeconds: 5
          failureThreshold: 12
          timeoutSeconds: 1
---
apiVersion: v1
kind: Service
metadata:
  name: readiness-liveness-with-startup-probe
  labels:
    app: readiness-liveness-with-startup-probe
spec:
  ports:
    - port: 80
  selector:
    app: readiness-liveness-with-startup-probe
  type: ClusterIP
```

```

➜ kubectl apply -f probes.yaml -n test
deployment.apps/readiness-liveness-with-startup-probe created
service/readiness-liveness-with-startup-probe created


# if app is writing to stdout, you will see startupprob, readiness and liveness entries when kube is trying to scrape against the endpoint
➜ kubectl logs readiness-liveness-with-startup-probe-56bdcf7c66-wnkjp -n test | head -n 10
13-01-2022_21:00:33 - Container started
13-01-2022_21:00:33 - START_DELAY is set - Simulating a slow-starting container by sleeping for 30 seconds ...
13-01-2022_21:01:05 - Kubernetes probes demo - Web service started
Exec-uting: nginx -g daemon off;
10.42.0.1 - - [13/Jan/2022:21:01:13 +0000] "GET /startupcheck.txt HTTP/1.1" 200 8 "-" "kube-probe/1.22" "-"
10.42.0.1 - - [13/Jan/2022:21:01:13 +0000] "GET /readinesscheck.txt HTTP/1.1" 200 6 "-" "kube-probe/1.22" "-"
10.42.0.1 - - [13/Jan/2022:21:01:18 +0000] "GET /livenesscheck.txt HTTP/1.1" 200 6 "-" "kube-probe/1.22" "-"
10.42.0.1 - - [13/Jan/2022:21:01:18 +0000] "GET /readinesscheck.txt HTTP/1.1" 200 6 "-" "kube-probe/1.22" "-"

```