# README #

<< [back to Home](../) >>

Continued from Kubernetes componet logs

#### Cluster Logging


Having a separate storage location for cluster component logging, such as nodes, pods, and applications.

- [Cluster-level logging architectures](https://kubernetes.io/docs/concepts/cluster-administration/logging/#cluster-level-logging-architectures)

- [Kubernetes Logging Best Practices](https://platform9.com/blog/kubernetes-logging-best-practices/)

Commonly deployed in one of three ways:

1. [Logging agent on each node](https://kubernetes.io/docs/concepts/cluster-administration/logging/#using-a-node-logging-agent) that sends log data to a backend storage repository

   1. These agents can be deployed using a DaemonSet replica to ensure nodes have the agent running
   
   2. Note: This approach only works for applications' standard output (_stdout_) and standard error (_stderr_)

2. [Logging agent as a sidecar](https://kubernetes.io/docs/concepts/cluster-administration/logging/#using-a-sidecar-container-with-the-logging-agent) to specific deployments that sends log data to a backend storage repository
   
   1. Note: Writing logs to a file and then streaming them to stdout can double disk usage

3. [Configure the containerized application](https://kubernetes.io/docs/concepts/cluster-administration/logging/#exposing-logs-directly-from-the-application) to send log data to a backend storage repository


### Node Logging

Having a log file on the node that is populated with standard output (_stdout_) and standard error (_stderr_) log entries from containers running on the node.

- [Logging at the node level](https://kubernetes.io/docs/concepts/cluster-administration/logging/#logging-at-the-node-level)


Demo : 
 
?? pending - in last - requires multiple nodes

Logging backend
- log forwarding agent
- all cluster logs
- namespace logs
- pod logs

[Demo source](https://www.linode.com/docs/guides/how-to-deploy-the-elastic-stack-on-kubernetes/)

Recommended Reading

- [how ELK works? ](https://logz.io/learn/complete-guide-elk-stack/)

- Youtube 

   - [Monitoring Kubernetes Environments with Elasticsearch, Elastic Stack (ELK Stack)](https://www.youtube.com/watch?v=NzHGDyAQ2_Y)

   - [Logging with EFK in Kubernetes](https://www.youtube.com/watch?v=mwToMPpDHfg)


```
➜ kubectl create ns elk
helm repo add elastic https://helm.elastic.co
helm repo update

helm install elasticsearch elastic/elasticsearch -n elk
helm install filebeat elastic/filebeat -n elk
helm install kibana elastic/kibana -n elk
namespace/elk created
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/a/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/a/.kube/config
"elastic" already exists with the same configuration, skipping
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/a/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/a/.kube/config
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "elastic" chart repository
...Successfully got an update from the "bitnami" chart repository
Update Complete. ⎈Happy Helming!⎈
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/a/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/a/.kube/config
W0113 17:22:52.462913    3869 warnings.go:70] policy/v1beta1 PodDisruptionBudget is deprecated in v1.21+, unavailable in v1.25+; use policy/v1 PodDisruptionBudget
W0113 17:22:52.498347    3869 warnings.go:70] policy/v1beta1 PodDisruptionBudget is deprecated in v1.21+, unavailable in v1.25+; use policy/v1 PodDisruptionBudget
NAME: elasticsearch
LAST DEPLOYED: Thu Jan 13 17:22:52 2022
NAMESPACE: elk
STATUS: deployed
REVISION: 1
NOTES:
1. Watch all cluster members come up.
  $ kubectl get pods --namespace=elk -l app=elasticsearch-master -w2. Test cluster health using Helm test.
  $ helm --namespace=elk test elasticsearch
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/a/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/a/.kube/config
NAME: filebeat
LAST DEPLOYED: Thu Jan 13 17:22:54 2022
NAMESPACE: elk
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
1. Watch all containers come up.
  $ kubectl get pods --namespace=elk -l app=filebeat-filebeat -w
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/a/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/a/.kube/config
NAME: kibana
LAST DEPLOYED: Thu Jan 13 17:22:57 2022
NAMESPACE: elk
STATUS: deployed
REVISION: 1
TEST SUITE: None

➜ helm ls -n elk
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/a/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/a/.kube/config
NAME            NAMESPACE       REVISION        UPDATED                                 STATUS          CHART                   APP VERSION
elasticsearch   elk             1               2022-01-13 17:22:52.2852828 -0500 EST   deployed        elasticsearch-7.16.2    7.16.2

filebeat        elk             1               2022-01-13 17:22:54.8356266 -0500 EST   deployed        filebeat-7.16.2         7.16.2

kibana          elk             1               2022-01-13 17:22:57.7306358 -0500 EST   deployed        kibana-7.16.2           7.16.2


➜ kubectl get pod -n elk
NAME                             READY   STATUS    RESTARTS   AGE
elasticsearch-master-1           0/1     Pending   0          25s
elasticsearch-master-2           0/1     Pending   0          25s
filebeat-filebeat-tnqnm          0/1     Running   0          22s
elasticsearch-master-0           0/1     Running   0          25s
kibana-kibana-6fbc6f945c-9qw4t   0/1     Running   0          19s
 



```

Everything is deployed but

1.  master pod are in pending state

      `elasticsearch-master-0` is running on worker1 and antiaffinity will ensure that pods will equally be distributed across all nodes. So, anti affinity rules will not let `master-1` and `master-2` to be scheduled on same `worker1`

            
      ```
      spec:
      affinity:
         podAntiAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
               matchExpressions:
               - key: app
                  operator: In
                  values:
                  - elasticsearch-master
            topologyKey: kubernetes.io/hostname

      ```


2. none of pod shows `1/1` ready status

      Readiness checkes are failling on all pod. Therefore, when user/client will try to reach, kubernetes may throw error e.g.


      ```
      ➜ curl localhost:5601
      Kibana server is not ready yet%
      ```



3. It may be possible that elastic search is expecting 3 pods to be schedule and achieve ready state first. 


      All 3 points are clarified by reviewing list of events as follow. 


      ```
      ➜ kubectl describe pod/elasticsearch-master-1 -n elk
      Name:           elasticsearch-master-1
      Namespace:      elk
      Priority:       0
      Node:           <none>
      ....
      ➜ kubectl get ev -n elk
      LAST SEEN   TYPE      REASON                OBJECT                                                              MESSAGE
      15m         Warning   FailedScheduling      pod/elasticsearch-master-1                                          0/1 nodes are available: 1 node(s) didn't match pod anti-affinity rules.
      16m         Warning   FailedScheduling      pod/elasticsearch-master-2                                          0/1 nodes are available: 1 node(s) didn't match pod anti-affinity rules.
      2m34s       Normal    WaitForPodScheduled   persistentvolumeclaim/elasticsearch-master-elasticsearch-master-1   waiting for pod elasticsearch-master-1 to be scheduled
      2m34s       Normal    WaitForPodScheduled   persistentvolumeclaim/elasticsearch-master-elasticsearch-master-2   waiting for pod elasticsearch-master-2 to be scheduled
      2m34s       Warning   Unhealthy             pod/elasticsearch-master-0                                          Readiness probe failed: Waiting for elasticsearch cluster to become ready (request params: "wait_for_status=green&timeout=1s" )
      Cluster is not yet ready (request params: "wait_for_status=green&timeout=1s" )
      2m30s       Warning   Unhealthy             pod/filebeat-filebeat-tnqnm                                         Readiness probe failed:
      2m22s       Warning   Unhealthy             pod/kibana-kibana-6fbc6f945c-9qw4t                                  Readiness probe failed: Error: Got HTTP code 503 but expected a 200
      ...


      ```


I would start fixing `3` and adjust elastic search cluster to `1` pod, first.

Recommended way: 

`helm upgrade` 

Since I used elastic helm chart, I would find a way to adjust pod count from [Github doc - Official Elastic Search chart](https://github.com/elastic/helm-charts/tree/main/elasticsearch)

by adjusting `replicas` to 1 may fix the issue. lets try

```
➜ helm upgrade --set replicas=1 -n elk elasticsearch elastic/elasticsearch
W0113 18:53:20.777053    5583 warnings.go:70] policy/v1beta1 PodDisruptionBudget is deprecated in v1.21+, unavailable in v1.25+; use policy/v1 PodDisruptionBudget
W0113 18:53:20.779887    5583 warnings.go:70] policy/v1beta1 PodDisruptionBudget is deprecated in v1.21+, unavailable in v1.25+; use policy/v1 PodDisruptionBudget
W0113 18:53:20.789431    5583 warnings.go:70] policy/v1beta1 PodDisruptionBudget is deprecated in v1.21+, unavailable in v1.25+; use policy/v1 PodDisruptionBudget
Release "elasticsearch" has been upgraded. Happy Helming!
NAME: elasticsearch
LAST DEPLOYED: Thu Jan 13 18:53:20 2022
NAMESPACE: elk
STATUS: deployed
REVISION: 2
NOTES:
1. Watch all cluster members come up.
  $ kubectl get pods --namespace=elk -l app=elasticsearch-master -w2. Test cluster health using Helm test.
  $ helm --namespace=elk test elasticsearch

```


```
➜ kubectl get pod -n elk
NAME                             READY   STATUS    RESTARTS   AGE
elasticsearch-master-0           1/1     Running   0          2m30s
filebeat-filebeat-tnqnm          1/1     Running   0          92m
kibana-kibana-6fbc6f945c-9qw4t   1/1     Running   0          92m


```



After setting `replicas: 1`, everything looks good. All pods got `1/1` ready status. 

App is showing log stream. 
For more advance logging requirement, please refer to the official document.

[Elastic Stack and Product Documentation](https://www.elastic.co/guide/index.html)


![elk result](elk.png)