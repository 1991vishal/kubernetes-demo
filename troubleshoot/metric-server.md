# README #

<< [back to Home](../) >>



### Install Metric-server 


[Metric-server](https://github.com/kubernetes-sigs/metrics-server)

```
controlplane $ kubectl top nodes
Error from server (NotFound): the server could not find the requested resource (get services http:heapster:)
controlplane $ 


controlplane $ kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
serviceaccount/metrics-server created
clusterrole.rbac.authorization.k8s.io/system:aggregated-metrics-reader created
clusterrole.rbac.authorization.k8s.io/system:metrics-server created
rolebinding.rbac.authorization.k8s.io/metrics-server-auth-reader created
clusterrolebinding.rbac.authorization.k8s.io/metrics-server:system:auth-delegator created
clusterrolebinding.rbac.authorization.k8s.io/system:metrics-server created
service/metrics-server created
deployment.apps/metrics-server created
apiservice.apiregistration.k8s.io/v1beta1.metrics.k8s.io created



controlplane $ 

kubectl top node

NAME                                               CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%   
gke-john-m-research-2-default-pool-42552c4a-fg80   89m          9%     781Mi           67%       
gke-john-m-research-2-default-pool-42552c4a-lx87   59m          6%     644Mi           55%       
gke-john-m-research-2-default-pool-42552c4a-rxmv   53m          5%     665Mi           57%

```

if `kubectl top` command still doesn't work, please review Compatibility. 


Metrics Server | Metrics API group/version | Supported Kubernetes version
---------------|---------------------------|-----------------------------
0.6.x          | `metrics.k8s.io/v1beta1`  | *1.19+
0.5.x          | `metrics.k8s.io/v1beta1`  | *1.8+
0.4.x          | `metrics.k8s.io/v1beta1`  | *1.8+
0.3.x          | `metrics.k8s.io/v1beta1`  | 1.8-1.21