# README #

<< [back to Home](../) >>


Recommended Reading:

- https://cloud.ibm.com/docs/containers?topic=containers-service-arch

We did demo of VPC enabled cluster. 

### VPC versus Classic infrastructure 

Source:

[Comparing IBM Cloud Classic and VPC infrastructure environments](https://cloud.ibm.com/docs/cloud-infrastructure?topic=cloud-infrastructure-compare-infrastructure)

https://www.ibm.com/cloud/architecture/content/course/advanced-networking-for-vpc/vpc-vs-classic-infrastructure/

#### VPC


IBM Cloud VPC is a virtual network that is tied to your customer account. It gives you cloud security by providing fine-grained control over your virtual infrastructure and your network traffic segmentation along with the ability to scale dynamically.

![VPC architecture](https://www.ibm.com/cloud/architecture/images/courses/advanced-networking-for-vpc/vpc-experience-simple.svg)

#### Classic 

The IBM Cloud Classic Infrastructure network consists of three distinct and redundant network architectures that are seamlessly integrated in the secured network-within-a-network topology (multiple isolated logical networks, for example VLANs). This configuration means if an outage occurs in a data center on the public network, the traffic is routed and traverses through other established networks. Routing the traffic across other networks and through another data center provides continued server availability.

> Note: Classic Infrastructure was formerly known as Softlayer.


&nbsp;

### IBM IKS - managed Kubernetes service 


| Component | Description |
|:-----------------|:-----------------|
| Master |  Master components, including the API server and etcd, have three replicas and are spread across zones for even higher availability. Masters include the same components as described in the community Kubernetes architecture. The master and all the master components are dedicated only to you, and are not shared with other IBM customers.  |
| Worker node |   the virtual machines that your cluster manages are instances that are called worker nodes. These worker nodes virtual machines and all the worker node components are dedicated to you only and are not shared with other IBM customers. However, the underlying hardware is shared with other IBM customers. For more information,  You manage the worker nodes through the automation tools that are provided, such as the API, CLI, or console. Unlike classic clusters, you don't see VPC compute worker nodes in your infrastructure portal or separate infrastructure bill, but instead manage all maintenance and billing activity for the worker nodes. Worker nodes include the same components as described in the Classic architecture. Community Kubernetes worker nodes run on Ubuntu 18.04 x86_64, 16.04 x86_64 (deprecated). |
| Cluster networking | Your worker nodes are created in a VPC subnet in the zone that you specify. By default, the public and private cloud service endpoints for your cluster are enabled. Communication between the master and worker nodes is over the private network. Authenticated external users can communicate with the master over the public network, such as to run `kubectl` commands. You can optionally set up your cluster to communicate with on-prem services by setting up a VPC VPN on the private network. |
| App networking | You can create a Kubernetes `LoadBalancer` service for your apps in the cluster, which automatically provisions a VPC load balancer in your VPC outside the cluster. The load balancer is multizonal and routes requests for your app through the private NodePorts that are automatically opened on your worker nodes. For more information.Calico is used as the cluster networking policy fabric. |
| Storage | You can set up only block persistent storage. Block storage is available as a cluster add-on. For more information. |


![VPC Kubernetes cluster](https://cloud.ibm.com/docs-content/v1/content/8ff10af2ce056a29a8fe2d02c627cbe537075b58/containers/images/cs_org_ov_vpc.png)

After doing research of different ways of upgrading IBM kubernetes service, 

there are few limitations with upgrade process. 

- Fundamental upgrade process will remain the same

    Drain master node, upgrade that node by one major version then move on to other master nodes. Similarly, the process is followed with worker nodes as well. 

    For certain kubernetes distribution, draining master node isn't possible e.g. managed kubernetes services (aws, ibm, azure) doesn't grant full access of master node to the client. so, it won't be possible to drain master nodes

- Unlike Azure and other managed services, IBM kubernetes worker nodes need to be drained by the user. IBM doesn't take care of that process.

- IBM node pool (of any finite number e.g. 3 or more or less) can not be upgraded which is possible to do with Azure, and Azure backend takes care of this process automatically with a single command. 

Therefore,

IMO best way to upgrade IBM kubernetes.

- find next major version to be upgrade. It is easy to find using `ibm cloud` cli command or from `portal` gui. IBM backend returns list of available upgrade version based on the current version.

    eg. assume IBM kubernetes is running on `1.14.1` and next possible versoin is `1.15.1`. IBM backend excludes other version from the list like `1.6.1,1.7.1` (will not be available for the selection)

- Upgrade master node first using cli, portal

- Add new node pool with upgrade version. 

- upon successful previous step, drain all nodes from previous node poll - one by one. 

- then remove the whole node pool.


This will be best way to upgrade IBM kubernetes service. It will be time-consuming to upgrade each worker node. So, best way to add new node pool with upgraded version and get rid of the older ones. 


Knowledgebase:

- [Updating the Kubernetes master](https://cloud.ibm.com/docs/containers?topic=containers-update#master)

- [Updating VPC worker nodes](https://cloud.ibm.com/docs/containers?topic=containers-update#vpc_worker_node)