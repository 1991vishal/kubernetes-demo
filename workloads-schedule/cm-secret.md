# README #

<< [back to Home](../) >>


## secret 
Source: [Kubernetes.io](https://kubernetes.io/docs/concepts/configuration/secret/), [Openshift.com](https://docs.openshift.com/container-platform/3.9/dev_guide/secrets.html)


`Builtin Type`   =>   `Usage`

============================================================	

`Opaque`	=> arbitrary user-defined data

`kubernetes.io/service-account-token`. => service account token.

`kubernetes.io/dockercfg`. => Uses the .dockercfg file for required Docker credentials.

`kubernetes.io/dockerconfigjson`. => Uses the .docker/config.json file for required Docker credentials.

`kubernetes.io/basic-auth`. => Use with Basic Authentication.

`kubernetes.io/ssh-auth`. => Use with SSH Key Authentication.

`kubernetes.io/tls`. => Use with TLS certificate authorities


=============================

`basic-auth` vs `Opaque`

Basic authentication requires either a combination of --username and --password, or a token to authenticate against the SCM server.

whereas, 
opaque does not have any set field name. The user has liberty to define any field within secret YAML.



Values are stored in base64 under kubernetes secret
if you're using bash, 

`echo "string" | base64` => to encode string to base64
`echo "base64 encoded string" | base64 -d` => to decode base64 encoded string.


1. `Opaque`

Opaque is the default Secret type if omitted from a Secret configuration file. When you create a Secret using kubectl, you will use the generic subcommand to indicate an Opaque Secret type. For example, the following command creates an empty Secret of type Opaque.

this secret can be created either by `kubectl` command or `YAML` as shown below. 


- using `kubectl` command

```
cloud_user@fdbb2828d21c:~$  kubectl create secret generic my-secret --from-literal=key1=supersecret --from-literal=key2=topsecret
secret/my-secret created
cloud_user@fdbb2828d21c:~$ kubectl get secret my-secret
NAME        TYPE     DATA   AGE
my-secret   Opaque   2      9s
cloud_user@fdbb2828d21c:~$ kubectl get secret my-secret -oyaml
apiVersion: v1
data:
  key1: c3VwZXJzZWNyZXQ=
  key2: dG9wc2VjcmV0
kind: Secret
metadata:
  creationTimestamp: "2021-09-15T15:35:11Z"
  name: my-secret
  namespace: default
  resourceVersion: "23828"
  uid: 5699d14a-5988-4ec2-a53a-6eedd3120adc
type: Opaque
cloud_user@fdbb2828d21c:~$ 

```


- YAML manifest


```
apiVersion: v1
kind: Secret
metadata:
 name: mysecret
 namespace: test
type: Opaque
data:
 username: YWRtaW4=
 password: MWYyZDFlMmU2N2Rm
---
apiVersion: v1
kind: Pod
metadata:
 name: configmap-demo-pod
 namespace: test
spec:
 volumes:
 - name: secret0
 secret:
 secretName: mysecret
 containers:
 - name: demo
 image: alpine
 command: ["sleep", "3600"]
 volumeMounts:
 - mountPath: /secrets
 name: secret0
 env:
 # Define the environment variable
 - name: USERNAME # Notice that the case is different here
 # from the key name in the ConfigMap.
 valueFrom:
 secretKeyRef:
 name: mysecret # The ConfigMap this value comes from.
 key: username # The key to fetch.
 - name: password
 valueFrom:
 secretKeyRef:
 name: mysecret # The ConfigMap this value comes from.
 key: password # The key to fetch.
 
```



2. `Docker pull credentials`


Create a new secret for use with Docker registries.

Dockercfg secrets are used to authenticate against Docker registries.

When using the Docker command line to push images, you can authenticate to a given registry by running ‘docker login DOCKER_REGISTRY_SERVER –username=DOCKER_USER –password=DOCKER_PASSWORD –email=DOCKER_EMAIL’. That produces a ~/.dockercfg file that is used by subsequent ‘docker push’ and ‘docker pull’ commands to authenticate to the registry.


e.g. 
docker registry - either `dockerhub` or `goharbor`

in lab, private registry `goharbor` is used at ep `demo.goharbor.io`

username `robot$demo-secret+test00`

password `sYchhhAeOxazZQ6Ev1wNgXVHbMB1Hifd`


-- using docker login command, 

```
docker login demo.goharbor.io -u `robot$demo-secret+test00`
password: 
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded

```
The above command will create file under `/root/.docker/config.json`

```
sudo cat /root/.docker/config.json
{
        "auths": {
                "demo.goharbor.io": {
                        "auth": "cm9ib3QkZGVtby1zZWNyZXQrdGVzdDAwOnNZY2hoaEFlT3hhelpRNkV2MXdOZ1hWSGJNQjFIaWZk"
                }
        }
```

Now, secret can be created using `kubectl create secret docker-registry ...` as follows


```
root@fdbb2828d21c:/home/cloud_user# kubectl create secret generic regcred --from-file=.dockerconfigjson=/root/.docker/config.json --type=kubernetes.io/dockerconfigjson
secret/regcred created
root@fdbb2828d21c:/home/cloud_user# kubectl get secret regcred -oyaml
apiVersion: v1
data:
  .dockerconfigjson: ewoJImF1dGhzIjogewoJCSJkZW1vLmdvaGFyYm9yLmlvIjogewoJCQkiYXV0aCI6ICJjbTlpYjNRa1pHVnRieTF6WldOeVpYUXJkR1Z6ZERBd09uTlpZMmhvYUVGbFQzaGhlbHBSTmtWMk1YZE9aMWhXU0dKTlFqRklhV1prIgoJCX0KCX0KfQ==
kind: Secret
metadata:
  creationTimestamp: "2021-09-15T15:43:17Z"
  name: regcred
  namespace: default
  resourceVersion: "24649"
  uid: cc0f772a-dcd6-4ad3-9399-34cc4b1a7946
type: kubernetes.io/dockerconfigjson
root@fdbb2828d21c:/home/cloud_user# 

```


-- passing docker credentials using `kubectl create secret docker-registry `



```
root@fdbb2828d21c:/home/cloud_user# kubectl create secret docker-registry my-secret --docker-server=demo.goharbor.io --docker-username='robot$demo-secret+test00' --docker-password=sYchhhAeOxazZQ6Ev1wNgXVHbMB1Hifd
secret/my-secret created
root@fdbb2828d21c:/home/cloud_user# kubectl get secret my-secret -oyaml
apiVersion: v1
data:
  .dockerconfigjson: eyJhdXRocyI6eyJkZW1vLmdvaGFyYm9yLmlvIjp7InVzZXJuYW1lIjoicm9ib3QkZGVtby1zZWNyZXQrdGVzdDAwIiwicGFzc3dvcmQiOiJzWWNoaGhBZU94YXpaUTZFdjF3TmdYVkhiTUIxSGlmZCIsImF1dGgiOiJjbTlpYjNRa1pHVnRieTF6WldOeVpYUXJkR1Z6ZERBd09uTlpZMmhvYUVGbFQzaGhlbHBSTmtWMk1YZE9aMWhXU0dKTlFqRklhV1prIn19fQ==
kind: Secret
metadata:
  creationTimestamp: "2021-09-15T15:49:10Z"
  name: my-secret
  namespace: default
  resourceVersion: "25243"
  uid: 491036b1-08ea-497d-b948-53793d39fd1c
type: kubernetes.io/dockerconfigjson
root@fdbb2828d21c:/home/cloud_user# 

```



now this secret can be defined under `.spec.template.spec.imagePullSecrets` workload YAML as follows.


```
...
      imagePullSecrets:
      - name: my-secret
...

```

```

apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: goserver
  name: goserver
  namespace: test
spec:
  replicas: 1
  selector:
    matchLabels:
      app: goserver
  template:
    metadata:
      labels:
        app: goserver
    spec:
      containers:
      - image: demo.goharbor.io/demo-secret/nginx-env:1
        imagePullPolicy: IfNotPresent
        name: go-webserver
      imagePullSecrets:
      - name: my-secret

```



3. `service-account-token`

service token is created while creating service account (unless `automountServiceAccountToken` is set to false )

```
➜ kubectl create ns test
namespace/test created
 a  /mnt/c/Users/v-vipatel/Downloads/git/kubernetes-demo   master ± 
➜ kubectl create sa test -n test
serviceaccount/test created
 a  /mnt/c/Users/v-vipatel/Downloads/git/kubernetes-demo   master ± 
➜ kubectl get sa test -n test -oyaml
apiVersion: v1
kind: ServiceAccount
metadata:
  creationTimestamp: "2021-12-04T14:14:20Z"
  name: test
  namespace: test
  resourceVersion: "62161"
  uid: b28fcfd7-eeb8-438c-87af-4fcab21a1c94
secrets:
- name: test-token-4p9wv
 a  /mnt/c/Users/v-vipatel/Downloads/git/kubernetes-demo   master ± 
➜ kubectl get secret test-token-4p9wv -n test oyaml
NAME               TYPE                                  DATA   AGE
test-token-4p9wv   kubernetes.io/service-account-token   3      28s

```

`token` secret is auto generated by `kubernetes`

```
➜ kubectl get secret test-token-4p9wv -n test -oyaml
apiVersion: v1
data:
  ca.crt: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUJkekNDQVIyZ0F3SUJBZ0lCQURBS0JnZ3Foa2pPUFFRREFqQWpNU0V3SHdZRFZRUUREQmhyTTNNdGMyVnkKZG1WeUxXTmhRREUyTXpnek1ETTJNREl3SGhjTk1qRXhNVE13TWpBeU1EQXlXaGNOTXpFeE1USTRNakF5TURBeQpXakFqTVNFd0h3WURWUVFEREJock0zTXRjMlZ5ZG1WeUxXTmhRREUyTXpnek1ETTJNREl3V1RBVEJnY3Foa2pPClBRSUJCZ2dxaGtqT1BRTUJCd05DQUFSYXJ4SDhtSnFCL3ROTW5IYU9jZ0pxdklHc3F5SHlDL0dDOVQwWlFIeWcKcElPdDdFZEcxTWs5U2ZUNy9tWnlqWlBBem1NbEhzQUt1NEx1ZEV4eFhLZWNvMEl3UURBT0JnTlZIUThCQWY4RQpCQU1DQXFRd0R3WURWUjBUQVFIL0JBVXdBd0VCL3pBZEJnTlZIUTRFRmdRVU1DWHFxSHlQanFSdFdKNzF2MzJiCnlCTFpmWU13Q2dZSUtvWkl6ajBFQXdJRFNBQXdSUUlnTzFuLytGZFpJRDZmQmpvekx2SCs1clQvbmhOZ0tyRzAKT3MvbjFSUllOL3dDSVFDRXVEeGJmMFZSTVF6SDZ1WjJpUm5BQnNFU25ha2tIS2NyQjZXbnRtbnlTZz09Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K
  namespace: dGVzdA==
  token: ZXlKaGJHY2lPaUpTVXpJMU5pSXNJbXRwWkNJNkltRklaMVJ6YWtZNVpHTndkMjVGWjFWMVREUlRSbDlXY1hoaVYxRnVNMTlrYTJ0Mk5UaDVORjh5VG1NaWZRLmV5SnBjM01pT2lKcmRXSmxjbTVsZEdWekwzTmxjblpwWTJWaFkyTnZkVzUwSWl3aWEzVmlaWEp1WlhSbGN5NXBieTl6WlhKMmFXTmxZV05qYjNWdWRDOXVZVzFsYzNCaFkyVWlPaUowWlhOMElpd2lhM1ZpWlhKdVpYUmxjeTVwYnk5elpYSjJhV05sWVdOamIzVnVkQzl6WldOeVpYUXVibUZ0WlNJNkluUmxjM1F0ZEc5clpXNHROSEE1ZDNZaUxDSnJkV0psY201bGRHVnpMbWx2TDNObGNuWnBZMlZoWTJOdmRXNTBMM05sY25acFkyVXRZV05qYjNWdWRDNXVZVzFsSWpvaWRHVnpkQ0lzSW10MVltVnlibVYwWlhNdWFXOHZjMlZ5ZG1salpXRmpZMjkxYm5RdmMyVnlkbWxqWlMxaFkyTnZkVzUwTG5WcFpDSTZJbUl5T0daalptUTNMV1ZsWWpndE5ETTRZeTA0TjJGbUxUUm1ZMkZpTWpGaE1XTTVOQ0lzSW5OMVlpSTZJbk41YzNSbGJUcHpaWEoyYVdObFlXTmpiM1Z1ZERwMFpYTjBPblJsYzNRaWZRLnhUZnllUHJMbkxqdXI5WFllN25DZDQxcVdTLUJWR1hFX3JkZVdObnRiel9WYW4ySjhzYl9idkt0VGlBeUE1Q3Q4YTBSR2otcTNxMVpOdzFiTEdVamZKM2x4QUQ1cExrNFhrc2NraURuYmJwOUhtQ2RBRUlqemdHenZ2LWgxQTRxd2E0V2MwVENHU3l1bjZ3eEdUQnFtSks2TVhPdFd5YV84R2NYUWlKWU5aODdXUUhLcEtqUVBlbDR6dko5ZmVzRXozVzNTMXVlVFRGelFsNnM3cDJIT05XNFo1NTQtVGwyMXRpS1pwUnJDNnd6aHdsNmxlWUpWekZiM3RVUEFtbm5LZmZLMV9UT3otcUY4czZJSGVxYmpGNm9tM3p1SzFQdTNEYTNwLTFReUZGYWR4b1FPNkNKWXQyQU5TR2gzenNtZm9LOC1nYjhPVTM0dGxZdXFiRHFfQQ==
kind: Secret
...
```


4. `tls` secret 

```
➜ openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -out ingress.crt \
    -keyout ingress.key \
    -subj "/CN=ingress.app.com/O=ingress-app"
Generating a RSA private key
..........+++++
...+++++
writing new private key to 'ingress.key'
-----
 a  /mnt/c/Users/v-vipatel/Downloads/git/certs 
➜ ls
ingress.crt  ingress.key
```

Certs are generated using openssl. To verify SAN, 

```
➜ openssl x509 -in ingress.crt -noout -text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            50:a5:f1:1f:cd:f8:b5:f0:c4:4f:1d:3e:d0:65:40:a1:9f:68:2f:73
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: CN = ingress.app.com, O = ingress-app
        Validity
            Not Before: Dec  4 23:17:58 2021 GMT
            Not After : Dec  4 23:17:58 2022 GMT
        Subject: CN = ingress.app.com, O = ingress-app
```

To create `tls` secret 

```
➜ kubectl create secret tls ingress-app -n test --cert=ingress.crt --key=ingress.key  -oyaml
apiVersion: v1
data:
  tls.crt: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURRVENDQWltZ0F3SUJBZ0lVVUtYeEg4MzR0ZkRFVHgwKzBHVkFvWjlvTDNNd0RRWUpLb1pJaHZjTkFRRUwKQlFBd01ERVlNQllHQTFVRUF3d1BhVzVuY21WemN5NWhjSEF1WTI5dE1SUXdFZ1lEVlFRS0RBdHBibWR5WlhOegpMV0Z3Y0RBZUZ3MHlNVEV5TURReU16RTNOVGhhRncweU1qRXlNRFF5TXpFM05UaGFNREF4R0RBV0JnTlZCQU1NCkQybHVaM0psYzNNdVlYQndMbU52YlRFVU1CSUdBMVVFQ2d3TGFXNW5jbVZ6Y3kxaGNIQXdnZ0VpTUEwR0NTcUcKU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRRGZybnc3ZEk2eTlrZ3c0UGhYQVQya3dUWC9FaFMwREtLTAovam5MVU84MHJ5Yks2eUxQN3AxZjVjVndGTW85LzBNa1pmWmsyM0xNNDA2ZGVBRE9LL0Jzak9rWGxEKzArY2VJCllRRXRpRmJCVzBkY2EwU2RHTDFZeEtwa3pwWC9zMnBSZTdqTi8vMWlPS0VvREtIWVhiS3crTk9HVEhmREoxMXAKZHE5TU83bS9uTFp6Zk9MMVFlY0NGU1NFTE1heE5mZU41Z1E1MmpKa3Q0WTZGR0JGcXNrSlg3TmVUdTh6QzlVLwpZWG1NNFI3dklzbHpTSVNrSVVtRVAvdmhOQ0dueXBRajRxaUVsN2JsSUFGVjM1NGhsUkdKbHNkR0g2aVFJQUFkCkxkRHg5WXlWN2hMQ3k3andhbDB3bTdaNFVDMFdJL2F1RStES0xXWlZwYmxKUW9oZlorNjNBZ01CQUFHalV6QlIKTUIwR0ExVWREZ1FXQkJSRzNzOEZaVTUvd200bVV1SHRmeUdUd0l5RnVUQWZCZ05WSFNNRUdEQVdnQlJHM3M4RgpaVTUvd200bVV1SHRmeUdUd0l5RnVUQVBCZ05WSFJNQkFmOEVCVEFEQVFIL01BMEdDU3FHU0liM0RRRUJDd1VBCkE0SUJBUURXNGMwdDNZeEZrc0dzVUNVVU4yM250TERuNUVoTjEzYUxwN0I3SzQzb2o1MlJ3aHFSc2dvRUo4U1kKUGtWdXZnRDZlSDZhZUQ1T1VHRFEvR2xrVVd5aW5uaGlRcGxlYUxxT1QyQ1hpdXVCUWs3aWVrZjN2MlorZTJ4MApkbUlEVVlIeDhOaFZGSzFwcmJKRnVkeng2T1l6aUNwMldwbkMzWVY5ODFZVDZzb1o1Q2VMUUI3VVFWY0hkNVhWCjJTOUJOK2xsNjF2UFJndnZuaHZURzcrU05BQjVxWmpsL3Z5eGZtNEFMQmNMbS9jam1mdHFpbEkwL0M0NmtkS0QKM1llbDVOcWpsSWZOOTMySi96bS9Sa3NxSGxGWmhkb0FiNVlZY1M5YmxUUlM4dnlqQWU4REhsS1ZOUGcvVk41aAo5RVEzcW9OSmtnQmgrWGJ0TGV4YUFhVkRFSTJCCi0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K
  tls.key: LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUV2Z0lCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQktnd2dnU2tBZ0VBQW9JQkFRRGZybnc3ZEk2eTlrZ3cKNFBoWEFUMmt3VFgvRWhTMERLS0wvam5MVU84MHJ5Yks2eUxQN3AxZjVjVndGTW85LzBNa1pmWmsyM0xNNDA2ZAplQURPSy9Cc2pPa1hsRCswK2NlSVlRRXRpRmJCVzBkY2EwU2RHTDFZeEtwa3pwWC9zMnBSZTdqTi8vMWlPS0VvCkRLSFlYYkt3K05PR1RIZkRKMTFwZHE5TU83bS9uTFp6Zk9MMVFlY0NGU1NFTE1heE5mZU41Z1E1MmpKa3Q0WTYKRkdCRnFza0pYN05lVHU4ekM5VS9ZWG1NNFI3dklzbHpTSVNrSVVtRVAvdmhOQ0dueXBRajRxaUVsN2JsSUFGVgozNTRobFJHSmxzZEdINmlRSUFBZExkRHg5WXlWN2hMQ3k3andhbDB3bTdaNFVDMFdJL2F1RStES0xXWlZwYmxKClFvaGZaKzYzQWdNQkFBRUNnZ0VCQUkyV1dUVVpWcXFaa3FDK0J2U3I2R1U5bC94WmFabCsxZlZmZXgzam9VYnMKQjlZeWVOcTk1QWdyM1lwUTNxUFh6SnhobmxIQlJ5M0NSS1VRclJIVVpsK1A0b3YyWDAwalB0Wm84T0h0aDF6bApkb3BkckJUMDhBWWNsdTh3aFdwU0cxNXd2azVIV0JDd1gvS0Y3SHlVOHVOcTNob0lsVmFHdUc1bG5oalhSWUN2CmR5bHU4TGFXdW1CSDZISC8rQmQzb0N2ZGpMMVZxSkJQSERpWC80dytFQTBpdnhDK2QxUVJjeTd0cEVBclgyTGMKYXh0TTYvM2c2L2x3VUUxUEZPZG51SEpmbXVoay9WNDNWNlRSWVdaNTRWMlBtVU9qK1VhYXJISGt5a21TMFhENgprTGZ6V0ZuNzFWdUphc3V0N0Yyem9sUVJZQWJyS2pvQjJnQTFzMHVkQzhFQ2dZRUE4VFY5VlhSenRORktHQkdpCnB1TStBR2pydUVTQitVTXVseEprcmNadnNoYkdSYXdMRllhVWJ2RVlINm91Zldkc1dEMWtqWHVYQnZFTWhlZEoKczZqcUpNQzNucE1XTmdHOEsxY2hTekR4cDJ1azg1UjNjdUxUMG5EcE0yeFB0UDBmZElDTmdud3NwYUxTZnU4Nwo2alovbWFZcXV2WUxWcDh0VzlHWVE5dXR1cGtDZ1lFQTdXWFpySVdTclN4bzh2WW5PZkxaUEZ5MFBUcVJyU0o1Cms4enlIeUY0WVBTMGROdWdWM0xsNUF0OFp6UXV4VWlsVEpzRmhBT090MnBmSGRHaWlSdTBPZ0hXZ3g1Q1RRNXAKQWhTQ05FaDRIby9oaXVlb2t3UkkrYTg0OXBmQ1c4ZzV0bWk1cm1sQlppWXhwRGFhMEluam1xdFNjelk3clREYQpvZ2NJYXBITGxjOENnWUFOZ1BZY1U2R3FyTkRlR2hhbUN6RE1heE5QUko2V0t0bXY0WHE3YzlHTEFkVmx6eVRyCnRwWVd3KzZhenpjOUZBNHZ2bmdqcDVpajdkaktvbEV3K3JMcERkVDV6cmcrbThoQ0VPV3ZSUFV5YjBkVHo2c0UKUm9pZlRtenYwUStCVFc1MllrQUozeFBNemhuRXUzWDZQY1RWQUVXQ0pBWXNla3JheWhnaExHcEVlUUtCZ0NWcwpKOHN0WnpqTFJtUCtiNktQNkxTK1BGOW1peTVLdi9XUG1oOVVMR0RFVW1vVmNRbzMvNHRhVUNzSXE5TEhwOThTCmxPVUJsbzdOUXQ4MWU3UzNYRXJFbldjZjd2MnNwdHNBRnFKZFJpb3pMaSt5WXYyQjhHc0R0eXdRTWtBN3FQVmIKUHVJQ2o1K2xwNittTi9ObHVTNVprVzY5L3R4SFZqK3ljaDR5aTNBN0FvR0JBTUhxYnVlK2VoT0FjblJXRVVLKwpmTnRNMGZlMnhaWThOZzVPbVBmQlcrNTJlUjhZVVBjc3hnSHhQM0hJNzdWbWI2Vlllc29lcWtHeHN4SDJma05TCkpheUU2KzRqaEVEMlRGYUk2QWQwcGJ0RkZzU1IwVnJxdFZlc1NxZTlYQWVxa3g2V3ZWNThDMmNHSHJnVS9sQk8KZm1PKzRZQ1pVTXZnWFVUWThvT0EvWHE4Ci0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0K
kind: Secret
metadata:
  creationTimestamp: null
  name: ingress-app
  namespace: test
type: kubernetes.io/tls
```


```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: tls-example-ingress
spec:
  tls:
  - hosts:
      - ingress-app
    secretName: testsecret-tls
  rules:
  - host: ingress.app.com
```




---
---
---
## Mounting secrets/cm as volume or calling them as env variable


1. Calling secrets or cm as `ENV` variable inside the pod

For configmap

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: game-demo
data:
  # property-like keys; each key maps to a simple value
  player_initial_lives: "3"
  game.properties: |
    enemy.types=aliens,monsters
    player.maximum-lives=5   
```

```
apiVersion: v1
kind: Pod
metadata:
  name: configmap-demo-pod
spec:
  containers:
    - name: demo
      image: alpine
      command: ["sleep", "3600"]
      env:
        # Define the environment variable
        - name: PLAYER_INITIAL_LIVES # Notice that the case is different here
                                     # from the key name in the ConfigMap.
          valueFrom:
            configMapKeyRef: # or use secretKeyRef for secrets
              name: game-demo           # The ConfigMap this value comes from.
              key: player_initial_lives
```



2. Mounting cm or secret as  `volume`

For the same cm, 


```
apiVersion: v1
kind: Pod
metadata:
  name: configmap-demo-pod
spec:
  containers:
    - name: demo
      image: alpine
      command: ["sleep", "3600"]
      volumeMounts:
      - name: config
        mountPath: "/config"
        readOnly: true
  volumes:
    # You set volumes at the Pod level, then mount them into containers inside that Pod
    - name: config
      configMap: 
        # Provide the name of the ConfigMap you want to mount.
        name: game-demo
        # An array of keys from the ConfigMap to create as files
        items:
        - key: "game.properties"
          path: "game.properties"

```

for secrets

```
...
  volumes:
  - name: foo
    secret:
      secretName: mysecret
```