# README #

<< [back to Home](../) >>


Course: 
[Helm from IBM.com](https://www.ibm.com/cloud/architecture/content/course/helm-fundamentals)

For better understanding, it is strongly recommened to go through IBM helm course before doing the following dmeo. 

worth understanding fundamental of Helm charts, tiller and `values.yaml`


[Longhorn Installation using helm](https://longhorn.io/docs/1.2.3/deploy/install/install-with-helm/):

1. adding longhorn repo to `helm`

```
controlplane $ helm repo add longhorn https://charts.longhorn.io
"longhorn" has been added to your repositories
controlplane $ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "longhorn" chart repository
Update Complete. ⎈Happy Helming!⎈
controlplane $ helm repo list
NAME            URL                       
longhorn        https://charts.longhorn.io
controlplane $ 
```

2. 
let us decide 
- which longhorn version needs to be installed?
- what will be app configuration?

```
controlplane $ helm search repo longhorn
NAME                    CHART VERSION   APP VERSION     DESCRIPTION                                       
longhorn/longhorn       1.2.3           v1.2.3          Longhorn is a distributed block storage system ...
controlplane $ 
```

Longhorn uses local directories to store persistent data. it also handles syncing of same data across all worker nodes. 
For demo purposes, I would set `1` replica cound and data will be stored under dir: `/home/packer/longhorn`

therefore, `values.yaml` should have the following values set

```
defaultSettings:
  defaultDataPath: /home/packer/longhorn
persistence:
  defaultClassReplicaCount: 1
```

There are several ways of passing above value during installation of helm chart.
Please run `helm install --help` to find out all ways of passing these params. 

a. using `helm -f values.yaml` file
b. by setting as follow

```
     --set stringArray              set values on the command line (can specify multiple or separate values with commas: key1=val1,key2=val2)
      --set-file stringArray         set values from respective files specified via the command line (can specify multiple or separate values with commas: key1=path1,key2=path2)
      --set-string stringArray       set STRING values on the command line (can specify multiple or separate values with commas: key1=val1,key2=val2)
```


There is way to generate template before installation. Thus, configuration can be checked before applying

`helm template  ...`

3. 

let us run some commands to check the final template

```
# will generate longhorn template with latest version
helm template longhorn longhorn/longhorn --namespace longhorn-system --create-namespace


controlplane $ helm template longhorn longhorn/longhorn --namespace longhorn-system --create-namespace --set defaultSettings.defaultDataPath=/home/packer/longhorn,persistence.defaultClassReplicaCount=1 | grep -i "numberofreplica\|/home"
    default-data-path: /home/packer/longhorn
      numberOfReplicas: "1"
controlplane $ 

```

To install 

```
helm template longhorn longhorn/longhorn --namespace longhorn-system --create-namespace --set defaultSettings.defaultDataPath=/home/packer/longhorn,persistence.defaultClassReplicaCount=1 | kubectl apply -f -

or 

helm install longhorn longhorn/longhorn --namespace longhorn-system --create-namespace --set defaultSettings.defaultDataPath=/home/packer/longhorn,persistence.defaultClassReplicaCount=1

```


Installation failed with the following error

```

controlplane $ kubectl logs pod/longhorn-manager-r2572 -n longhorn-system
time="2022-01-02T18:26:11Z" level=error msg="Failed environment check, please make sure you have iscsiadm/open-iscsi installed on the host"
time="2022-01-02T18:26:11Z" level=fatal msg="Error starting manager: Environment check failed: Failed to execute: nsenter [--mount=/host/proc/968/ns/mnt --net=/host/proc/968/ns/net iscsiadm --version], output , stderr, nsenter: failed to execute iscsiadm: No such file or directory\n, error exit status 1"
controlplane $ 

```

[As per Prerequisites documentation of longhorn](https://longhorn.io/docs/1.2.3/deploy/install/#installation-requirements), `open-iscsi` needs to be installed on all worker nodes.

```
controlplane $ apt-get install open-iscsi
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following package was automatically installed and is no longer required:
  libuv1
Use 'apt autoremove' to remove it.
The following NEW packages will be installed
  open-iscsi
0 to upgrade, 1 to newly install, 0 to remove and 27 not to upgrade.
Need to get 335 kB of archives.
After this operation, 2,088 kB of additional disk space will be used.
Get:1 http://gb.archive.ubuntu.com/ubuntu xenial-updates/main amd64 open-iscsi amd64 2.0.873+git0.3b4b4500-14ubuntu3.7 [335 kB]
Fetched 335 kB in 0s (2,206 kB/s)
Preconfiguring packages ...
Selecting previously unselected package open-iscsi.
(Reading database ... 115667 files and directories currently installed.)
Preparing to unpack .../open-iscsi_2.0.873+git0.3b4b4500-14ubuntu3.7_amd64.deb ...
Unpacking open-iscsi (2.0.873+git0.3b4b4500-14ubuntu3.7) ...
Processing triggers for initramfs-tools (0.122ubuntu8.16) ...
update-initramfs: Generating /boot/initrd.img-4.4.0-184-generic
cp: cannot stat '/etc/iscsi/initiatorname.iscsi': No such file or directory
Processing triggers for ureadahead (0.100.0-19.1) ...
Processing triggers for systemd (229-4ubuntu21.28) ...
Processing triggers for man-db (2.7.5-1) ...
Setting up open-iscsi (2.0.873+git0.3b4b4500-14ubuntu3.7) ...
Processing triggers for initramfs-tools (0.122ubuntu8.16) ...
update-initramfs: Generating /boot/initrd.img-4.4.0-184-generic
Processing triggers for ureadahead (0.100.0-19.1) ...
Processing triggers for systemd (229-4ubuntu21.28) ...
controlplane $ curl -sSfL https://raw.githubusercontent.com/longhorn/longhorn/v1.2.3/scripts/environment_check.sh | bash
daemonset.apps/longhorn-environment-check created
waiting for pods to become ready (0/1)
waiting for pods to become ready (0/1)
all pods ready (1/1)

  MountPropagation is enabled!

cleaning up...
daemonset.apps "longhorn-environment-check" deleted
clean up complete
controlplane $ 
```


checking again.

```

controlplane $ kubectl get all -n longhorn-system
NAME                                            READY   STATUS    RESTARTS   AGE
pod/csi-attacher-64c99dc5d7-5qnql               1/1     Running   0          30s
pod/csi-attacher-64c99dc5d7-mxkkm               1/1     Running   0          30s
pod/csi-attacher-64c99dc5d7-spqfv               1/1     Running   0          30s
pod/csi-provisioner-59c65d54d-6sdvt             1/1     Running   0          29s
pod/csi-provisioner-59c65d54d-phnkq             1/1     Running   0          29s
pod/csi-provisioner-59c65d54d-xxj8x             1/1     Running   0          29s
pod/engine-image-ei-eee5f438-qmnjk              1/1     Running   0          69s
pod/instance-manager-e-3984ec4c                 1/1     Running   0          68s
pod/instance-manager-r-6c26dcfc                 1/1     Running   0          67s
pod/longhorn-csi-plugin-b4jf7                   2/2     Running   0          29s
pod/longhorn-driver-deployer-767494bcf4-5gs4q   1/1     Running   0          5m16s
pod/longhorn-manager-r2572                      1/1     Running   5          5m16s
pod/longhorn-ui-97659ff58-grnjn                 1/1     Running   0          5m16s

NAME                        TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)     AGE
service/csi-attacher        ClusterIP   10.104.91.49     <none>        12345/TCP   30s
service/csi-provisioner     ClusterIP   10.109.188.138   <none>        12345/TCP   30s
service/longhorn-backend    ClusterIP   10.102.49.247    <none>        9500/TCP    5m16s
service/longhorn-frontend   ClusterIP   10.105.81.113    <none>        80/TCP      5m16s

NAME                                      DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/engine-image-ei-eee5f438   1         1         1       1            1           <none>          69s
daemonset.apps/longhorn-csi-plugin        1         1         1       1            1           <none>          29s
daemonset.apps/longhorn-manager           1         1         1       1            1           <none>          5m16s

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/csi-attacher               3/3     3            3           30s
deployment.apps/csi-provisioner            3/3     3            3           29s
deployment.apps/longhorn-driver-deployer   1/1     1            1           5m16s
deployment.apps/longhorn-ui                1/1     1            1           5m16s

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/csi-attacher-64c99dc5d7               3         3         3       30s
replicaset.apps/csi-provisioner-59c65d54d             3         3         3       29s
replicaset.apps/longhorn-driver-deployer-767494bcf4   1         1         1       5m16s
replicaset.apps/longhorn-ui-97659ff58                 1         1         1       5m16s
controlplane $ 

```

longhorn is installed sucessfully.