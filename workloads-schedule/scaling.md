# README #

<< [back to Home](../) >>


## Scaling a replicaset
Source: [Kubernetes.io](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/#scaling-a-replicaset)

A ReplicaSet can be easily scaled up or down by simply updating the `.spec.replicas` field. The ReplicaSet controller ensures that a desired number of Pods with a matching label selector are available and operational.


## How to scale deployment/any kubernetes workload?

- create kubernetes deployment

```
kubectl create ns test 
kubectl create deployment nginx --image=nginx -n test
```

```
controlplane $ kubectl get pod,rs,deployment -n test
NAME                         READY   STATUS    RESTARTS   AGE
pod/nginx-65f88748fd-24b7r   1/1     Running   0          12m

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.extensions/nginx-65f88748fd   1         1         1       12m

NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.extensions/nginx   1/1     1            1           12m
controlplane $ 
```

Deployment `nginx` has been created with 1 replicas. 

replicaset YAML is as follows. 

```
apiVersion: extensions/v1beta1
kind: ReplicaSet
metadata:
  annotations:
    deployment.kubernetes.io/desired-replicas: "1"
    deployment.kubernetes.io/max-replicas: "2"
    deployment.kubernetes.io/revision: "1"
  creationTimestamp: null
  generation: 1
  labels:
    app: nginx
    pod-template-hash: 65f88748fd
  name: nginx-65f88748fd
  ownerReferences:
  - apiVersion: apps/v1
    blockOwnerDeletion: true
    controller: true
    kind: Deployment
    name: nginx
    uid: 7676873d-157b-11ec-aae5-0242ac11006f
  selfLink: /apis/extensions/v1beta1/namespaces/test/replicasets/nginx-65f88748fd
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
      pod-template-hash: 65f88748fd
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: nginx
        pod-template-hash: 65f88748fd
    spec:
      containers:
      - image: nginx
        imagePullPolicy: Always
        name: nginx
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
```

Now let us scale the application to more than `1` replicas

and observe the change 

```
controlplane $ kubectl scale deployment/nginx -n test --replicas=2
deployment.extensions/nginx scaled
controlplane $ kubectl get rs -n test
NAME               DESIRED   CURRENT   READY   AGE
nginx-65f88748fd   2         2         2       15m
controlplane $ kubectl get rs nginx-65f88748fd -n test -oyaml --export
Flag --export has been deprecated, This flag is deprecated and will be removed in future.
apiVersion: extensions/v1beta1
kind: ReplicaSet
metadata:
  annotations:
    deployment.kubernetes.io/desired-replicas: "2"
    deployment.kubernetes.io/max-replicas: "3"
    deployment.kubernetes.io/revision: "1"
  creationTimestamp: null
  generation: 1
  labels:
    app: nginx
    pod-template-hash: 65f88748fd
  name: nginx-65f88748fd
  ownerReferences:
  - apiVersion: apps/v1
    blockOwnerDeletion: true
    controller: true
    kind: Deployment
    name: nginx
    uid: 7676873d-157b-11ec-aae5-0242ac11006f
  selfLink: /apis/extensions/v1beta1/namespaces/test/replicasets/nginx-65f88748fd
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
      pod-template-hash: 65f88748fd
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: nginx
        pod-template-hash: 65f88748fd
    spec:
      containers:
      - image: nginx
        imagePullPolicy: Always
        name: nginx
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
status:
  replicas: 0
controlplane $
```



Using `kubectl scale` command, two paramaters are changed

- `desired-replicas` and `max-replicas` under `annotations`
- `replicas` field under `.spec`

```
metadata:
  annotations:
    deployment.kubernetes.io/desired-replicas: "2"
    deployment.kubernetes.io/max-replicas: "3"
    deployment.kubernetes.io/revision: "1"
  creationTimestamp: null
```

```
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
      pod-template-hash: 65f88748fd
```




## HPA 
Source: [Kubernetes.io](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/)

The Horizontal Pod Autoscaler automatically scales the number of Pods in a replication controller, deployment, replica set or stateful set based on observed CPU utilization (or, with custom metrics support, on some other application-provided metrics). Note that Horizontal Pod Autoscaling does not apply to objects that can't be scaled, for example, DaemonSets.


Using `kubectl autoscale` command, replicaset can be auto adjusted based on the set usage metric as ref. 

As shown below, HPA will scale/adjust replicaset when the application pod consumption reaches 80% or above. 

```
controlplane $ kubectl autoscale rs/nginx-65f88748fd -n test --max=5
horizontalpodautoscaler.autoscaling/nginx-65f88748fd autoscaled

controlplane $ kubectl get hpa nginx-65f88748fd -n test
NAME               REFERENCE                     TARGETS         MINPODS   MAXPODS   REPLICAS   AGE
nginx-65f88748fd   ReplicaSet/nginx-65f88748fd   <unknown>/80%   1         5         2          2m22s

controlplane $ kubectl get hpa nginx-65f88748fd -n test -oyaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  annotations:
    autoscaling.alpha.kubernetes.io/conditions: '[{"type":"AbleToScale","status":"True","lastTransitionTime":"2021-09-14T17:07:33Z","reason":"SucceededGetScale","message":"the
      HPA controller was able to get the target''s current scale"},{"type":"ScalingActive","status":"False","lastTransitionTime":"2021-09-14T17:07:33Z","reason":"FailedGetResourceMetric","message":"the
      HPA was unable to compute the replica count: unable to get metrics for resource
      cpu: unable to fetch metrics from resource metrics API: the server could not
      find the requested resource (get pods.metrics.k8s.io)"}]'
  creationTimestamp: "2021-09-14T17:07:17Z"
  name: nginx-65f88748fd
  namespace: test
  resourceVersion: "2663"
  selfLink: /apis/autoscaling/v1/namespaces/test/horizontalpodautoscalers/nginx-65f88748fd
  uid: 373cc6bb-157e-11ec-aae5-0242ac11006f
spec:
  maxReplicas: 5
  minReplicas: 1
  scaleTargetRef:
    apiVersion: extensions/v1beta1
    kind: ReplicaSet
    name: nginx-65f88748fd
  targetCPUUtilizationPercentage: 80
status:
  currentReplicas: 2
  desiredReplicas: 0

```