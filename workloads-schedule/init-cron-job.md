# README #

<< [back to Home](../) >>

### [Init Container](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/)

A pod can have multiple containers
running apps within it, but it can also have one or more init containers, which are run
before the app containers are started.

Init containers are exactly like regular containers, except:

* Init containers always run to completion.
* Each init container must complete successfully before the next one starts.

If a Pod's init container fails, the kubelet repeatedly restarts that init container until it succeeds. 
However, if the Pod has a `restartPolicy` of Never, and an init container fails during startup of that Pod, Kubernetes treats the overall Pod as failed.

To specify an init container for a Pod, add the `initContainers` field into
the Pod specification,
as an array of `container` items (similar to the app `containers` field and its contents).


The status of the init containers is returned in `.status.initContainerStatuses`
field as an array of the container statuses (similar to the `.status.containerStatuses`
field).



Demo:

A. **Init Container**

Init container is creating static files for nginx container to host within the same pod.

```
#Init.yaml
apiVersion: v1
kind: Pod
metadata:
  name: initcontainer
spec:
  containers:
  - name: nginx
    image: nginx
    ports:
    - containerPort: 80
    volumeMounts:
    - name: workdir
      mountPath: /usr/share/nginx/html
  # These containers are run during pod initialization
  initContainers:
  - name: busybox
    image: busybox
    command: ["/bin/sh"]
    args: ["-c", "echo '<html><h1>InitDir</h1><html>' >> /work-dir/index.html"]
    volumeMounts:
    - name: workdir
      mountPath: "/work-dir"
  dnsPolicy: Default
  volumes:
  - name: workdir
    emptyDir: {}
```

### Output

1. from the following output, `busybox` container has terminated status with `completed` reason. It means it has completed its operation and file is created under dir. 

2. To check output of that file, `curl localhost` will return file out from `nginx`



```
controlplane $ kubectl describe initcontainer -n test
error: the server doesn't have a resource type "initcontainer"
controlplane $ kubectl describe pod initcontainer -n test
Name:               initcontainer
Namespace:          test
Priority:           0
PriorityClassName:  <none>
Node:               controlplane/172.17.0.37
Start Time:         Sat, 08 Jan 2022 20:17:25 +0000
Labels:             <none>
Annotations:        kubectl.kubernetes.io/last-applied-configuration:
                      {"apiVersion":"v1","kind":"Pod","metadata":{"annotations":{},"name":"initcontainer","namespace":"test"},"spec":{"containers":[{"image":"ng...
Status:             Running
IP:                 10.32.0.5
Init Containers:
  busybox:
    Container ID:  docker://2e9ba08a8f68702cde106f829008abc0f80299a8a0da2722ed21a21d820af3d7
    Image:         busybox
    Image ID:      docker-pullable://busybox@sha256:5acba83a746c7608ed544dc1533b87c737a0b0fb730301639a0179f9344b1678
    Port:          <none>
    Host Port:     <none>
    Command:
      /bin/sh
    Args:
      -c
      echo '<html><h1>InitDir</h1><html>' >> /work-dir/index.html
    State:          Terminated
      Reason:       Completed
      Exit Code:    0
      Started:      Sat, 08 Jan 2022 20:17:28 +0000
      Finished:     Sat, 08 Jan 2022 20:17:28 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8p47v (ro)
      /work-dir from workdir (rw)
Containers:
  nginx:
    Container ID:   docker://3d8126280187303d8b6c3aec31d3260fa3d1169263422ba8bacb033b522acfba
    Image:          nginx
    Image ID:       docker-pullable://nginx@sha256:0d17b565c37bcbd895e9d92315a05c1c3c9a29f762b011a10c54a66cd53c9b31
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Sat, 08 Jan 2022 20:17:32 +0000
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /usr/share/nginx/html from workdir (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-8p47v (ro)


# not output when checking logs - busybox container just creates file, but doesn't write any thing to syslogger
controlplane $ kubectl logs initcontainer -c busybox -n test


# logs of nginx container
controlplane $ kubectl logs initcontainer -c nginx -n test
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2022/01/08 20:17:32 [notice] 1#1: using the "epoll" event method
2022/01/08 20:17:32 [notice] 1#1: nginx/1.21.5
2022/01/08 20:17:32 [notice] 1#1: built by gcc 10.2.1 20210110 (Debian 10.2.1-6) 
2022/01/08 20:17:32 [notice] 1#1: OS: Linux 4.4.0-184-generic
2022/01/08 20:17:32 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
2022/01/08 20:17:32 [notice] 1#1: start worker processes
2022/01/08 20:17:32 [notice] 1#1: start worker process 31
2022/01/08 20:17:32 [notice] 1#1: start worker process 32
127.0.0.1 - - [08/Jan/2022:20:18:03 +0000] "GET / HTTP/1.1" 200 29 "-" "curl/7.74.0" "-"

# File out - created by busybox container
controlplane $ kubectl exec -it initcontainer -n test -- curl localhost
<html><h1>InitDir</h1><html>
```


&nbsp;


### [Job](https://kubernetes.io/docs/concepts/workloads/controllers/job/)

A Job creates one or more Pods and will continue to retry execution of the Pods until a specified number of them successfully terminate. As pods successfully complete, the Job tracks the successful completions. When a specified number of successful completions is reached, the task (ie, Job) is complete. Deleting a Job will clean up the Pods it created. Suspending a Job will delete its active Pods until the Job is resumed again.

A simple case is to create one Job object in order to reliably run one Pod to completion. The Job object will start a new Pod if the first Pod fails or is deleted (for example due to a node hardware failure or a node reboot).

You can also use a Job to run multiple Pods in parallel.

Demo:

B. [**Job**](https://devopscube.com/create-kubernetes-jobs-cron-jobs/)



```
# job.yaml
apiVersion: batch/v1 
kind: Job 
metadata:   
  name: kubernetes-job-example   
  labels:     
    jobgroup: jobexample 
spec:   
  template:     
    metadata:       
      name: kubejob       
      labels:         
        jobgroup: jobexample     
    spec:       
      containers:       
      - name: c         
        image: devopscube/kubernetes-job-demo:latest         
        args: ["10"]       
      restartPolicy: OnFailure
```

### Output 



1. 


[Parallel execution for Jobs ](https://kubernetes.io/docs/concepts/workloads/controllers/job/#parallel-jobs)

For a non-parallel Job, you can leave both .spec.completions and `.spec.parallelism` unset. When both are unset, both are defaulted to 1.

For a fixed completion count Job, you should set `.spec.completions` to the number of completions needed. You can set `.spec.parallelism`, or leave it unset and it will default to 1.

2. 

after completion of the job, `pod` gets completed status as shown end of the output block. 

```
controlplane $ kubectl apply -f job.yaml -n test
job.batch/kubernetes-job-example created


# 
controlplane $ kubectl get job -n test
NAME                     COMPLETIONS   DURATION   AGE
kubernetes-job-example   0/1           7s         7s

#
controlplane $ kubectl describe job -n test
Name:           kubernetes-job-example
Namespace:      test
Selector:       controller-uid=c7e5e2a2-70c3-11ec-94e2-0242ac110025
Labels:         jobgroup=jobexample
Annotations:    kubectl.kubernetes.io/last-applied-configuration:
                  {"apiVersion":"batch/v1","kind":"Job","metadata":{"annotations":{},"labels":{"jobgroup":"jobexample"},"name":"kubernetes-job-example","nam...
Parallelism:    1
Completions:    1
Start Time:     Sat, 08 Jan 2022 20:44:31 +0000
Pods Statuses:  1 Running / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  controller-uid=c7e5e2a2-70c3-11ec-94e2-0242ac110025
           job-name=kubernetes-job-example
           jobgroup=jobexample
  Containers:
   c:
    Image:      devopscube/kubernetes-job-demo:latest
    Port:       <none>
    Host Port:  <none>
    Args:
      100
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age   From            Message
  ----    ------            ----  ----            -------
  Normal  SuccessfulCreate  14s   job-controller  Created pod: kubernetes-job-example-2kwn7


controlplane $ kubectl get pod -n test
NAME                           READY   STATUS    RESTARTS   AGE
kubernetes-job-example-2kwn7   1/1     Running   0          31s
controlplane $ kubectl logs kubernetes-job-example-2kwn7 -n test
This Job will echo message 100 times
1] Hey I will run till the job completes.
2] Hey I will run till the job completes.
3] Hey I will run till the job completes.
4] Hey I will run till the job completes.
5] Hey I will run till the job completes.


NAME                               COMPLETIONS   DURATION   AGE
job.batch/kubernetes-job-example   1/1           24s        37s

NAME                               READY   STATUS      RESTARTS   AGE
pod/kubernetes-job-example-66qtl   0/1     Completed   0          37s
controlplane $ 
```

&nbsp;

### [Cronjob](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/#cronjob)
 
CronJobs are meant for performing regular scheduled actions such as backups, report generation, and so on. Each of those tasks should be configured to recur indefinitely (for example: once a day / week / month); you can define the point in time within that interval when the job should start.


Demo

```
controlplane $ cat cronjob.yaml 
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: c
spec:
  schedule: "* * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: c
            image: devopscube/kubernetes-job-demo:latest
            imagePullPolicy: IfNotPresent
            args: ["65"]
          restartPolicy: OnFailure
controlplane $ 
```

let us use the same `job.yaml` as cronjob. 


### Output


1. Setting `cron` every minute.
After `60s`, first pod starts.



```
controlplane $ kubectl get cronjob,pod -n test
NAME              SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/c   * * * * *   False     0        <none>          60s
controlplane $ kubectl get cronjob,pod -n test
NAME              SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/c   * * * * *   False     1        3s              62s

NAME                     READY   STATUS    RESTARTS   AGE
pod/c-1641676560-xcq8x   0/1     Pending   0          1s
controlplane $ kubectl get cronjob,pod -n test
NAME              SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/c   * * * * *   False     1        5s              64s

NAME                     READY   STATUS              RESTARTS   AGE
pod/c-1641676560-xcq8x   0/1     ContainerCreating   0          3s
controlplane $ 
```

2. 
Second pod is started even though first is still running.

```
controlplane $ kubectl logs pod/c-1641676560-xcq8x -n test --tail 2
53] Hey I will run till the job completes.
54] Hey I will run till the job completes.
controlplane $ kubectl get cronjob,pod -n test
NAME              SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/c   * * * * *   False     2        61s             3m

NAME                     READY   STATUS    RESTARTS   AGE
pod/c-1641676560-xcq8x   1/1     Running   0          119s
pod/c-1641676620-srdjg   1/1     Running   0          58s
controlplane $ 

```

3. 

at every minute, new pod starts running until it reaches to `65`. Also completed history is set to `824641063208` by default. this can be issue when tons of pod stays with successful message. It is better to set fair limit to keep `kubectl get pod` output clean. 



```

controlplane $ kubectl get cronjob,pod -n test
NAME              SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/c   * * * * *   False     3        7s              4m6s

NAME                     READY   STATUS      RESTARTS   AGE
pod/c-1641676560-xcq8x   0/1     Completed   0          3m5s
pod/c-1641676620-srdjg   1/1     Running     0          2m4s
pod/c-1641676680-z6jxf   1/1     Running     0          64s
pod/c-1641676740-f8hch   1/1     Running     0          4s


...
Schedule:                      * * * * *
Concurrency Policy:            Allow
Suspend:                       False
Successful Job History Limit:  824641063208
Failed Job History Limit:      1
...

```

4. 

- let us disable parallel operation using [Concurrency Policy](https://kubernetes.io/docs/tasks/job/automated-tasks-with-cron-jobs/#concurrency-policy)
- Jobs History Limits set `.spec.successfulJobsHistoryLimit` `0`. So, no pod with completed status will remain there. 

cronjob yaml will look like as follows

```
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: c
spec:
  successfulJobsHistoryLimit: 0
  concurrencyPolicy: Replace
  schedule: "* * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: c
            image: devopscube/kubernetes-job-demo:latest
            imagePullPolicy: IfNotPresent
            args: ["65"]
          restartPolicy: OnFailure


# upon applying cronjob
controlplane $ kubectl get pod,cronjob -n test
NAME                     READY   STATUS    RESTARTS   AGE
pod/c-1641689940-pvcd4   1/1     Running   0          37s

NAME              SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/c   * * * * *   False     1        45s             58s
controlplane $ 


# after one minute of application, previous is being terminated due to replace policy
controlplane $ kubectl get pod,cronjob -n test
NAME                     READY   STATUS        RESTARTS   AGE
pod/c-1641689940-pvcd4   0/1     Terminating   0          92s
pod/c-1641690000-tlhn9   1/1     Running       0          32s

NAME              SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/c   * * * * *   False     1        40s             113s

```

Now setting `arg: ['5']` so operation will finish in few seconds as follow

```
# job is completed within 20 seconds. pod with completed state will be cleaned up due to .spec.successfulJobsHistoryLimit: 0
controlplane $ kubectl get pod,cronjob -n test
NAME                     READY   STATUS      RESTARTS   AGE
pod/c-1641690240-sd7hm   0/1     Completed   0          19s

NAME              SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/c   * * * * *   False     1        28s             84s
controlplane $ kubectl get pod,cronjob -n test
NAME              SCHEDULE    SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/c   * * * * *   False     0        30s             86s
```