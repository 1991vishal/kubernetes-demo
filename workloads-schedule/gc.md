# README #

<< [back to Home](../) >>


### [Garbage Collection](https://kubernetes.io/docs/concepts/architecture/garbage-collection/#containers-images)
Garbage collection is a collective term for the various mechanisms Kubernetes uses to clean up cluster resources. This allows the clean up of resources like the following:

- Failed pods
- Completed Jobs
- Objects without owner references
- Unused containers and container images
- Dynamically provisioned PersistentVolumes with a StorageClass reclaim policy of Delete
- Stale or expired CertificateSigningRequests (CSRs)
- Nodes deleted in the following scenarios:
  - On a cloud when the cluster uses a cloud controller manager
  - On-premises when the cluster uses an addon similar to a cloud controller manager
- Node Lease objects


### Recommended readings:

[Kubernetes Garbage Collection
](https://medium.com/@bharatnc/kubernetes-garbage-collection-781223f03c17)

[Deletion and Garbage Collection of Kubernetes Objects
](https://thenewstack.io/deletion-garbage-collection-kubernetes-objects/)


Demo: 

1. apply crd 




```

controlplane $ cat crd-.yaml 
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: webservers.stable.example.io
spec:
  group: stable.example.io
  version: v1
  scope: Namespaced
  names:
    plural: webservers
    singular: webserver
    kind: Deployment
    shortNames:
    - ws
---
controlplane $ kubectl apply -f crd-.yaml 
customresourcedefinition.apiextensions.k8s.io/webservers.stable.example.io created
```



2. created webserver pod

```
controlplane $ kubectl create ns test --dry-run -oyaml| tee workload.yaml
...

---
controlplane $ kubectl create deployment nginx --image=nginx --dry-run -oyaml -n test | tee -a workload.yaml 
...


# Let us modify this file 
controlplane $ cat workload.yaml 
apiVersion: v1
kind: Namespace
metadata:
  creationTimestamp: null
  name: test
spec: {}
status: {}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: nginx
  name: nginx
  namespace: test
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
status: {}
controlplane $ 
```


```
#after modification of workload.yaml
apiVersion: v1
kind: Namespace
metadata:
  creationTimestamp: null
  name: test
---
apiVersion: stable.example.io/v1
kind: Deployment
metadata:
  labels:
    app: nginx
  name: nginx
  namespace: test
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx
        name: nginx
```

3. lets associate deployment obj with ns using uid of webserver

```
controlplane $ kubectl get ws -n test -oyaml | grep -i uid
    uid: 99c47e4e-7156-11ec-ac9d-0242ac11002f
controlplane $


# workload.yaml file will look like this

apiVersion: v1
kind: Namespace
metadata:
  name: test
  ownerReferences:
    - apiVersion: v1
      controller: true
      blockOwnerDeletion: true
      kind: Deployment
      name: nginx
      uid: 99c47e4e-7156-11ec-ac9d-0242ac11002f


controlplane $ kubectl apply -f workload.yaml 
namespace/test configured
deployment.stable.example.io/nginx unchanged
controlplane $ 
```

4. check by deleting ws obj. if ns also gets deleted by itself

```
controlplane $ kubectl get ws -n test
NAME    AGE
nginx   4m3s
controlplane $ kubectl delete ws --all -n test
deployment.stable.example.io "nginx" deleted
controlplane $ kubectl get ns
NAME              STATUS        AGE
default           Active        21m
kube-node-lease   Active        21m
kube-public       Active        21m
kube-system       Active        21m
test              Terminating   4m13s
controlplane $ 

```

It works !!! 
Ns is also get deleted 

Thus, proper planning of crds design can help in minimizing admin overhead of cleaning up resource manually.

there are tons of use cases available on official website. Please go through recommended readings.