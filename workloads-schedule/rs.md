# README #

<< [back to Home](../) >>

source: 
[Replicaset vs HPA](https://stackoverflow.com/a/66431624)

When we create a deployment it create a replica set and number of pods (that we gave in replicas). Deployment control the RS, and RS controls pods. Now, HPA is another abstraction which give the instructions to deployment and through RS make sure the pods fullfil the respective scaling.

As far the k8s doc: The Horizontal Pod Autoscaler automatically scales the number of Pods in a replication controller, deployment, replica set or stateful set based on observed CPU utilization (or, with custom metrics support, on some other application-provided metrics). Note that Horizontal Pod Autoscaling does not apply to objects that can't be scaled, for example, DaemonSets.

![HPA](horizontal-pod-autoscaler.svg)

A brief high level overview is: Basically it's all about controller. Every k8s object has a controller, when a deployment object is created then respective controller creates the rs and associated pods, rs controls the pods, deployment controls rs. On the other hand, when hpa controllers sees that at any moment number of pods gets higher/lower than expected then it talks to deployment.

e.g.



```
➜ k get deployment,pod,rs,hpa -n test --show-labels
NAME                    READY   UP-TO-DATE   AVAILABLE   AGE   LABELS
deployment.apps/nginx   1/1     1            1           23h   app=nginx
deployment.apps/redis   1/1     1            1           22m   app=redis

NAME                         READY   STATUS    RESTARTS   AGE   LABELS
pod/nginx-86d5c747b9-jrpsz   1/1     Running   0          22m   app=nginx,pod-template-hash=86d5c747b9
pod/redis-6749d7bd65-mdxmr   1/1     Running   0          22m   app=redis,pod-template-hash=6749d7bd65

NAME                               DESIRED   CURRENT   READY   AGE   LABELS
replicaset.apps/nginx-86d5c747b9   1         1         1       22m   app=nginx,pod-template-hash=86d5c747b9
replicaset.apps/redis-6749d7bd65   1         1         1       22m   app=redis,pod-template-hash=6749d7bd65

```


## Replicaset vs Replica controller

### Kubernetes Replica Sets

It can be tricky to compare a replica controller vs replica set (ReplicaSet), because the latter is a sort of a hybrid. They are in some ways more powerful than ReplicationControllers, and in others they are less powerful.

**ReplicaSets are declared in essentially the same way as ReplicationControllers, except that they have more options for the selector.**


### Replica Controller

The Replication Controller is the original form of replication in Kubernetes.  It’s being replaced by Replica Sets, but it’s still in wide use, so it’s worth understanding what it is and how it works.

A Replication Controller is a structure that enables you to easily create multiple pods, then make sure that that number of pods always exists. If a pod does crash, the Replication Controller replaces it.

 A Kubernetes controller such as the Replication Controller also provides other benefits, such as the ability to scale the number of pods, and to update or delete multiple pods with a single command.