# README #

<< [back to Home](../) >>


Source : [Resource requests and limits of Pod and container](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#resource-requests-and-limits-of-pod-and-container)

[**Requests and limits**](https://cloud.google.com/blog/products/containers-kubernetes/kubernetes-best-practices-resource-requests-and-limits) are the mechanisms Kubernetes uses to control resources such as CPU and memory. Requests are what the container is guaranteed to get. If a container requests a resource, Kubernetes will only schedule it on a node that can give it that resource. Limits, on the other hand, make sure a container never goes above a certain value. The container is only allowed to go up to the limit, and then it is restricted.


Under `pod.spec.containers.resources`



```
apiVersion: v1
kind: Pod
metadata:
  name: frontend
spec:
  containers:
  - name: app
    image: images.my-company.example/app:v4
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
```



auto scale can be setup as follow by tuning `targetCPUUtilizationPercentage`

 `apiVersion: autoscaling/v2xxxx` or higher version supports custom/external metrics.

```
controlplane $ kubectl autoscale deployment/nginx --min=1 --max=2 --cpu-percent=10 -n test --dry-run -oyaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  creationTimestamp: null
  name: nginx
spec:
  maxReplicas: 2
  minReplicas: 1
  scaleTargetRef:
    apiVersion: extensions/v1beta1
    kind: Deployment
    name: nginx
  targetCPUUtilizationPercentage: 10
```

Demo:

[Demo source](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/)

`kubectl apply -f deployment.yaml -n test`


```
controlplane $ cat deployment.yaml 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: php-apache
spec:
  selector:
    matchLabels:
      run: php-apache
  replicas: 1
  template:
    metadata:
      labels:
        run: php-apache
    spec:
      containers:
      - name: php-apache
        image: k8s.gcr.io/hpa-example
        ports:
        - containerPort: 80
        resources:
          limits:
            cpu: 500m
          requests:
            cpu: 200m
---
apiVersion: v1
kind: Service
metadata:
  name: php-apache
  labels:
    run: php-apache
spec:
  ports:
  - port: 80
  selector:
    run: php-apache
```

`kubectl apply -f hpa.yaml -n test`

```
controlplane $ cat hpa.yaml 
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  creationTimestamp: null
  name: php-apache
spec:
  maxReplicas: 10
  minReplicas: 1
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: php-apache
  targetCPUUtilizationPercentage: 50
```

`HPA` will try to scale/add more pod when cpu load is detected `50%` of `500m` or higher.


Let us generate simulated load using 

`kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache; done"`


```

NAME                              READY   STATUS    RESTARTS   AGE
pod/load-generator                1/1     Running   0          54s
pod/php-apache-7656945b6b-76cfn   1/1     Running   0          3s
pod/php-apache-7656945b6b-dmbnz   1/1     Running   0          18s
pod/php-apache-7656945b6b-lphh2   1/1     Running   0          109s
pod/php-apache-7656945b6b-pcdjn   1/1     Running   0          18s
pod/php-apache-7656945b6b-txvwn   1/1     Running   0          18s

NAME                                             REFERENCE               TARGETS    MINPODS   MAXPODS   REPLICAS   AGE
horizontalpodautoscaler.autoscaling/php-apache   Deployment/php-apache   214%/50%   1         10        4          93s

```

as you can see, pods replicas are set `4` as load-generator is increasing load against the service endpoint.