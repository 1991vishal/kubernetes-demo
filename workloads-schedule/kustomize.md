# README #

<< [back to Home](../) >>

### Kustomize

Recommended tuts:

[Kustomize Tutorial from densify](https://www.densify.com/kubernetes-tools/kustomize), 

[Kustomize Tutorial: Creating a Kubernetes app out of multiple pieces](https://www.mirantis.com/blog/introduction-to-kustomize-part-1-creating-a-kubernetes-app-out-of-multiple-pieces/)


&nbsp;


**Kustomize** is one of the most useful tools in the Kubernetes ecosystem for simplifying deployments, allowing you to create an entire Kubernetes application out of individual pieces — without touching the YAML configuration files for the individual components.



Demo:

Using build command and touching yaml file, kubernetes object can be modified. 

```
#deployment.yaml file
$ kubectl create deployment nginx --image=nginx --dry-run -oyaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: nginx
  name: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx
        name: nginx
```

```
$ kustomize build
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: nginx
  name: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
status: {}
$ cat kustomization.yaml 

resources:
- deployment.yaml
$ 
```

I would like to change label from `app: nginx` to `app: webserver`

`kustomization.yaml` would be like this 

```
commonLabels:
  app: webserver

resources:
- deployment.yaml
```


`kustomize build` will return the following output

```
$ kustomize build
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: webserver
  name: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webserver
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: webserver
    spec:
      containers:
      - image: nginx
        name: nginx
```


To apply `kustomize build` config, 

run 
`kustomize build | kubectl apply -f -`

To learn how to write `kustomization.yaml` file, visit [`kustomize.io`](https://kustomize.io)