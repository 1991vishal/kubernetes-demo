# README #

<< [back to Home](../) >>

### taints and tolerations



A taint allows a node to refuse a pod to be scheduled unless that pod has a matching toleration.

You apply **taints** to a node through the Node specification (`NodeSpec`) and apply **tolerations** to a pod through the Pod specification (`PodSpec`). When you apply a taint a node, the scheduler cannot place a pod on that node unless the pod can tolerate the taint

`taint on node`
```
spec:
....
  template:
....
    spec:
      taints:
      - effect: NoExecute
        key: key1
        value: value1
....
```
`tolerations` at pod level
```
spec:
....
  template:
....
    spec:
      tolerations:
      - key: "key1"
        operator: "Equal"
        value: "value1"
        effect: "NoExecute"
        tolerationSeconds: 3600
....

```


Demo: 

```

# 2 node cluster. By default, control plane node won't schedule workload due to taint 
controlplane $ kubectl get nodes
NAME           STATUS   ROLES    AGE   VERSION
controlplane   Ready    master   65s   v1.14.0
node01         Ready    <none>   47s   v1.14.0

# node01 is worker node and now I applied taint to make it not schedulable unless podSpec has any toleration defined. 

controlplane $ kubectl taint node node01 name=tom:NoSchedule
node/node01 tainted 

controlplane $ kubectl describe node/node01 | grep -i tain
Taints:             name=tom:NoSchedule
controlplane $ 
```

let us verify what happens when pod is trying to run.




```
# creating test ns and nginx deployment
controlplane $ kubectl create ns test
namespace/test created
controlplane $ kubectl create deployment ws --image=nginx -n test 
deployment.apps/ws created

# pod is stuck with pending state
controlplane $ kubectl get pod -n test; kubectl get ev -n test | tail -n 5
NAME                 READY   STATUS    RESTARTS   AGE
ws-8dfd47f48-v6brj   0/1     Pending   0          3m45s



6m39s       Normal    ScalingReplicaSet   deployment/nginx              Scaled up replica set nginx-6dd8f8644f to 1
6m32s       Normal    ScalingReplicaSet   deployment/nginx              Scaled down replica set nginx-65f88748fd to 0


###
it says that both nodes have taint present. so, no node will run any workloads.
###

37s         Warning   FailedScheduling    pod/ws-8dfd47f48-v6brj        0/2 nodes are available: 2 node(s) had taints that the pod didn't tolerate.
3m46s       Normal    SuccessfulCreate    replicaset/ws-8dfd47f48       Created pod: ws-8dfd47f48-v6brj
3m46s       Normal    ScalingReplicaSet   deployment/ws                 Scaled up replica set ws-8dfd47f48 to 1
controlplane $ 


```

let us reapply ws deployment with tolerations as follows

```

# added toleration using patch command
controlplane $ kubectl patch deployment/ws -p '{"spec": {"template": { "spec": {"tolerations": [{"key": "name", "operator": "Exists" , "effect" : "NoSchedule"}]}}}}' -n test 
deployment.extensions/ws patched

# now previous pod is being deleted and will be replaced with new one
controlplane $ kubectl get pod -n test -owide
NAME                  READY   STATUS              RESTARTS   AGE   IP       NODE     NOMINATED NODE   READINESS GATES
ws-774df5fff4-ptnds   0/1     ContainerCreating   0          6s    <none>   node01   <none>           <none>
ws-8dfd47f48-bdgdb    0/1     Pending             0          29s   <none>   <none>   <none>           <none>

# now pod is running successfully.
controlplane $ kubectl get pod -n test -owide
NAME                  READY   STATUS    RESTARTS   AGE   IP            NODE     NOMINATED NODE   READINESS GATES
ws-774df5fff4-ptnds   1/1     Running   0          11s   10.32.0.193   node01   <none>           <none>
controlplane $ 
``` 


[Kubernetes.io](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/)

The default value for **operator** is *Equal*.

A toleration "matches" a taint if the keys are the same and the effects are the same, and:

  the **operator** is Exists (in which case no value should be specified), or
  
  the **operator** is Equal and the values are equal.
