# README #

<< [back to Home](../) >>




## Rolling Deployment
source [deployment methods on harness.io](https://docs.harness.io/article/325x7awntc-deployment-concepts-and-strategies#rolling_deployment) 

With a Rolling Deployment, all nodes within a single environment are incrementally updated one-by-one or in N batches (as defined by a window size) with a new service/artifact version.

When to use Rolling Deployments
When you need to support both new and old deployments.
Load balancing scenarios that require reduced downtime.
One use of Rolling deployments is as the stage following a Canary deployment in a deployment pipeline. For example, in the first stage you can perform a Canary deployment to a QA environment and verify each group of nodes and, once successful, you perform a Rolling to production.

![rolling deployment](./rolling-2.png)

- Pros

    - Simple, relatively simple to rollback, less risk than Basic deployment.
    - Gradual app rollout with increasing traffic.

- Cons

    - Verification gates between nodes difficult and slow.
    - App/DB needs to support both new and old artifacts. Manual checks/verification at each increment could take a long time.
    - Lost transactions and logged-off users are also something to take into consideration.


## Demo 



```
➜ kubectl create deployment nginx --image=nginx:1.9.1 -n test
deployment.apps/nginx created
```

By default,    `deployment.spec.strategy.type` is set to `RollingUpdate`. 

or it can be set by editing existing workloads under `deployment.spec.strategy.type`: `RollingUpdate`. 

```
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    deployment.kubernetes.io/revision: "2"
    kubernetes.io/change-cause: kubectl1.21.0 set image deployment/nginx nginx=nginx:stable
      --record=true --namespace=test
  labels:
    app: nginx
  name: nginx
  namespace: test
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate

```


Therefore, to record the change, the following command can be used


```
➜ kubectl set image deployment/nginx nginx=nginx:stable --record -n test 
deployment.apps/nginx image updated
 

➜ kubectl rollout history deployment/nginx -n test
deployment.apps/nginx 
REVISION  CHANGE-CAUSE
1         <none>
2         kubectl1.21.0 set image deployment/nginx nginx=nginx:stable --record=true --namespace=test


```


Instead of `kubectl set image`, kubernetes workloads can be edited to make changes using `kubectl edit`. changes will still be recorded when checked running `kubectl rollout history` if workload strategy is set to `RollingUpdate`.


`--revision` flag with `kubectl rollout history` will describe the difference/change.

```
➜ kubectl rollout history deployment/nginx -n test --revision 2
deployment.apps/nginx with revision #2
Pod Template:
  Labels:       app=nginx
        pod-template-hash=74774d4949
  Annotations:  kubernetes.io/change-cause: kubectl1.21.0 set image deployment/nginx nginx=nginx:stable --record=true --namespace=test
  Containers:
   nginx:
    Image:      nginx:stable
    Port:       <none>
    Host Port:  <none>
    Environment:        <none>
    Mounts:     <none>
  Volumes:      <none>

```

To rollback, 

```
➜ kubectl rollout undo deployment/nginx -n test --to-revision 1
deployment.apps/nginx rolled back
```

when `--record` is set to `false`, it would show `CHANGE-CAUSE`  as `<none>`

```
➜ kubectl set image deployment nginx nginx=redis --record=false -n test
deployment.apps/nginx image updated
 
➜ kubectl rollout history deployment/nginx -n test 
deployment.apps/nginx 
REVISION  CHANGE-CAUSE
2         kubectl1.21.0 set image deployment/nginx nginx=nginx:stable --record=true --namespace=test
3         kubectl1.21.0 set image deployment/nginx nginx=nginx:stable --record=true --namespace=test
4         kubectl1.21.0 set image deployment/nginx nginx=nginx:stable --record=true --namespace=test
5         <none>
6         <none>

```