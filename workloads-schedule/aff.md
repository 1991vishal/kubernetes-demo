# README #

<< [back to Home](../) >>


### [ Assigning Pods to Nodes](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/)

You can constrain a Pod so that it can only run on particular set of Node(s). There are several ways to do this and the recommended approaches all use label selectors to facilitate the selection. 

#### [NodeSelector](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#nodeselector)

`nodeSelector` is the simplest recommended form of node selection constraint. `nodeSelector` is a field of PodSpec. It specifies a map of key-value pairs.


`key-value` pairs

let us say `node1` has label of `disk=hdd`
In pod/deployment template, `.spec.nodeSelector` can be defined as follow. pod will strictly be scheduled on `node1`.

```
 nodeSelector:
    disktype: hdd
```

Demo: 

```


controlplane $ kubectl get nodes --show-labels
NAME           STATUS   ROLES    AGE    VERSION   LABELS
controlplane   Ready    master   102s   v1.14.0   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=controlplane,kubernetes.io/os=linux,node-role.kubernetes.io/master=
node01         Ready    <none>   74s    v1.14.0   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=node01,kubernetes.io/os=linux

# applying disk label to the controlplane
controlplane $ kubectl label node/controlplane disk=hdd
node/controlplane labeled


#applied label is visible now
controlplane $ kubectl get nodes --show-labels
NAME           STATUS   ROLES    AGE     VERSION   LABELS
controlplane   Ready    master   2m13s   v1.14.0   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,disk=hdd,kubernetes.io/arch=amd64,kubernetes.io/hostname=controlplane,kubernetes.io/os=linux,node-role.kubernetes.io/master=
node01         Ready    <none>   105s    v1.14.0   beta.kubernetes.io/arch=amd64,beta.kubernetes.io/os=linux,kubernetes.io/arch=amd64,kubernetes.io/hostname=node01,kubernetes.io/os=linux
controlplane $ 
```



```
# ns test created to spin up nginx pod
controlplane $ kubectl create ns test
namespace/test created

controlplane $ kubectl create deployment nginx --image=nginx -n test
deployment.apps/nginx created

# pod is scheduled on node01 to run
controlplane $ kubectl create deployment nginx --image=nginx -n testdeployment.apps/nginx created
controlplane $ kubectl get pod -n test -owide
NAME                     READY   STATUS    RESTARTS   AGE   IP            NODE     NOMINATED NODE   READINESS GATES
nginx-65f88748fd-cwkcc   1/1     Running   0          10s   10.32.0.193   node01   <none>           <none>
controlplane $  
```

let us apply `.spec.nodeSelector` = `disk:hdd`

```
controlplane $ kubectl patch deployment nginx -n test -p '{"spec": {"template": {"spec": {"nodeSelector": {"disk": "hdd"}}}}}'
deployment.extensions/nginx patched
controlplane $


# pod is rescheduled on a control plane node
controlplane $ kubectl get pod -n test -owide
NAME                     READY   STATUS    RESTARTS   AGE   IP          NODE           NOMINATED NODE   READINESS GATES
nginx-865cd5b7cc-zftch   1/1     Running   0          28s   10.32.0.2   controlplane   <none>           <none>
controlplane $ 
```

to remove label control plane

```
controlplane $ kubectl label node/controlplane disk-
node/controlplane labeled
controlplane $
```

Alternatively, `nodeName` can be used instead of `nodeSelector`. Key-value pair approach works well when there is a group of nodes.

```
controlplane $ kubectl drain node01
node/node01 already cordoned

controlplane $ kubectl get nodes
NAME           STATUS                     ROLES    AGE   VERSION
controlplane   Ready                      master   26m   v1.14.0
node01         Ready,SchedulingDisabled   <none>   26m   v1.14.0
controlplane $

controlplane $ kubectl create deployment nginx --image=nginx -n test 
deployment.apps/nginx created


# scheduler wouldnt able to schedule pod on worker node as it is not ready state. 
controlplane $ kubectl get pod -owide -n test
NAME                     READY   STATUS    RESTARTS   AGE   IP       NODE     NOMINATED NODE   READINESS GATES
nginx-65f88748fd-ntjv8   0/1     Pending   0          13s   <none>   <none>   <none>           <none>
controlplane $

#using nodeName, let us schedule it manually.

controlplane $ kubectl patch deployment nginx -n test -p '{"spec": {"template": {"spec": {"nodeName": "node01"}}}}'
deployment.extensions/nginx patched


# now pod is in running state
controlplane $ kubectl get pod -n test -owide
NAME                    READY   STATUS    RESTARTS   AGE   IP            NODE     NOMINATED NODE   READINESS GATES
nginx-f6cffbb95-xzfqp   1/1     Running   0          5s    10.32.0.193   node01   <none>           <none>
controlplane $ 


```

&nbsp;

#### Node Affinity

[katacoda](https://www.katacoda.com/andresguisado/courses/kubernetes101/assign-pod-nodes)

When you look at the [Kubernetes API Reference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.18/#nodeaffinity-v1-core), you'll notice that the two specs for Node Affinity are:

  `spec.affinity.nodeAffinity.preferredDuringSchedulingIgnoredDuringExecution` 
  
  - **Soft NodeAffinity** and **Anti-Affinity**: If the node label exists, the Pod will be ran there. If not, then the Pod will be rescheduled elsewhere within the cluster.

  `spec.affinity.nodeAffinity.requiredDuringSchedulingIgnoredDuringExecution` 
  
  - **Hard NodeAffinity** and **Anti-Affinity**: If the node label doesn't exist, then the pod won't be scheduled at all.


#### Soft Node Affinity

Let's inspect the `node-soft-affinity.yaml` manifest:

`cat /manifests/node-soft-affinity.yaml`

The manfifest reads as: "If there are no nodes labelled as `apple`, then still schedule the pod to a node".

```
controlplane $ cat /manifests/node-soft-affinity.yaml;echo
apiVersion: v1
kind: Pod
metadata:
  name: happypanda
  labels: 
    app: redis
    segment: backend
    company: mycompany 
    disk: ssd  
spec:
  affinity:
    nodeAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 1
        preference:
          matchExpressions:
          - key: fruit
            operator: In
            values:
            - apple
  containers:
  - name: redis
    image: redis
    ports:
    - name: redisport
      containerPort: 6379
      protocol: TCP
controlplane $ 

```


#### Hard Node Affinity

Now inspect the `node-hard-affinity.yaml` manifest:

`cat /manifests/node-hard-affinity.yaml`

The manifest reads as: "If there are no nodes labelled as apple, then this pod won't be assigned a node by the scheduler".

```
controlplane $ cat /manifests/node-hard-affinity.yaml;echo
apiVersion: v1
kind: Pod
metadata:
  name: happypanda
  labels: 
    app: redis
    segment: backend
    company: mycompany 
    disk: ssd  
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: fruit
            operator: In
            values:
            - apple
  containers:
  - name: redis
    image: redis
    ports:
    - name: redisport
      containerPort: 6379
      protocol: TCP
```

`Node anti-affinity` can be achieved by using the `NotIn` operator. This will help us to ignore nodes while scheduling.

For a node anti-affinity check the `node-hard-anti-affinity.yaml` and `node-soft-anti-affinity.yaml` manifest files:

```

controlplane $ cat /manifests/node-hard-anti-affinity.yaml ; echo
apiVersion: v1
kind: Pod
metadata:
  name: happypanda
  labels: 
    app: redis
    segment: backend
    company: mycompany 
    disk: ssd  
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: fruit
            operator: NotIn
            values:
            - apple
  containers:
  - name: redis
    image: redis
    ports:
    - name: redisport
      containerPort: 6379
      protocol: TCP
controlplane $ 


controlplane $ cat /manifests/node-soft-anti-affinity.yaml; echo
apiVersion: v1
kind: Pod
metadata:
  name: happypanda
  labels: 
    app: redis
    segment: backend
    company: mycompany 
    disk: ssd  
spec:
  affinity:
    nodeAffinity:
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 1
        preference:
          matchExpressions:
          - key: fruit
            perator: NotIn
            values:
            - apple
  containers:
  - name: redis
    image: redis
    ports:
    - name: redisport
      containerPort: 6379
      protocol: TCP
controlplane $ 
```