# README #

<< [back to Home](../) >>




Recommended Reading

- https://docs.openshift.com/container-platform/3.11/dev_guide/compute_resources.html

## Quotas

A resource quota, defined by a `ResourceQuota` object, provides constraints that limit aggregate resource consumption per project. It can limit the quantity of objects that can be created in a project by type, as well as the total amount of compute resources and storage that may be consumed by resources in that project.


Demo: 

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: core-object-counts
spec:
  hard:
    configmaps: "1" 

```

let us create this file under ns `test`


```shell
controlplane $ kubectl create ns test 
namespace/test created
controlplane $ kubectl create -f rs -n test
resourcequota/core-object-counts created
controlplane $ kubectl get resourcequota -n test
NAME                 AGE   REQUEST           LIMIT
core-object-counts   9s    configmaps: 0/1   
controlplane $ 


```

as you can see, 
after creating `my-config` cm, `my-config1` can not be created since `cm` quota is set to `1` under ns `test`

```
controlplane $ kubectl create cm my-config --from-literal=key1=config1 --from-literal=key2=config2 -n test
configmap/my-config created
controlplane $ kubectl get resourcequota -n test
NAME                 AGE   REQUEST           LIMIT
core-object-counts   50s   configmaps: 1/1   
controlplane $ kubectl create cm my-config1 --from-literal=key1=config1 --from-literal=key2=config2 -n test
Error from server (Forbidden): configmaps "my-config1" is forbidden: exceeded quota: core-object-counts, requested: configmaps=1, used: configmaps=1, limited: configmaps=1
```

## Limit Ranges

A limit range, defined by a `LimitRange` object, enumerates compute resource constraints in a project at the pod, container, image, image stream, and persistent volume claim level, and specifies the amount of resources that a pod, container, image, image stream, or persistent volume claim can consume.

All requests to create and modify resources are evaluated against each LimitRange object in the project. If the resource violates any of the enumerated constraints, the resource is rejected. If the resource does not set an explicit value, and if the constraint supports a default value, the default value is applied to the resource.

For CPU and memory limits, if you specify a maximum value but do not specify a minimum limit, the resource can consume more CPU and memory resources than the maximum value.


Demo :

```yaml

apiVersion: "v1"
kind: "LimitRange"
metadata:
  name: "core-resource-limits" 
spec:
  limits:
    - type: "Pod"
      max:
        cpu: "1" 
```

```shell
controlplane $ kubectl create -f lm -n test
limitrange/core-resource-limits created
controlplane $ kubectl get limitrange -n test
NAME                   CREATED AT
core-resource-limits   2022-01-27T20:09:32Z
controlplane $ 
```

Total cpu of all pod should not exceed `1`

let us create 3 pods of `0.5` CPU each as request.

```yaml
#pod yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: podname
  name: podname
  namespace: test
spec:
  containers:
  - image: nginx
    name: podname
    resources:
      requests:
        cpu: "0.5"
```

```shell

controlplane $ cat podname  | sed 's/podname/nginx/g' | kubectl -n test apply -f -
pod/nginx created
controlplane $ cat podname  | sed 's/podname/nginx1/g' | kubectl -n test apply -f -
pod/nginx1 created
controlplane $ kubectl get pod -n test
NAME     READY   STATUS    RESTARTS   AGE
nginx    1/1     Running   0          16s
nginx1   1/1     Running   0          11s

```

two pods `nginx, nginx1` have been created. 

so max limit has been met so 3rd pod can not be created. Will return following error

```shell
controlplane $ cat podname  | sed 's/podname/nginx2/g' | kubectl -n test apply -f -
Error from server (Forbidden): error when creating "STDIN": pods "nginx2" is forbidden: maximum cpu usage per Pod is 1.  No limit is specified
controlplane $ 

```