
# README #

<< [back to Home](../) >>


Source : [DNS Resolution Demo](https://medium.com/kubernetes-tutorials/kubernetes-dns-for-services-and-pods-664804211501)


e.g.
for pods, `pod-ip-address.my-namespace.pod.cluster.local`
for svc, `name.my-svc.my-namespace.svc.cluster.local`

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: test-pod
  template:
    metadata:
      labels:
        app: test-pod
    spec:
      containers:
      - name: python-http-server
        image: python:2.7
        command: ["/bin/bash"]
        args: ["-c", "echo \" Hello from $(hostname)\" > index.html; python -m SimpleHTTPServer 80"]
        ports:
        - name: http
          containerPort: 80

---
kind: Service
apiVersion: v1
metadata:
  name: test-service
spec:
  selector:
    app: test-pod
  ports:
  - protocol: TCP
    port: 4000
    targetPort: http


```

Client side 

```
apiVersion: v1
kind: Pod 
metadata: 
  name: client-pod
spec:
  containers:
  - name: curl
    image: appropriate/curl
    command: ["/bin/sh"]
    args: ["-c","curl test-service:4000 "]

```

output 


```

busybox nslookup test-service.default.svc.cluster.local
Server:    10.96.0.10
Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local
Name:      test-service.default.svc.cluster.local
Address 1: 10.109.90.121 test-service.default.svc.cluster.local

```

