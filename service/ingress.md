
# README #

<< [back to Home](../) >>


Source : [Ingress Controller](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/), [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/)


## Ingress

Ingress exposes HTTP and HTTPS routes from outside the cluster to services within the cluster. Traffic routing is controlled by rules defined on the Ingress resource.

Here is a simple example where an Ingress sends all its traffic to one Service:

![ingress](./ingress.png)



e.g. ingress resource example 

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: minimal-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /testpath
        pathType: Prefix
        backend:
          service:
            name: test
            port:
              number: 80

```



## Ingress Controller

In order for the Ingress resource to work, the cluster must have an ingress controller running.

Unlike other types of controllers which run as part of the kube-controller-manager binary, Ingress controllers are not started automatically with a cluster. Use this page to choose the ingress controller implementation that best fits your cluster.

Kubernetes as a project supports and maintains AWS, GCE, and nginx ingress controllers.


[Additional controllers](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/#additional-controllers) There are tons of ingress controllers available - managed by different vendors and cloud providers.

`nginx` vendor from F5 load balancer has ingress controller project which is quite popular. 

In one kubernetes cluster, multiple ingress controller can be defined as ingress class. When applying ingress resource, specific ingress controller can be defined using `annotation` method as follow. 

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: name-of-ingress-resource
```

Demo: 
source [katakoda](https://www.katacoda.com/contino/courses/kubernetes/ingress)



1. Deploying app and svc 

```

apiVersion: apps/v1
kind: Deployment
metadata:
  name: webapp1
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webapp1
  template:
    metadata:
      labels:
        app: webapp1
    spec:
      containers:
      - name: webapp1
        image: katacoda/docker-http-server:latest
        ports:
        - containerPort: 80
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webapp2
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webapp2
  template:
    metadata:
      labels:
        app: webapp2
    spec:
      containers:
      - name: webapp2
        image: katacoda/docker-http-server:latest
        ports:
        - containerPort: 80
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webapp3
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webapp3
  template:
    metadata:
      labels:
        app: webapp3
    spec:
      containers:
      - name: webapp3
        image: katacoda/docker-http-server:latest
        ports:
        - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: webapp1-svc
  labels:
    app: webapp1
spec:
  ports:
  - port: 80
  selector:
    app: webapp1
---
apiVersion: v1
kind: Service
metadata:
  name: webapp2-svc
  labels:
    app: webapp2
spec:
  ports:
  - port: 80
  selector:
    app: webapp2
---
apiVersion: v1
kind: Service
metadata:
  name: webapp3-svc
  labels:
    app: webapp3
spec:
  ports:
  - port: 80
  selector:
    app: webapp3

```

```
Output

deployment.apps/webapp1 created
deployment.apps/webapp2 created
deployment.apps/webapp3 created
service/webapp1-svc created
service/webapp2-svc created
service/webapp3-svc created
```

2. creating ingress controller

```
kubectl apply -f ingress.yaml #ingress.yaml is copied here

```


```
controlplane $ kubectl get svc
NAME                    TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
ingress-nginx           LoadBalancer   10.99.173.122   172.17.0.44   80:30477/TCP,443:32756/TCP   4m54s
kubernetes              ClusterIP      10.96.0.1       <none>        443/TCP                      5h54m
nginx-default-backend   ClusterIP      10.97.224.235   <none>        80/TCP                       4m51s
controlplane $ curl 172.17.0.44 ;echo
default backend - 404
controlplane $ 

```

it is returning `404`. it means http backend is working. let us create ingress resources next. 

3. create ingress resource for the app


```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: webapp-ingress
spec:
  rules:
  - host: my.kubernetes.example
    http:
      paths:
      - path: /webapp1
        backend:
          serviceName: webapp1-svc
          servicePort: 80
      - path: /webapp2
        backend:
          serviceName: webapp2-svc
          servicePort: 80
      - backend:
          serviceName: webapp3-svc
          servicePort: 80
```

```
controlplane $ kubectl get ing
NAME             HOSTS                   ADDRESS   PORTS   AGE
webapp-ingress   my.kubernetes.example             80      4s
controlplane $ 

#load balancer ip is 172.17.0.44  - output from step2 

controlplane $ curl -H "Host: my.kubernetes.example" 172.17.0.44/webapp1
<h1>This request was processed by host: webapp1-6d7df9f8d-7mjf6</h1>
controlplane $ curl -H "Host: my.kubernetes.example" 172.17.0.44/webapp2
<h1>This request was processed by host: webapp2-6d48b8ff76-25g49</h1>
controlplane $ curl -H "Host: my.kubernetes.example" 172.17.0.44/webapp3
<h1>This request was processed by host: webapp3-7df59dc67b-qf542</h1>
controlplane $ 

```



