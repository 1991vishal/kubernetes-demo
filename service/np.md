
# README #

<< [back to Home](../) >>

### [Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)


Recommended reading

https://kubernetes.io/docs/concepts/services-networking/network-policies/

https://docs.microsoft.com/en-us/azure/aks/use-network-policies


If you want to control traffic flow at the IP address or port level (OSI layer 3 or 4), then you might consider using Kubernetes NetworkPolicies for particular applications in your cluster. NetworkPolicies are an application-centric construct which allow you to specify how a pod is allowed to communicate with various network "entities" (we use the word "entity" here to avoid overloading the more common terms such as "endpoints" and "services", which have specific Kubernetes connotations) over the network. NetworkPolicies apply to a connection with a pod on one or both ends, and are not relevant to other connections.

The entities that a Pod can communicate with are identified through a combination of the following 3 identifiers:

Other pods that are allowed (exception: a pod cannot block access to itself)
Namespaces that are allowed
IP blocks (exception: traffic to and from the node where a Pod is running is always allowed, regardless of the IP address of the Pod or the node)
When defining a pod- or namespace- based NetworkPolicy, you use a selector to specify what traffic is allowed to and from the Pod(s) that match the selector.

Meanwhile, when IP based NetworkPolicies are created, we define policies based on IP blocks (CIDR ranges)

- Prerequisites 

Network policies are implemented by the network plugin. To use network policies, you must be using a networking solution which supports NetworkPolicy. Creating a NetworkPolicy resource without a controller that implements it will have no effect


---
Demo from [Microsoft KB](https://docs.microsoft.com/en-us/azure/aks/use-network-policies) 

** Deny all inbound traffic to a pod



```
#Creating NS developmenet
 a >> /mnt/c/Users/v-vipatel/Downloads >>
➜ k create namespace development
namespace/development created
 
 #Labeling the ns
 a >> /mnt/c/Users/v-vipatel/Downloads >>
➜ k label namespace/development purpose=development
namespace/development labeled
 
 # Creating sample pod with the label to block all inbound traffic
 a >> /mnt/c/Users/v-vipatel/Downloads >>
➜ k run backend --image=mcr.microsoft.com/oss/nginx/nginx:1.15.5-alpine --labels app=webapp,role=backend --namespace development --expose --port 80
service/backend created
pod/backend created

# Test backend service reponse
 a >> /mnt/c/Users/v-vipatel/Downloads >>
➜ k  run --rm -it --image=mcr.microsoft.com/aks/fundamental/base-ubuntu:v0.0.11 network-policy --namespace development
If you don't see a command prompt, try pressing enter.
root@network-policy:/# wget -qO- http://backend
<!DOCTYPE html>
<html>

<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
root@network-policy:/# exit
exit
Session ended, resume using 'kubectl attach network-policy -c network-policy -i -t' command when the pod is running
pod "network-policy" deleted

#####
It is possible to talk to Backend service without any issue. Currently no network policy is deployed
#####


#Let us create networkpolicy under development ns
 a >> /mnt/c/Users/v-vipatel/Downloads >>
➜ nano np.yaml

# np.yaml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: backend-policy
  namespace: development
spec:
  podSelector:
    matchLabels:
      app: webapp
      role: backend
  ingress: []


 a >> /mnt/c/Users/v-vipatel/Downloads >>
➜ k apply -f np.yaml
networkpolicy.networking.k8s.io/backend-policy created

### 
Network policy has been deployed to block communication with backend service

###
 a >> /mnt/c/Users/v-vipatel/Downloads >>
➜ k run --rm -it --image=mcr.microsoft.com/aks/fundamental/base-ubuntu:v0.0.11 network-policy --namespace development
If you don't see a command prompt, try pressing enter.
root@network-policy:/# wget -O- --timeout=2 --tries=1 http://backend
--2022-01-04 15:10:08--  http://backend/
Resolving backend (backend)... 10.101.253.76
Connecting to backend (backend)|10.101.253.76|:80... failed: Connection timed out.
Giving up.


###
Thus backend service isn't accepting the traffic.
###

```


Let us allow communication from only selected pods/source

** Allow inbound traffic based on a pod label

```
#np.yaml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: backend-policy
  namespace: development
spec:
  podSelector:
    matchLabels:
      app: webapp
      role: backend
  ingress:
  - from:
    - namespaceSelector: {}
      podSelector:
        matchLabels:
          app: webapp
          role: frontend

```

any pod with label `app=webapp,role=frontend` can talk to backend (pod with `app=webapp,role=backend`). All other pods can't talk to `backend` service.

let us apply the above policy

```
 a >> /mnt/c/Users/v-vipatel/Downloads >>
➜ cat np.yaml
kind: NetworkPolicy
apiVersion: networking.k8s.io/v1
metadata:
  name: backend-policy
  namespace: development
spec:
  podSelector:
    matchLabels:
      app: webapp
      role: backend
  ingress:
  - from:
    - namespaceSelector: {}
      podSelector:
        matchLabels:
          app: webapp
          role: frontend


 a >> /mnt/c/Users/v-vipatel/Downloads >>
➜ k apply -f np.yaml
networkpolicy.networking.k8s.io/backend-policy configured

 a >> /mnt/c/Users/v-vipatel/Downloads >>
➜ k run --rm -it frontend --image=mcr.microsoft.com/aks/fundamental/base-ubuntu:v0.0.11 --labels app=webapp,role=fronten
d --namespace development
If you don't see a command prompt, try pressing enter.

###
Thus frontend pod can talk to backend services.
###

root@frontend:/# wget -qO- http://backend
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>

```