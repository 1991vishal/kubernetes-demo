# README #


Recommended Book Reading:

- [Kubernetes in Action](https://www.torontopubliclibrary.ca/detail.jsp?Entt=RDM4020254&R=4020254)

- [Kubernetes: Preparing for the CKA and CKAD Certifications](https://www.torontopubliclibrary.ca/detail.jsp?Entt=RDM4040371&R=4040371) 

- [Kubernetes Toronto Library Catalog](https://www.torontopubliclibrary.ca/search.jsp?Ntt=kubernetes)

---

25% - Cluster Architecture, Installation & Configuration

•	[(a1a) Manage role based access control (RBAC)](install-k8s/rbac.md)

•	[(a1b) Use Kubeadm to install a basic cluster](install-k8s/install.md)

•	[(a1c) Manage a highly-available Kubernetes cluster](install-k8s/ha-install.md)

•	[(a1d) Provision underlying infrastructure to deploy a Kubernetes cluster - same as initiating cluster using kubeadm](install-k8s/install.md)

•	[(a1e) Perform a version upgrade on a Kubernetes cluster using Kubeadm](install-k8s/upgrade.md)

•	[(a1f) Implement etcd backup and restore](install-k8s/etcdback.md)


# 
20% - Services & Networking

•	[(a2a) Understand host networking conﬁguration on the cluster nodes](https://kubernetes.io/docs/concepts/cluster-administration/networking/)

•	[(a2b) Understand connectivity between Pods](https://kubernetes.io/docs/concepts/workloads/pods/#pod-networking)

•	[(a2c) Understand ClusterIP, NodePort, LoadBalancer service types and endpoints](service/svctype.md)


* [(a2c/1) External name, Headless Service](service/svctype.md)

* [(a2c/2) Pod and SVC DNS resolution using curl](service/dnsres.md) ,  [DNS Troubleshooting](https://kubernetes.io/docs/tasks/administer-cluster/dns-debugging-resolution/)

* [DNS records](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/)


•	[(a2d) Know how to use Ingress controllers and Ingress resources](service/ingress.md)

•	(a2e) Know how to conﬁgure and use CoreDNS

-   [Configuration of CoreDNS](https://kubernetes.io/docs/tasks/administer-cluster/coredns/)
- [POD DNS Policy and DNS records](https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pod-s-dns-policy)

•	(a2f) Choose an appropriate container network interface plugin

- [How to choose a k8s CNI plugin](https://medium.com/thermokline/how-to-choose-a-k8s-cni-plugin-771edf4842c0)

- [Top Considerations When Selecting CNI Plugins for Kubernetes](https://thenewstack.io/top-considerations-when-selecting-cni-plugins-for-kubernetes/)

- [Comparison of CNI - Flannel, Calico, Canal, Weave - Suse Linux](https://www.suse.com/c/rancher_blog/comparing-kubernetes-cni-providers-flannel-calico-canal-and-weave/)

•	[(a2g) Network Policy](service/np.md)


#
15% - Workloads & Scheduling

•	[(a3a) Understand deployments and how to perform rolling update and rollbacks](workloads-schedule/rollingupdate.md)

•	[(a3b) Use ConﬁgMaps and Secrets to conﬁgure applications](workloads-schedule/cm-secret.md)

•	[(a3c) Know how to scale applications](workloads-schedule/scaling.md)

•	(a3d) Understand the primitives used to create robust, self-healing, application deployments


>   = [replicaset](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) , [Relationship: Replicaset-ReplicaController vs HPA](workloads-schedule/rs.md)

>   = [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)

```
#To create deployment
kubectl create deployment deployment_name --image=imageName

# for more option
kubectl create deployment --help
```

>   = [Statefulset](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/), [Replication Statefulset Demo](https://www.katacoda.com/bascar/scenarios/px-cassandra)

>   = [Daemonset](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/)

>   = [Init Container, job, Cronjob workloads](workloads-schedule/init-cron-job.md)

>   = [Garbage Collection](workloads-schedule/gc.md)

•	[(a3e) Understand how resource limits (CPU, memory) can affect Pod scheduling](workloads-schedule/r-limits.md)

•	(a3f) Awareness of manifest management and common templating tools

>   - [helm](workloads-schedule/helm.md)
>   - [kustomize](workloads-schedule/kustomize.md)

•	[(a3g) NodeSelector, NodeName, Node Affinity](workloads-schedule/aff.md)

•	[(a3h) pod Affinity](workloads-schedule/aff-pod.md)

•	[(a3i) taint-toleration](workloads-schedule/taint.md)

•   [(a3j) Limit Ranges, ResourceQuotas](workloads-schedule/workloads-schedule/lm.md)

**Recommended readings for pod-node affinity**

- [openshift - Advanced Scheduling and Pod Affinity and Anti-affinity](https://docs.openshift.com/container-platform/3.11/admin_guide/scheduling/pod_affinity.html)

- [Kubernetes in Action](https://livebook.manning.com/concept/kubernetes/topologykey)

    - [Toronto Lib-eBook](https://www.torontopubliclibrary.ca/detail.jsp?Entt=RDM4020254&R=4020254)   

#
10% - Storage

•	(a4a) Understand storage classes, persistent volumes

   - [Installation of NFS server and create Storage Class](storage/NFSserver.md)

     - [Rook NFS](storage/rookNFS.md)

   - [Create Persistent Volume - Static vs Dynamic](storage/pv-static-dynamic.md)

•	(a4b) [Understand volume mode, access modes and reclaim policies for volumes](storage/mode-reclaim.md)
   

•	(a4c) [Understand persistent volume claims primitive](https://portworx.com/tutorial-kubernetes-persistent-volumes/)

•	(a4d) [Know how to conﬁgure applications with persistent storage](https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/)

  > * [emptyDir, downwardAPI](storage/emptyDir.md) 




* *Longhorn installation has been demonstrated under `helm` template* [helm.md](workloads-schedule/helm.md)


• Disaster Recovery [Backup, restore](storage/dr.md) - Kubernetes components, storage volumes etc. 

  * [Volume cloning](storage/volcloning.md)

  * [Disaster Recovery Preparedness for Your Kubernetes Clusters - Suse](https://www.suse.com/c/rancher_blog/disaster-recovery-preparedness-for-your-kubernetes-clusters/)

  * [For understanding/Knowledge - A simple guide to Kubernetes disaster recovery across regions with Kasten by Veeam ](https://blogs.oracle.com/cloud-infrastructure/post/a-simple-guide-to-kubernetes-disaster-recovery-across-regions-with-kasten-by-veeam)

    This approch can be used with other cloud providers as well.


#
30% - Troubleshooting

Recommended Reading for Logs

- [Logging Architecture](https://kubernetes.io/docs/concepts/cluster-administration/logging)

•	(a5a) Evaluate cluster and node logging

- [Find logs of Kube-system components](troubleshoot/ksyslogs.md)

•	(a5b) Understand how to monitor applications, cluster components

-   [Liveness, Readiness, Startup Probes](troubleshoot/liveness-readiness-startup.md)

- [Installation of Metrics server to run kubernetes `top` command](troubleshoot/metric-server.md)

- [Use of Prometheus (for detailed metrics) and Grafana for Dashboard](troubleshoot/Prometheus.md)


•	(a5c) Manage container stdout & stderr logs

- [Collect logs of a pod and redirect it to file](troubleshoot/log-redirect.md)

- [Instllation of Logging backend and use of log forwarded to collect logs of whole cluster or selected resources (namespace, workloads)](troubleshoot/elk-fluentd.md)


•	(a5d) [Troubleshoot application failure](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-application/)

•	(a5e) Troubleshoot cluster component failure

- [Troubleshoot Clusters](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-cluster/)

- [A general overview of cluster failure modes](https://kubernetes.io/docs/tasks/debug-application-cluster/debug-cluster/#a-general-overview-of-cluster-failure-modes)

- [Control Plane Components](https://kubernetes.io/docs/concepts/overview/components/#control-plane-components)

- [Node Components](https://kubernetes.io/docs/concepts/overview/components/#node-components)

•	(a5f) Troubleshoot networking

- [Flannel Troubleshooting](https://github.com/coreos/flannel/blob/master/Documentation/troubleshooting.md#kubernetes-specific)

    The flannel kube subnet manager relies on the fact that each node already has a podCIDR defined.

- [Calico Troubleshooting](https://docs.projectcalico.org/maintenance/troubleshoot/)

    - [Containers do not have network connectivity](https://docs.projectcalico.org/maintenance/troubleshoot/troubleshooting#containers-do-not-have-network-connectivity)



- [DNS troubleshooting](https://kubernetes.io/docs/tasks/administer-cluster/dns-debugging-resolution/)


#
Extra Topic coverage

•	[(a6a) Core concepts/components of Kubernetes](https://www.aquasec.com/cloud-native-academy/kubernetes-101/kubernetes-architecture/)

•	(a6b) Advanced networking - Kubelet - cni - namespace - node iptables etc.

- [kube-iptables-tailer](https://kubernetes.io/blog/2019/04/19/introducing-kube-iptables-tailer/)

- [Liberating Kubernetes From Kube-proxy and Iptables - Martynas Pumputis, Cilium](https://www.youtube.com/watch?v=bIRwSIwNHC0) 

•	(a6c) Docker

- [Linux Foundation Docker Networking Deep Dive](docker/Container_network_Deep_Dive.pdf)

- [IBM IKS - Kubernetes service](ibm-iks.md)

- [TutorialsPoint - Docker](https://www.tutorialspoint.com/docker/index.htm)

- [Docker Build, Push to private reg, scan - Demo](docker/dockerbuild.md)
    - [`Docker-compose`](docker/docker-compose.md)