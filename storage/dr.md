# README #

<< [back to Home](../) >>


#### Kubernetes backup and restore

backup and restoring of entire cluster with data volume is bit tricky. If control plane and data plane won't match upon restore, kubernetes cluster may experience issue after restore. 

We have already seen how to take etcd backup which saves cluster state. Data volumes used by each pod is not part of etcd components. Therefore, both have to be taken care, carefully. 

It is recommended of using third party tools like

- Velero 

    The community has been using this Velero (formerly known as heptio) for while - esp for application backup when the kubernetes clusters are treated as cattle.

    https://www.wecloudpro.com/2020/08/22/kubernetes-disaster-recovery-with-velero.html

    https://docs.microsoft.com/en-us/azure-stack/aks-hci/backup-workload-cluster

- https://docs.kasten.io/latest/operating/dr.html


There are lot of research happening this area so, we always end up finding different DR plan and approaches. 