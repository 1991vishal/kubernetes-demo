# README #

<< [back to Home](../) >>

Source: https://kubernetes.io/docs/concepts/storage/persistent-volumes/


### Provisioning

There are two ways PVs may be provisioned: statically or dynamically.

#### Static

A cluster administrator creates a number of PVs. They carry the details of the real storage, which is available for use by cluster users. They exist in the Kubernetes API and are available for consumption.

Demo:
`/exports/data-0001` has been created on nfs server `172.17.0.73`. Similar nfs share can be mounted to the pod, bound under pv/pvc/storage etc. 

1. `/exports/data-0001` is mounted under `/tmp/nfs/`

```
# I mounted this share under /tmp/
controlplane $ sudo mount 172.17.0.73:/exports/data-0001 /tmp/nfs/
controlplane $ 

# share has hi named fine now. let us check after mounting to the pod
controlplane $ touch /tmp/nfs/hi
controlplane $ ls /tmp/nfs/
hi

controlplane $ cat nfs-0001.yaml 
apiVersion: v1
kind: PersistentVolume
metadata:
  name: nfs-0001
spec:
  capacity:
    storage: 2Gi
  accessModes:
    - ReadWriteOnce
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Recycle
  nfs:
    server: 172.17.0.73
    path: /exports/data-0001
controlplane $ 

```

2. creating pv to use for pod as `Persistent Volume`

```
controlplane $ kubectl create -f nfs-0001.yaml
persistentvolume/nfs-0001 created

controlplane $ cat pvc-http.yaml
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: claim-http
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
controlplane $ kubectl create -f pvc-http.yaml
persistentvolumeclaim/claim-http created
controlplane $ 


# as you can see, pvc is bound to pv
controlplane $ kubectl get pv -oyaml
apiVersion: v1
items:
- apiVersion: v1
  kind: PersistentVolume
....
    claimRef:
      apiVersion: v1
      kind: PersistentVolumeClaim
      name: claim-http
      namespace: default
      resourceVersion: "1727"
      uid: 7468e980-795c-11ec-8f29-0242ac110049
    nfs:
      path: /exports/data-0001
      server: 172.17.0.73
      ....

```

3. now mounting this pvc to pod

nfs share is successfully mounted and showing `hi` file under it. 

```
controlplane $ cat pod-www.yaml 
apiVersion: v1
kind: Pod
metadata:
  name: www
  labels:
    name: www
spec:
  containers:
  - name: www
    image: nginx:alpine
    ports:
      - containerPort: 80
        name: www
    volumeMounts:
      - name: www-persistent-storage
        mountPath: /usr/share/nginx/html
  volumes:
    - name: www-persistent-storage
      persistentVolumeClaim:
        claimName: claim-http
controlplane $ kubectl apply -f pod-www.yaml 
pod/www created
controlplane $ kubectl get pod www
NAME   READY   STATUS    RESTARTS   AGE
www    1/1     Running   0          21s
controlplane $ kubectl exec -it www -- ls /usr/share/nginx/html
hi
controlplane $ 
```




#### Dynamic

When none of the static PVs the administrator created match a user's PersistentVolumeClaim,
the cluster may try to dynamically provision a volume specially for the PVC.
This provisioning is based on StorageClasses: the PVC must request a
storage class and
the administrator must have created and configured that class for dynamic
provisioning to occur. Claims that request the class `""` effectively disable
dynamic provisioning for themselves.

To enable dynamic storage provisioning based on storage class, the cluster administrator
needs to enable the `DefaultStorageClass` admission controller
on the API server. This can be done, for example, by ensuring that `DefaultStorageClass` is
among the comma-delimited, ordered list of values for the `--enable-admission-plugins` flag of the API server component.


Kubernetes hold provisioner and that stays in communication with storage backend. That provisioner can request creation, updation or deletion of volumes depending upon what is supported as client and user's request. 

Demo:


1. Assuming sc is already configured and communicating to storage backend - waiting for user's request

```
# Assuming sc is already configured and communicating to storage backend - waiting for user's request
controlplane $ kubectl get sc
NAME   PROVISIONER               AGE
fast   kubernetes.io/storageos   5s
controlplane $


# pvc yaml is as follow. Kubernetes will request the following volume from storage class fast. 
controlplane $ cat storageos-pvc.yaml 
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: fast0001
  annotations:
    volume.beta.kubernetes.io/storage-class: fast
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi

controlplane $ kubectl create -f storageos-pvc.yaml
persistentvolumeclaim/fast0001 created


controlplane $ kubectl describe pvc;kubectl get pvc
Name:          fast0001

...

Events:
  Type       Reason                 Age   From                         Message
  ----       ------                 ----  ----                         -------
  Normal     ProvisioningSucceeded  59s   persistentvolume-controller  Successfully provisioned volume pvc-d2360c65-796a-11ec-b44d-0242ac110055 using kubernetes.io/storageos
Mounted By:  <none>
NAME       STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
fast0001   Bound    pvc-d2360c65-796a-11ec-b44d-0242ac110055   5Gi        RWO            fast           72s
controlplane $ 

...

```
2. 
Based on events and `kubectl describe pvc` it seems that pvc has been created. This is dynamic approach of creating/deleting volumes when needed.

