# README #

<< [back to Home](../) >>


Recommended Reading:

- https://kubernetes.io/docs/concepts/storage/volume-pvc-datasource/


Demo (approch [d2iq - Volume Cloning](https://docs.d2iq.com/dkp/konvoy/1.6/storage/volume-clone/), [tested on katacoda - openEBS storage cStor](https://www.katacoda.com/kubesphere/scenarios/KubeSphere-DevOps)): 

1. First, create a Persistent Volume Claim by creating a file called `original-pvc.yaml`.

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: original-pvc
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  volumeMode: Filesystem
#   storageClassName: cstor-csi-disk
```

1. Apply the configuration to create the PVC.

```shell
controlplane $ kubectl create ns test
namespace/test created
controlplane $ kubectl create -f original-pvc.yaml -n test
persistentvolumeclaim/original-pvc created
```


output:

```
controlplane $ kubectl get pvc -n test
NAME           STATUS    VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS       AGE
original-pvc   Pending                                      cstor-csi-disk   5m32s
controlplane $ kubectl get sc
NAME                         PROVISIONER        RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGE
openebs-device               openebs.io/local   Delete          WaitForFirstConsumer   false                  14m
cstor-csi-disk (default)   cstor.csi.openebs.io   Delete          WaitForFirstConsumer   false                  14m
controlplane $ 
```

PVC will not be provisioned until pod is trying to access it. It is due to binding mode set to `WaitForFirstConsumer`

1. To create a Pod, create a new file `original-pod.yaml`

```yaml
apiVersion: v1
kind: Pod
metadata:
    name: original-pvc-consumer
spec:
    containers:
    - name: ubuntu
      image: ubuntu:latest
      command: [ "/bin/bash", "-c", "--" ]
      args: [ "echo 'test' > /mnt/azuredisk/file; while true; do sleep 30; done;" ]
      volumeMounts:
        - name: original-pvc
          mountPath: "/mnt/azuredisk"
    volumes:
      - name: original-pvc
        persistentVolumeClaim:
          claimName: original-pvc
```
Output:

Now volume has been created. and pod is in process of creation.

```shell
controlplane $ kubectl create -f original-pod.yaml -n test
pod/original-pvc-consumer created
controlplane $ kubectl get pod,pvc -n test
NAME                        READY   STATUS              RESTARTS   AGE
pod/original-pvc-consumer   0/1     ContainerCreating   0          11s

NAME                                 STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS       AGE
persistentvolumeclaim/original-pvc   Bound    pvc-0406ce71-0884-4f9c-903d-b5cc9b9fabd0   1Gi        RWO            cstor-csi-disk   10m
controlplane $ 
```


## Clone a Persistent Volume Claim

1. Create a Persistent Volume Claim by cloning an existing PVC. Create a new file `cloned-pvc.yaml`.

   An important part of this definition is `dataSource` which is set to `PersistentVolumeClaim` kind.

   It is possible to request storage capacity that is identical or higher than the source PVC.

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: cloned-pvc
  namespace: test
spec:
  resources:
    requests:
      storage: 10Gi
  accessModes:
  - ReadWriteOnce
  dataSource:
    kind: PersistentVolumeClaim
    name: original-pvc
```

```shell
controlplane $ kubectl create -f cloned-pvc.yaml -n test
persistentvolumeclaim/cloned-pvc created
controlplane $ kubectl get pvc,pod -n test
NAME                                 STATUS    VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS           AGE
persistentvolumeclaim/cloned-pvc     Pending                                                                        azurediskprovisioner   7s
persistentvolumeclaim/original-pvc   Bound     pvc-0406ce71-0884-4f9c-903d-b5cc9b9fabd0   1Gi        RWO            cstor-csi-disk       13m

NAME                        READY   STATUS    RESTARTS   AGE
pod/original-pvc-consumer   1/1     Running   0          3m15s
controlplane $ 
```

cloned-pvc will not created until any pod starts using that volume claim.

therefore, 

  The new PVC `cloned-pvc` is now waiting for its consumer.

1. Create a new file called `cloned-pod.yaml`.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: cloned-pvc-consumer
spec:
  containers:
  - name: ubuntu
    image: ubuntu:latest
    command: [ "/bin/bash", "-c", "--" ]
    args: [ "ls -lah /mnt/azuredisk/file; cat /mnt/azuredisk/file; while true; do sleep 30; done;" ]
    volumeMounts:
      - name: cloned-pvc
        mountPath: "/mnt/azuredisk"
  volumes:
    - name: cloned-pvc
      persistentVolumeClaim:
        claimName: cloned-pvc

```

```shell

controlplane $ kubectl create -f cloned-pod.yaml -n test
pod/cloned-pvc-consumer created
controlplane $ 
```


 Check the logs output of the new pod and verify that the file is present in the filesystem.

```shell
kubectl logs cloned-pvc-consumer -n test
-rw-r--r-- 1 root root 5 Jan 27 19:48 /mnt/azuredisk/file
test
```