# README #

<< [back to Home](../) >>

- NFS server

    Running nfs server in docker and creating nfs share under `exports` dir.

```
controlplane $ docker run -d --net=host \
>    --privileged --name nfs-server \
>    katacoda/contained-nfs-server:centos7 \
>    /exports/data-0001 /exports/data-0002
Unable to find image 'katacoda/contained-nfs-server:centos7' locally
centos7: Pulling from katacoda/contained-nfs-server
8d30e94188e7: Pull complete 
2b2b27f1f462: Pull complete 
133e63cf95fe: Pull complete 
Digest: sha256:5f2ea4737fe27f26be5b5cabaa23e24180079a4dce8d5db235492ec48c5552d1
Status: Downloaded newer image for katacoda/contained-nfs-server:centos7
cb2e3fdbcbc89b1d691e088bc1810506d4d1c9e4a0b853c363f4ecfb36ce120d
controlplane $ 

```

In order to create storage class, provisioner plugin with RBAC would be required which will act as client to manage nfs operation (e.g create, delete, update, etc)

kubernetes-sig team has `Kubernetes NFS Subdir External Provisioner` [github](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner)

helm will be an easiest way to install the provisioner, however, it is good to understand what is required to have storage class operation. Please read the documentation available on github.

```
helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/

helm repo update

helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
    --set nfs.server=x.x.x.x \
    --set nfs.path=/exported/path

```

In my case, nfs server would be `172.17.0.26` and would like to choose path `/exports/`

```
# helm version 2, command may differ with helm v3


controlplane $ helm install nfs-subdir-external-provisioner/      --set nfs.server=172.17.0.26     --set nfs.path=/exports/data-0001 --name=nfs-subdir-external-provisioner
NAME:   nfs-subdir-external-provisioner
LAST DEPLOYED: Sun Jan 16 23:48:40 2022
NAMESPACE: default
STATUS: DEPLOYED

RESOURCES:
==> v1/ClusterRole
NAME                                    AGE
nfs-subdir-external-provisioner-runner  0s

==> v1/ClusterRoleBinding
NAME                                 AGE
run-nfs-subdir-external-provisioner  0s

==> v1/Deployment
NAME                             READY  UP-TO-DATE  AVAILABLE  AGE
nfs-subdir-external-provisioner  0/1    1           0          0s

==> v1/Pod(related)
NAME                                              READY  STATUS             RESTARTS  AGE
nfs-subdir-external-provisioner-5c7bc45dc9-vns9f  0/1    ContainerCreating  0         0s

==> v1/Role
NAME                                            AGE
leader-locking-nfs-subdir-external-provisioner  0s

==> v1/RoleBinding
NAME                                            AGE
leader-locking-nfs-subdir-external-provisioner  0s

==> v1/ServiceAccount
NAME                             SECRETS  AGE
nfs-subdir-external-provisioner  1        0s

==> v1/StorageClass
NAME        PROVISIONER                                    AGE
nfs-client  cluster.local/nfs-subdir-external-provisioner  0s

controlplane $ kubectl get pod,sc
NAME                                                   READY   STATUS    RESTARTS   AGE
pod/nfs-subdir-external-provisioner-5c7bc45dc9-vns9f   1/1     Running   0          48s

NAME                                     PROVISIONER                                     AGE
storageclass.storage.k8s.io/nfs-client   cluster.local/nfs-subdir-external-provisioner   48s
controlplane $ 


```

storage class has been created and provisioner pod is running without any issue.

```
kind: Deployment
apiVersion: apps/v1
metadata:
  name: nfs-client-provisioner
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nfs-client-provisioner
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: nfs-client-provisioner
    spec:
      serviceAccountName: nfs-client-provisioner
      containers:
        - name: nfs-client-provisioner
          image: quay.io/external_storage/nfs-client-provisioner:latest
          volumeMounts:
            - name: nfs-client-root
              mountPath: /persistentvolumes
          env:
            - name: PROVISIONER_NAME
              value: fuseim.pri/ifs
            - name: NFS_SERVER
              value: 172.17.0.26
            - name: NFS_PATH
              value: /exports/data-0001/nfs
      volumes:
        - name: nfs-client-root
          nfs:
            server: 172.17.0.26
            path: /exports/data-0001/nfs
```