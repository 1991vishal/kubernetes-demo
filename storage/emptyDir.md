# README #

<< [back to Home](../) >>

Recommended Readings

- https://kubernetes.io/docs/concepts/storage/volumes/#downwardapi 

  - https://kubernetes.io/docs/tasks/inject-data-application/downward-api-volume-expose-pod-information/

- https://kubernetes.io/docs/concepts/storage/volumes/#emptydir

### downwardAPI 

A `downwardAPI` volume makes downward API data available to applications.
It mounts a directory and writes the requested data in plain text files.


A container using the downward API as a [`subPath`](#using-subpath) volume mount will not
receive downward API updates.


See the [downward API example](https://kubernetes.io/docs/tasks/inject-data-application/downward-api-volume-expose-pod-information/) for more details.

### emptyDir

An `emptyDir` volume is first created when a Pod is assigned to a node, and
exists as long as that Pod is running on that node. As the name says, the
`emptyDir` volume is initially empty. All containers in the Pod can read and write the same
files in the `emptyDir` volume, though that volume can be mounted at the same
or different paths in each container. When a Pod is removed from a node for
any reason, the data in the `emptyDir` is deleted permanently.


A container crashing does *not* remove a Pod from a node. The data in an `emptyDir` volume
is safe across container crashes.


Some uses for an `emptyDir` are:

* scratch space, such as for a disk-based merge sort
* checkpointing a long computation for recovery from crashes
* holding files that a content-manager container fetches while a webserver
  container serves the data

Depending on your environment, `emptyDir` volumes are stored on whatever medium that backs the
node such as disk or SSD, or network storage. However, if you set the `emptyDir.medium` field
to `"Memory"`, Kubernetes mounts a tmpfs (RAM-backed filesystem) for you instead.
While tmpfs is very fast, be aware that unlike disks, tmpfs is cleared on
node reboot and any files you write count against your container's
memory limit.


If the `SizeMemoryBackedVolumes` [feature gate](https://kubernetes.io/docs/reference/command-line-tools-reference/feature-gates/) is enabled,
you can specify a size for memory backed volumes.  If no size is specified, memory
backed volumes are sized to 50% of the memory on a Linux host.


#### emptyDir configuration example

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: k8s.gcr.io/test-webserver
    name: test-container
    volumeMounts:
    - mountPath: /cache
      name: cache-volume
  volumes:
  - name: cache-volume
    emptyDir: {}
```
